'use strict';

describe('Controller Tests', function() {

    describe('Enquete Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockEnquete, MockSituation, MockModeObtention, MockPartenaire, MockEtudiant;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockEnquete = jasmine.createSpy('MockEnquete');
            MockSituation = jasmine.createSpy('MockSituation');
            MockModeObtention = jasmine.createSpy('MockModeObtention');
            MockPartenaire = jasmine.createSpy('MockPartenaire');
            MockEtudiant = jasmine.createSpy('MockEtudiant');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Enquete': MockEnquete,
                'Situation': MockSituation,
                'ModeObtention': MockModeObtention,
                'Partenaire': MockPartenaire,
                'Etudiant': MockEtudiant
            };
            createController = function() {
                $injector.get('$controller')("EnqueteDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'taagliProjectApp:enqueteUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
