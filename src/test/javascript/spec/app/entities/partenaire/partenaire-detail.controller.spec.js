'use strict';

describe('Controller Tests', function() {

    describe('Partenaire Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockPartenaire, MockNaf, MockRegion, MockActivite;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockPartenaire = jasmine.createSpy('MockPartenaire');
            MockNaf = jasmine.createSpy('MockNaf');
            MockRegion = jasmine.createSpy('MockRegion');
            MockActivite = jasmine.createSpy('MockActivite');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Partenaire': MockPartenaire,
                'Naf': MockNaf,
                'Region': MockRegion,
                'Activite': MockActivite
            };
            createController = function() {
                $injector.get('$controller')("PartenaireDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'taagliProjectApp:partenaireUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
