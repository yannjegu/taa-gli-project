'use strict';

describe('Controller Tests', function() {

    describe('TexteEnquete Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockTexteEnquete, MockEnquete;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockTexteEnquete = jasmine.createSpy('MockTexteEnquete');
            MockEnquete = jasmine.createSpy('MockEnquete');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'TexteEnquete': MockTexteEnquete,
                'Enquete': MockEnquete
            };
            createController = function() {
                $injector.get('$controller')("TexteEnqueteDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'taagliProjectApp:texteEnqueteUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
