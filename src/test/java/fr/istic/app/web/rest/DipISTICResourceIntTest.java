package fr.istic.app.web.rest;

import fr.istic.app.TaagliProjectApp;

import fr.istic.app.domain.DipISTIC;
import fr.istic.app.repository.DipISTICRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DipISTICResource REST controller.
 *
 * @see DipISTICResource
 */
@RunWith(SpringRunner.class)

@SpringBootTest(classes = TaagliProjectApp.class)

public class DipISTICResourceIntTest {
    private static final String DEFAULT_DI_LIBELLE = "AAAAA";
    private static final String UPDATED_DI_LIBELLE = "BBBBB";

    private static final String DEFAULT_DI_DUREE = "AAAA";
    private static final String UPDATED_DI_DUREE = "BBBB";
    private static final String DEFAULT_DI_LIBC = "AAAAA";
    private static final String UPDATED_DI_LIBC = "BBBBB";

    @Inject
    private DipISTICRepository dipISTICRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDipISTICMockMvc;

    private DipISTIC dipISTIC;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DipISTICResource dipISTICResource = new DipISTICResource();
        ReflectionTestUtils.setField(dipISTICResource, "dipISTICRepository", dipISTICRepository);
        this.restDipISTICMockMvc = MockMvcBuilders.standaloneSetup(dipISTICResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DipISTIC createEntity(EntityManager em) {
        DipISTIC dipISTIC = new DipISTIC()
                .diLibelle(DEFAULT_DI_LIBELLE)
                .diDuree(DEFAULT_DI_DUREE)
                .diLibc(DEFAULT_DI_LIBC);
        return dipISTIC;
    }

    @Before
    public void initTest() {
        dipISTIC = createEntity(em);
    }

    @Test
    @Transactional
    public void createDipISTIC() throws Exception {
        int databaseSizeBeforeCreate = dipISTICRepository.findAll().size();

        // Create the DipISTIC

        restDipISTICMockMvc.perform(post("/api/dip-istics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dipISTIC)))
                .andExpect(status().isCreated());

        // Validate the DipISTIC in the database
        List<DipISTIC> dipISTICS = dipISTICRepository.findAll();
        assertThat(dipISTICS).hasSize(databaseSizeBeforeCreate + 1);
        DipISTIC testDipISTIC = dipISTICS.get(dipISTICS.size() - 1);
        assertThat(testDipISTIC.getDiLibelle()).isEqualTo(DEFAULT_DI_LIBELLE);
        assertThat(testDipISTIC.getDiDuree()).isEqualTo(DEFAULT_DI_DUREE);
        assertThat(testDipISTIC.getDiLibc()).isEqualTo(DEFAULT_DI_LIBC);
    }

    @Test
    @Transactional
    public void checkDiLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = dipISTICRepository.findAll().size();
        // set the field null
        dipISTIC.setDiLibelle(null);

        // Create the DipISTIC, which fails.

        restDipISTICMockMvc.perform(post("/api/dip-istics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dipISTIC)))
                .andExpect(status().isBadRequest());

        List<DipISTIC> dipISTICS = dipISTICRepository.findAll();
        assertThat(dipISTICS).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDiDureeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dipISTICRepository.findAll().size();
        // set the field null
        dipISTIC.setDiDuree(null);

        // Create the DipISTIC, which fails.

        restDipISTICMockMvc.perform(post("/api/dip-istics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dipISTIC)))
                .andExpect(status().isBadRequest());

        List<DipISTIC> dipISTICS = dipISTICRepository.findAll();
        assertThat(dipISTICS).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDipISTICS() throws Exception {
        // Initialize the database
        dipISTICRepository.saveAndFlush(dipISTIC);

        // Get all the dipISTICS
        restDipISTICMockMvc.perform(get("/api/dip-istics?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(dipISTIC.getId().intValue())))
                .andExpect(jsonPath("$.[*].diLibelle").value(hasItem(DEFAULT_DI_LIBELLE.toString())))
                .andExpect(jsonPath("$.[*].diDuree").value(hasItem(DEFAULT_DI_DUREE)))
                .andExpect(jsonPath("$.[*].diLibc").value(hasItem(DEFAULT_DI_LIBC.toString())));
    }

    @Test
    @Transactional
    public void getDipISTIC() throws Exception {
        // Initialize the database
        dipISTICRepository.saveAndFlush(dipISTIC);

        // Get the dipISTIC
        restDipISTICMockMvc.perform(get("/api/dip-istics/{id}", dipISTIC.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dipISTIC.getId().intValue()))
            .andExpect(jsonPath("$.diLibelle").value(DEFAULT_DI_LIBELLE.toString()))
            .andExpect(jsonPath("$.diDuree").value(DEFAULT_DI_DUREE))
            .andExpect(jsonPath("$.diLibc").value(DEFAULT_DI_LIBC.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDipISTIC() throws Exception {
        // Get the dipISTIC
        restDipISTICMockMvc.perform(get("/api/dip-istics/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDipISTIC() throws Exception {
        // Initialize the database
        dipISTICRepository.saveAndFlush(dipISTIC);
        int databaseSizeBeforeUpdate = dipISTICRepository.findAll().size();

        // Update the dipISTIC
        DipISTIC updatedDipISTIC = dipISTICRepository.findOne(dipISTIC.getId());
        updatedDipISTIC
                .diLibelle(UPDATED_DI_LIBELLE)
                .diDuree(UPDATED_DI_DUREE)
                .diLibc(UPDATED_DI_LIBC);

        restDipISTICMockMvc.perform(put("/api/dip-istics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedDipISTIC)))
                .andExpect(status().isOk());

        // Validate the DipISTIC in the database
        List<DipISTIC> dipISTICS = dipISTICRepository.findAll();
        assertThat(dipISTICS).hasSize(databaseSizeBeforeUpdate);
        DipISTIC testDipISTIC = dipISTICS.get(dipISTICS.size() - 1);
        assertThat(testDipISTIC.getDiLibelle()).isEqualTo(UPDATED_DI_LIBELLE);
        assertThat(testDipISTIC.getDiDuree()).isEqualTo(UPDATED_DI_DUREE);
        assertThat(testDipISTIC.getDiLibc()).isEqualTo(UPDATED_DI_LIBC);
    }

    @Test
    @Transactional
    public void deleteDipISTIC() throws Exception {
        // Initialize the database
        dipISTICRepository.saveAndFlush(dipISTIC);
        int databaseSizeBeforeDelete = dipISTICRepository.findAll().size();

        // Get the dipISTIC
        restDipISTICMockMvc.perform(delete("/api/dip-istics/{id}", dipISTIC.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<DipISTIC> dipISTICS = dipISTICRepository.findAll();
        assertThat(dipISTICS).hasSize(databaseSizeBeforeDelete - 1);
    }
}
