package fr.istic.app.web.rest;

import fr.istic.app.TaagliProjectApp;

import fr.istic.app.domain.DiplISTIC;
import fr.istic.app.repository.DiplISTICRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DiplISTICResource REST controller.
 *
 * @see DiplISTICResource
 */
@RunWith(SpringRunner.class)

@SpringBootTest(classes = TaagliProjectApp.class)

public class DiplISTICResourceIntTest {

    private static final Integer DEFAULT_AN_SORTIE = 1;
    private static final Integer UPDATED_AN_SORTIE = 2;
    private static final String DEFAULT_IF_DIVERS = "AAAAA";
    private static final String UPDATED_IF_DIVERS = "BBBBB";

    private static final Integer DEFAULT_AN_ENTREE = 1;
    private static final Integer UPDATED_AN_ENTREE = 2;
    private static final String DEFAULT_POURSUITE_ETUDES = "AAAAA";
    private static final String UPDATED_POURSUITE_ETUDES = "BBBBB";

    @Inject
    private DiplISTICRepository diplISTICRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDiplISTICMockMvc;

    private DiplISTIC diplISTIC;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DiplISTICResource diplISTICResource = new DiplISTICResource();
        ReflectionTestUtils.setField(diplISTICResource, "diplISTICRepository", diplISTICRepository);
        this.restDiplISTICMockMvc = MockMvcBuilders.standaloneSetup(diplISTICResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DiplISTIC createEntity(EntityManager em) {
        DiplISTIC diplISTIC = new DiplISTIC()
                .anSortie(DEFAULT_AN_SORTIE)
                .ifDivers(DEFAULT_IF_DIVERS)
                .anEntree(DEFAULT_AN_ENTREE)
                .poursuiteEtudes(DEFAULT_POURSUITE_ETUDES);
        return diplISTIC;
    }

    @Before
    public void initTest() {
        diplISTIC = createEntity(em);
    }

    @Test
    @Transactional
    public void createDiplISTIC() throws Exception {
        int databaseSizeBeforeCreate = diplISTICRepository.findAll().size();

        // Create the DiplISTIC

        restDiplISTICMockMvc.perform(post("/api/dipl-istics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(diplISTIC)))
                .andExpect(status().isCreated());

        // Validate the DiplISTIC in the database
        List<DiplISTIC> diplISTICS = diplISTICRepository.findAll();
        assertThat(diplISTICS).hasSize(databaseSizeBeforeCreate + 1);
        DiplISTIC testDiplISTIC = diplISTICS.get(diplISTICS.size() - 1);
        assertThat(testDiplISTIC.getAnSortie()).isEqualTo(DEFAULT_AN_SORTIE);
        assertThat(testDiplISTIC.getIfDivers()).isEqualTo(DEFAULT_IF_DIVERS);
        assertThat(testDiplISTIC.getAnEntree()).isEqualTo(DEFAULT_AN_ENTREE);
        assertThat(testDiplISTIC.getPoursuiteEtudes()).isEqualTo(DEFAULT_POURSUITE_ETUDES);
    }

    @Test
    @Transactional
    public void checkAnSortieIsRequired() throws Exception {
        int databaseSizeBeforeTest = diplISTICRepository.findAll().size();
        // set the field null
        diplISTIC.setAnSortie(null);

        // Create the DiplISTIC, which fails.

        restDiplISTICMockMvc.perform(post("/api/dipl-istics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(diplISTIC)))
                .andExpect(status().isBadRequest());

        List<DiplISTIC> diplISTICS = diplISTICRepository.findAll();
        assertThat(diplISTICS).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDiplISTICS() throws Exception {
        // Initialize the database
        diplISTICRepository.saveAndFlush(diplISTIC);

        // Get all the diplISTICS
        restDiplISTICMockMvc.perform(get("/api/dipl-istics?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(diplISTIC.getId().intValue())))
                .andExpect(jsonPath("$.[*].anSortie").value(hasItem(DEFAULT_AN_SORTIE)))
                .andExpect(jsonPath("$.[*].ifDivers").value(hasItem(DEFAULT_IF_DIVERS.toString())))
                .andExpect(jsonPath("$.[*].anEntree").value(hasItem(DEFAULT_AN_ENTREE)))
                .andExpect(jsonPath("$.[*].poursuiteEtudes").value(hasItem(DEFAULT_POURSUITE_ETUDES.toString())));
    }

    @Test
    @Transactional
    public void getDiplISTIC() throws Exception {
        // Initialize the database
        diplISTICRepository.saveAndFlush(diplISTIC);

        // Get the diplISTIC
        restDiplISTICMockMvc.perform(get("/api/dipl-istics/{id}", diplISTIC.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(diplISTIC.getId().intValue()))
            .andExpect(jsonPath("$.anSortie").value(DEFAULT_AN_SORTIE))
            .andExpect(jsonPath("$.ifDivers").value(DEFAULT_IF_DIVERS.toString()))
            .andExpect(jsonPath("$.anEntree").value(DEFAULT_AN_ENTREE))
            .andExpect(jsonPath("$.poursuiteEtudes").value(DEFAULT_POURSUITE_ETUDES.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDiplISTIC() throws Exception {
        // Get the diplISTIC
        restDiplISTICMockMvc.perform(get("/api/dipl-istics/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDiplISTIC() throws Exception {
        // Initialize the database
        diplISTICRepository.saveAndFlush(diplISTIC);
        int databaseSizeBeforeUpdate = diplISTICRepository.findAll().size();

        // Update the diplISTIC
        DiplISTIC updatedDiplISTIC = diplISTICRepository.findOne(diplISTIC.getId());
        updatedDiplISTIC
                .anSortie(UPDATED_AN_SORTIE)
                .ifDivers(UPDATED_IF_DIVERS)
                .anEntree(UPDATED_AN_ENTREE)
                .poursuiteEtudes(UPDATED_POURSUITE_ETUDES);

        restDiplISTICMockMvc.perform(put("/api/dipl-istics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedDiplISTIC)))
                .andExpect(status().isOk());

        // Validate the DiplISTIC in the database
        List<DiplISTIC> diplISTICS = diplISTICRepository.findAll();
        assertThat(diplISTICS).hasSize(databaseSizeBeforeUpdate);
        DiplISTIC testDiplISTIC = diplISTICS.get(diplISTICS.size() - 1);
        assertThat(testDiplISTIC.getAnSortie()).isEqualTo(UPDATED_AN_SORTIE);
        assertThat(testDiplISTIC.getIfDivers()).isEqualTo(UPDATED_IF_DIVERS);
        assertThat(testDiplISTIC.getAnEntree()).isEqualTo(UPDATED_AN_ENTREE);
        assertThat(testDiplISTIC.getPoursuiteEtudes()).isEqualTo(UPDATED_POURSUITE_ETUDES);
    }

    @Test
    @Transactional
    public void deleteDiplISTIC() throws Exception {
        // Initialize the database
        diplISTICRepository.saveAndFlush(diplISTIC);
        int databaseSizeBeforeDelete = diplISTICRepository.findAll().size();

        // Get the diplISTIC
        restDiplISTICMockMvc.perform(delete("/api/dipl-istics/{id}", diplISTIC.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<DiplISTIC> diplISTICS = diplISTICRepository.findAll();
        assertThat(diplISTICS).hasSize(databaseSizeBeforeDelete - 1);
    }
}
