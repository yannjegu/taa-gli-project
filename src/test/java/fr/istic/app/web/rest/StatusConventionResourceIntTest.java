package fr.istic.app.web.rest;

import fr.istic.app.TaagliProjectApp;

import fr.istic.app.domain.StatusConvention;
import fr.istic.app.repository.StatusConventionRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StatusConventionResource REST controller.
 *
 * @see StatusConventionResource
 */
@RunWith(SpringRunner.class)

@SpringBootTest(classes = TaagliProjectApp.class)

public class StatusConventionResourceIntTest {
    private static final String DEFAULT_LIBELLE = "AAAAA";
    private static final String UPDATED_LIBELLE = "BBBBB";

    @Inject
    private StatusConventionRepository statusConventionRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restStatusConventionMockMvc;

    private StatusConvention statusConvention;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        StatusConventionResource statusConventionResource = new StatusConventionResource();
        ReflectionTestUtils.setField(statusConventionResource, "statusConventionRepository", statusConventionRepository);
        this.restStatusConventionMockMvc = MockMvcBuilders.standaloneSetup(statusConventionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StatusConvention createEntity(EntityManager em) {
        StatusConvention statusConvention = new StatusConvention()
                .libelle(DEFAULT_LIBELLE);
        return statusConvention;
    }

    @Before
    public void initTest() {
        statusConvention = createEntity(em);
    }

    @Test
    @Transactional
    public void createStatusConvention() throws Exception {
        int databaseSizeBeforeCreate = statusConventionRepository.findAll().size();

        // Create the StatusConvention

        restStatusConventionMockMvc.perform(post("/api/status-conventions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(statusConvention)))
                .andExpect(status().isCreated());

        // Validate the StatusConvention in the database
        List<StatusConvention> statusConventions = statusConventionRepository.findAll();
        assertThat(statusConventions).hasSize(databaseSizeBeforeCreate + 1);
        StatusConvention testStatusConvention = statusConventions.get(statusConventions.size() - 1);
        assertThat(testStatusConvention.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
    }

    @Test
    @Transactional
    public void getAllStatusConventions() throws Exception {
        // Initialize the database
        statusConventionRepository.saveAndFlush(statusConvention);

        // Get all the statusConventions
        restStatusConventionMockMvc.perform(get("/api/status-conventions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(statusConvention.getId().intValue())))
                .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE.toString())));
    }

    @Test
    @Transactional
    public void getStatusConvention() throws Exception {
        // Initialize the database
        statusConventionRepository.saveAndFlush(statusConvention);

        // Get the statusConvention
        restStatusConventionMockMvc.perform(get("/api/status-conventions/{id}", statusConvention.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(statusConvention.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStatusConvention() throws Exception {
        // Get the statusConvention
        restStatusConventionMockMvc.perform(get("/api/status-conventions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStatusConvention() throws Exception {
        // Initialize the database
        statusConventionRepository.saveAndFlush(statusConvention);
        int databaseSizeBeforeUpdate = statusConventionRepository.findAll().size();

        // Update the statusConvention
        StatusConvention updatedStatusConvention = statusConventionRepository.findOne(statusConvention.getId());
        updatedStatusConvention
                .libelle(UPDATED_LIBELLE);

        restStatusConventionMockMvc.perform(put("/api/status-conventions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedStatusConvention)))
                .andExpect(status().isOk());

        // Validate the StatusConvention in the database
        List<StatusConvention> statusConventions = statusConventionRepository.findAll();
        assertThat(statusConventions).hasSize(databaseSizeBeforeUpdate);
        StatusConvention testStatusConvention = statusConventions.get(statusConventions.size() - 1);
        assertThat(testStatusConvention.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    public void deleteStatusConvention() throws Exception {
        // Initialize the database
        statusConventionRepository.saveAndFlush(statusConvention);
        int databaseSizeBeforeDelete = statusConventionRepository.findAll().size();

        // Get the statusConvention
        restStatusConventionMockMvc.perform(delete("/api/status-conventions/{id}", statusConvention.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<StatusConvention> statusConventions = statusConventionRepository.findAll();
        assertThat(statusConventions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
