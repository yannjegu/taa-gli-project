package fr.istic.app.web.rest;

import fr.istic.app.TaagliProjectApp;

import fr.istic.app.domain.Naf;
import fr.istic.app.repository.NafRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NafResource REST controller.
 *
 * @see NafResource
 */
@RunWith(SpringRunner.class)

@SpringBootTest(classes = TaagliProjectApp.class)

public class NafResourceIntTest {
    private static final String DEFAULT_NAF_LIBELLE = "AAAAA";
    private static final String UPDATED_NAF_LIBELLE = "BBBBB";

    @Inject
    private NafRepository nafRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restNafMockMvc;

    private Naf naf;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NafResource nafResource = new NafResource();
        ReflectionTestUtils.setField(nafResource, "nafRepository", nafRepository);
        this.restNafMockMvc = MockMvcBuilders.standaloneSetup(nafResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Naf createEntity(EntityManager em) {
        Naf naf = new Naf()
                .nafLibelle(DEFAULT_NAF_LIBELLE);
        return naf;
    }

    @Before
    public void initTest() {
        naf = createEntity(em);
    }

    @Test
    @Transactional
    public void createNaf() throws Exception {
        int databaseSizeBeforeCreate = nafRepository.findAll().size();

        // Create the Naf

        restNafMockMvc.perform(post("/api/nafs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(naf)))
                .andExpect(status().isCreated());

        // Validate the Naf in the database
        List<Naf> nafs = nafRepository.findAll();
        assertThat(nafs).hasSize(databaseSizeBeforeCreate + 1);
        Naf testNaf = nafs.get(nafs.size() - 1);
        assertThat(testNaf.getNafLibelle()).isEqualTo(DEFAULT_NAF_LIBELLE);
    }

    @Test
    @Transactional
    public void checkNafLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = nafRepository.findAll().size();
        // set the field null
        naf.setNafLibelle(null);

        // Create the Naf, which fails.

        restNafMockMvc.perform(post("/api/nafs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(naf)))
                .andExpect(status().isBadRequest());

        List<Naf> nafs = nafRepository.findAll();
        assertThat(nafs).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNafs() throws Exception {
        // Initialize the database
        nafRepository.saveAndFlush(naf);

        // Get all the nafs
        restNafMockMvc.perform(get("/api/nafs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(naf.getId().intValue())))
                .andExpect(jsonPath("$.[*].nafLibelle").value(hasItem(DEFAULT_NAF_LIBELLE.toString())));
    }

    @Test
    @Transactional
    public void getNaf() throws Exception {
        // Initialize the database
        nafRepository.saveAndFlush(naf);

        // Get the naf
        restNafMockMvc.perform(get("/api/nafs/{id}", naf.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(naf.getId().intValue()))
            .andExpect(jsonPath("$.nafLibelle").value(DEFAULT_NAF_LIBELLE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNaf() throws Exception {
        // Get the naf
        restNafMockMvc.perform(get("/api/nafs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNaf() throws Exception {
        // Initialize the database
        nafRepository.saveAndFlush(naf);
        int databaseSizeBeforeUpdate = nafRepository.findAll().size();

        // Update the naf
        Naf updatedNaf = nafRepository.findOne(naf.getId());
        updatedNaf
                .nafLibelle(UPDATED_NAF_LIBELLE);

        restNafMockMvc.perform(put("/api/nafs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedNaf)))
                .andExpect(status().isOk());

        // Validate the Naf in the database
        List<Naf> nafs = nafRepository.findAll();
        assertThat(nafs).hasSize(databaseSizeBeforeUpdate);
        Naf testNaf = nafs.get(nafs.size() - 1);
        assertThat(testNaf.getNafLibelle()).isEqualTo(UPDATED_NAF_LIBELLE);
    }

    @Test
    @Transactional
    public void deleteNaf() throws Exception {
        // Initialize the database
        nafRepository.saveAndFlush(naf);
        int databaseSizeBeforeDelete = nafRepository.findAll().size();

        // Get the naf
        restNafMockMvc.perform(delete("/api/nafs/{id}", naf.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Naf> nafs = nafRepository.findAll();
        assertThat(nafs).hasSize(databaseSizeBeforeDelete - 1);
    }
}
