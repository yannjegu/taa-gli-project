package fr.istic.app.web.rest;

import fr.istic.app.TaagliProjectApp;

import fr.istic.app.domain.Situation;
import fr.istic.app.repository.SituationRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SituationResource REST controller.
 *
 * @see SituationResource
 */
@RunWith(SpringRunner.class)

@SpringBootTest(classes = TaagliProjectApp.class)

public class SituationResourceIntTest {
    private static final String DEFAULT_SIT_ORDRE = "AAAAA";
    private static final String UPDATED_SIT_ORDRE = "BBBBB";
    private static final String DEFAULT_SIT_LIB_C = "AAAAA";
    private static final String UPDATED_SIT_LIB_C = "BBBBB";
    private static final String DEFAULT_SIT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_SIT_DESCRIPTION = "BBBBB";
    private static final String DEFAULT_SIT_EMPLOI = "AAAAA";
    private static final String UPDATED_SIT_EMPLOI = "BBBBB";

    @Inject
    private SituationRepository situationRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restSituationMockMvc;

    private Situation situation;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SituationResource situationResource = new SituationResource();
        ReflectionTestUtils.setField(situationResource, "situationRepository", situationRepository);
        this.restSituationMockMvc = MockMvcBuilders.standaloneSetup(situationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Situation createEntity(EntityManager em) {
        Situation situation = new Situation()
                .sitOrdre(DEFAULT_SIT_ORDRE)
                .sitLibC(DEFAULT_SIT_LIB_C)
                .sitDescription(DEFAULT_SIT_DESCRIPTION)
                .sitEmploi(DEFAULT_SIT_EMPLOI);
        return situation;
    }

    @Before
    public void initTest() {
        situation = createEntity(em);
    }

    @Test
    @Transactional
    public void createSituation() throws Exception {
        int databaseSizeBeforeCreate = situationRepository.findAll().size();

        // Create the Situation

        restSituationMockMvc.perform(post("/api/situations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(situation)))
                .andExpect(status().isCreated());

        // Validate the Situation in the database
        List<Situation> situations = situationRepository.findAll();
        assertThat(situations).hasSize(databaseSizeBeforeCreate + 1);
        Situation testSituation = situations.get(situations.size() - 1);
        assertThat(testSituation.getSitOrdre()).isEqualTo(DEFAULT_SIT_ORDRE);
        assertThat(testSituation.getSitLibC()).isEqualTo(DEFAULT_SIT_LIB_C);
        assertThat(testSituation.getSitDescription()).isEqualTo(DEFAULT_SIT_DESCRIPTION);
        assertThat(testSituation.getSitEmploi()).isEqualTo(DEFAULT_SIT_EMPLOI);
    }

    @Test
    @Transactional
    public void checkSitOrdreIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationRepository.findAll().size();
        // set the field null
        situation.setSitOrdre(null);

        // Create the Situation, which fails.

        restSituationMockMvc.perform(post("/api/situations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(situation)))
                .andExpect(status().isBadRequest());

        List<Situation> situations = situationRepository.findAll();
        assertThat(situations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSitLibCIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationRepository.findAll().size();
        // set the field null
        situation.setSitLibC(null);

        // Create the Situation, which fails.

        restSituationMockMvc.perform(post("/api/situations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(situation)))
                .andExpect(status().isBadRequest());

        List<Situation> situations = situationRepository.findAll();
        assertThat(situations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSitEmploiIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationRepository.findAll().size();
        // set the field null
        situation.setSitEmploi(null);

        // Create the Situation, which fails.

        restSituationMockMvc.perform(post("/api/situations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(situation)))
                .andExpect(status().isBadRequest());

        List<Situation> situations = situationRepository.findAll();
        assertThat(situations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSituations() throws Exception {
        // Initialize the database
        situationRepository.saveAndFlush(situation);

        // Get all the situations
        restSituationMockMvc.perform(get("/api/situations?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(situation.getId().intValue())))
                .andExpect(jsonPath("$.[*].sitOrdre").value(hasItem(DEFAULT_SIT_ORDRE.toString())))
                .andExpect(jsonPath("$.[*].sitLibC").value(hasItem(DEFAULT_SIT_LIB_C.toString())))
                .andExpect(jsonPath("$.[*].sitDescription").value(hasItem(DEFAULT_SIT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].sitEmploi").value(hasItem(DEFAULT_SIT_EMPLOI.toString())));
    }

    @Test
    @Transactional
    public void getSituation() throws Exception {
        // Initialize the database
        situationRepository.saveAndFlush(situation);

        // Get the situation
        restSituationMockMvc.perform(get("/api/situations/{id}", situation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(situation.getId().intValue()))
            .andExpect(jsonPath("$.sitOrdre").value(DEFAULT_SIT_ORDRE.toString()))
            .andExpect(jsonPath("$.sitLibC").value(DEFAULT_SIT_LIB_C.toString()))
            .andExpect(jsonPath("$.sitDescription").value(DEFAULT_SIT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.sitEmploi").value(DEFAULT_SIT_EMPLOI.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSituation() throws Exception {
        // Get the situation
        restSituationMockMvc.perform(get("/api/situations/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSituation() throws Exception {
        // Initialize the database
        situationRepository.saveAndFlush(situation);
        int databaseSizeBeforeUpdate = situationRepository.findAll().size();

        // Update the situation
        Situation updatedSituation = situationRepository.findOne(situation.getId());
        updatedSituation
                .sitOrdre(UPDATED_SIT_ORDRE)
                .sitLibC(UPDATED_SIT_LIB_C)
                .sitDescription(UPDATED_SIT_DESCRIPTION)
                .sitEmploi(UPDATED_SIT_EMPLOI);

        restSituationMockMvc.perform(put("/api/situations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedSituation)))
                .andExpect(status().isOk());

        // Validate the Situation in the database
        List<Situation> situations = situationRepository.findAll();
        assertThat(situations).hasSize(databaseSizeBeforeUpdate);
        Situation testSituation = situations.get(situations.size() - 1);
        assertThat(testSituation.getSitOrdre()).isEqualTo(UPDATED_SIT_ORDRE);
        assertThat(testSituation.getSitLibC()).isEqualTo(UPDATED_SIT_LIB_C);
        assertThat(testSituation.getSitDescription()).isEqualTo(UPDATED_SIT_DESCRIPTION);
        assertThat(testSituation.getSitEmploi()).isEqualTo(UPDATED_SIT_EMPLOI);
    }

    @Test
    @Transactional
    public void deleteSituation() throws Exception {
        // Initialize the database
        situationRepository.saveAndFlush(situation);
        int databaseSizeBeforeDelete = situationRepository.findAll().size();

        // Get the situation
        restSituationMockMvc.perform(delete("/api/situations/{id}", situation.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Situation> situations = situationRepository.findAll();
        assertThat(situations).hasSize(databaseSizeBeforeDelete - 1);
    }
}
