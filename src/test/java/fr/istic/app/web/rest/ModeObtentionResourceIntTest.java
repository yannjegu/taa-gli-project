package fr.istic.app.web.rest;

import fr.istic.app.TaagliProjectApp;

import fr.istic.app.domain.ModeObtention;
import fr.istic.app.repository.ModeObtentionRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ModeObtentionResource REST controller.
 *
 * @see ModeObtentionResource
 */
@RunWith(SpringRunner.class)

@SpringBootTest(classes = TaagliProjectApp.class)

public class ModeObtentionResourceIntTest {
    private static final String DEFAULT_OBT_ORDRE = "AAAAA";
    private static final String UPDATED_OBT_ORDRE = "BBBBB";
    private static final String DEFAULT_OBT_LIB_C = "AAAAA";
    private static final String UPDATED_OBT_LIB_C = "BBBBB";
    private static final String DEFAULT_OBT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_OBT_DESCRIPTION = "BBBBB";

    @Inject
    private ModeObtentionRepository modeObtentionRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restModeObtentionMockMvc;

    private ModeObtention modeObtention;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ModeObtentionResource modeObtentionResource = new ModeObtentionResource();
        ReflectionTestUtils.setField(modeObtentionResource, "modeObtentionRepository", modeObtentionRepository);
        this.restModeObtentionMockMvc = MockMvcBuilders.standaloneSetup(modeObtentionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModeObtention createEntity(EntityManager em) {
        ModeObtention modeObtention = new ModeObtention()
                .obtOrdre(DEFAULT_OBT_ORDRE)
                .obtLibC(DEFAULT_OBT_LIB_C)
                .obtDescription(DEFAULT_OBT_DESCRIPTION);
        return modeObtention;
    }

    @Before
    public void initTest() {
        modeObtention = createEntity(em);
    }

    @Test
    @Transactional
    public void createModeObtention() throws Exception {
        int databaseSizeBeforeCreate = modeObtentionRepository.findAll().size();

        // Create the ModeObtention

        restModeObtentionMockMvc.perform(post("/api/mode-obtentions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeObtention)))
                .andExpect(status().isCreated());

        // Validate the ModeObtention in the database
        List<ModeObtention> modeObtentions = modeObtentionRepository.findAll();
        assertThat(modeObtentions).hasSize(databaseSizeBeforeCreate + 1);
        ModeObtention testModeObtention = modeObtentions.get(modeObtentions.size() - 1);
        assertThat(testModeObtention.getObtOrdre()).isEqualTo(DEFAULT_OBT_ORDRE);
        assertThat(testModeObtention.getObtLibC()).isEqualTo(DEFAULT_OBT_LIB_C);
        assertThat(testModeObtention.getObtDescription()).isEqualTo(DEFAULT_OBT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void checkObtOrdreIsRequired() throws Exception {
        int databaseSizeBeforeTest = modeObtentionRepository.findAll().size();
        // set the field null
        modeObtention.setObtOrdre(null);

        // Create the ModeObtention, which fails.

        restModeObtentionMockMvc.perform(post("/api/mode-obtentions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeObtention)))
                .andExpect(status().isBadRequest());

        List<ModeObtention> modeObtentions = modeObtentionRepository.findAll();
        assertThat(modeObtentions).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkObtLibCIsRequired() throws Exception {
        int databaseSizeBeforeTest = modeObtentionRepository.findAll().size();
        // set the field null
        modeObtention.setObtLibC(null);

        // Create the ModeObtention, which fails.

        restModeObtentionMockMvc.perform(post("/api/mode-obtentions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeObtention)))
                .andExpect(status().isBadRequest());

        List<ModeObtention> modeObtentions = modeObtentionRepository.findAll();
        assertThat(modeObtentions).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModeObtentions() throws Exception {
        // Initialize the database
        modeObtentionRepository.saveAndFlush(modeObtention);

        // Get all the modeObtentions
        restModeObtentionMockMvc.perform(get("/api/mode-obtentions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(modeObtention.getId().intValue())))
                .andExpect(jsonPath("$.[*].obtOrdre").value(hasItem(DEFAULT_OBT_ORDRE.toString())))
                .andExpect(jsonPath("$.[*].obtLibC").value(hasItem(DEFAULT_OBT_LIB_C.toString())))
                .andExpect(jsonPath("$.[*].obtDescription").value(hasItem(DEFAULT_OBT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getModeObtention() throws Exception {
        // Initialize the database
        modeObtentionRepository.saveAndFlush(modeObtention);

        // Get the modeObtention
        restModeObtentionMockMvc.perform(get("/api/mode-obtentions/{id}", modeObtention.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modeObtention.getId().intValue()))
            .andExpect(jsonPath("$.obtOrdre").value(DEFAULT_OBT_ORDRE.toString()))
            .andExpect(jsonPath("$.obtLibC").value(DEFAULT_OBT_LIB_C.toString()))
            .andExpect(jsonPath("$.obtDescription").value(DEFAULT_OBT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingModeObtention() throws Exception {
        // Get the modeObtention
        restModeObtentionMockMvc.perform(get("/api/mode-obtentions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModeObtention() throws Exception {
        // Initialize the database
        modeObtentionRepository.saveAndFlush(modeObtention);
        int databaseSizeBeforeUpdate = modeObtentionRepository.findAll().size();

        // Update the modeObtention
        ModeObtention updatedModeObtention = modeObtentionRepository.findOne(modeObtention.getId());
        updatedModeObtention
                .obtOrdre(UPDATED_OBT_ORDRE)
                .obtLibC(UPDATED_OBT_LIB_C)
                .obtDescription(UPDATED_OBT_DESCRIPTION);

        restModeObtentionMockMvc.perform(put("/api/mode-obtentions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedModeObtention)))
                .andExpect(status().isOk());

        // Validate the ModeObtention in the database
        List<ModeObtention> modeObtentions = modeObtentionRepository.findAll();
        assertThat(modeObtentions).hasSize(databaseSizeBeforeUpdate);
        ModeObtention testModeObtention = modeObtentions.get(modeObtentions.size() - 1);
        assertThat(testModeObtention.getObtOrdre()).isEqualTo(UPDATED_OBT_ORDRE);
        assertThat(testModeObtention.getObtLibC()).isEqualTo(UPDATED_OBT_LIB_C);
        assertThat(testModeObtention.getObtDescription()).isEqualTo(UPDATED_OBT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deleteModeObtention() throws Exception {
        // Initialize the database
        modeObtentionRepository.saveAndFlush(modeObtention);
        int databaseSizeBeforeDelete = modeObtentionRepository.findAll().size();

        // Get the modeObtention
        restModeObtentionMockMvc.perform(delete("/api/mode-obtentions/{id}", modeObtention.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ModeObtention> modeObtentions = modeObtentionRepository.findAll();
        assertThat(modeObtentions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
