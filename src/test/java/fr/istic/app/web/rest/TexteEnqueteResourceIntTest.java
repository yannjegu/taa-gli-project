package fr.istic.app.web.rest;

import fr.istic.app.TaagliProjectApp;

import fr.istic.app.domain.TexteEnquete;
import fr.istic.app.repository.TexteEnqueteRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TexteEnqueteResource REST controller.
 *
 * @see TexteEnqueteResource
 */
@RunWith(SpringRunner.class)

@SpringBootTest(classes = TaagliProjectApp.class)

public class TexteEnqueteResourceIntTest {
    private static final String DEFAULT_TE_REPONSE = "AAAAA";
    private static final String UPDATED_TE_REPONSE = "BBBBB";

    @Inject
    private TexteEnqueteRepository texteEnqueteRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restTexteEnqueteMockMvc;

    private TexteEnquete texteEnquete;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TexteEnqueteResource texteEnqueteResource = new TexteEnqueteResource();
        ReflectionTestUtils.setField(texteEnqueteResource, "texteEnqueteRepository", texteEnqueteRepository);
        this.restTexteEnqueteMockMvc = MockMvcBuilders.standaloneSetup(texteEnqueteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TexteEnquete createEntity(EntityManager em) {
        TexteEnquete texteEnquete = new TexteEnquete()
                .teReponse(DEFAULT_TE_REPONSE);
        return texteEnquete;
    }

    @Before
    public void initTest() {
        texteEnquete = createEntity(em);
    }

    @Test
    @Transactional
    public void createTexteEnquete() throws Exception {
        int databaseSizeBeforeCreate = texteEnqueteRepository.findAll().size();

        // Create the TexteEnquete

        restTexteEnqueteMockMvc.perform(post("/api/texte-enquetes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(texteEnquete)))
                .andExpect(status().isCreated());

        // Validate the TexteEnquete in the database
        List<TexteEnquete> texteEnquetes = texteEnqueteRepository.findAll();
        assertThat(texteEnquetes).hasSize(databaseSizeBeforeCreate + 1);
        TexteEnquete testTexteEnquete = texteEnquetes.get(texteEnquetes.size() - 1);
        assertThat(testTexteEnquete.getTeReponse()).isEqualTo(DEFAULT_TE_REPONSE);
    }

    @Test
    @Transactional
    public void checkTeReponseIsRequired() throws Exception {
        int databaseSizeBeforeTest = texteEnqueteRepository.findAll().size();
        // set the field null
        texteEnquete.setTeReponse(null);

        // Create the TexteEnquete, which fails.

        restTexteEnqueteMockMvc.perform(post("/api/texte-enquetes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(texteEnquete)))
                .andExpect(status().isBadRequest());

        List<TexteEnquete> texteEnquetes = texteEnqueteRepository.findAll();
        assertThat(texteEnquetes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTexteEnquetes() throws Exception {
        // Initialize the database
        texteEnqueteRepository.saveAndFlush(texteEnquete);

        // Get all the texteEnquetes
        restTexteEnqueteMockMvc.perform(get("/api/texte-enquetes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(texteEnquete.getId().intValue())))
                .andExpect(jsonPath("$.[*].teReponse").value(hasItem(DEFAULT_TE_REPONSE.toString())));
    }

    @Test
    @Transactional
    public void getTexteEnquete() throws Exception {
        // Initialize the database
        texteEnqueteRepository.saveAndFlush(texteEnquete);

        // Get the texteEnquete
        restTexteEnqueteMockMvc.perform(get("/api/texte-enquetes/{id}", texteEnquete.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(texteEnquete.getId().intValue()))
            .andExpect(jsonPath("$.teReponse").value(DEFAULT_TE_REPONSE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTexteEnquete() throws Exception {
        // Get the texteEnquete
        restTexteEnqueteMockMvc.perform(get("/api/texte-enquetes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTexteEnquete() throws Exception {
        // Initialize the database
        texteEnqueteRepository.saveAndFlush(texteEnquete);
        int databaseSizeBeforeUpdate = texteEnqueteRepository.findAll().size();

        // Update the texteEnquete
        TexteEnquete updatedTexteEnquete = texteEnqueteRepository.findOne(texteEnquete.getId());
        updatedTexteEnquete
                .teReponse(UPDATED_TE_REPONSE);

        restTexteEnqueteMockMvc.perform(put("/api/texte-enquetes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedTexteEnquete)))
                .andExpect(status().isOk());

        // Validate the TexteEnquete in the database
        List<TexteEnquete> texteEnquetes = texteEnqueteRepository.findAll();
        assertThat(texteEnquetes).hasSize(databaseSizeBeforeUpdate);
        TexteEnquete testTexteEnquete = texteEnquetes.get(texteEnquetes.size() - 1);
        assertThat(testTexteEnquete.getTeReponse()).isEqualTo(UPDATED_TE_REPONSE);
    }

    @Test
    @Transactional
    public void deleteTexteEnquete() throws Exception {
        // Initialize the database
        texteEnqueteRepository.saveAndFlush(texteEnquete);
        int databaseSizeBeforeDelete = texteEnqueteRepository.findAll().size();

        // Get the texteEnquete
        restTexteEnqueteMockMvc.perform(delete("/api/texte-enquetes/{id}", texteEnquete.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TexteEnquete> texteEnquetes = texteEnqueteRepository.findAll();
        assertThat(texteEnquetes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
