package fr.istic.app.web.rest;

import fr.istic.app.TaagliProjectApp;

import fr.istic.app.domain.Activite;
import fr.istic.app.repository.ActiviteRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ActiviteResource REST controller.
 *
 * @see ActiviteResource
 */
@RunWith(SpringRunner.class)

@SpringBootTest(classes = TaagliProjectApp.class)

public class ActiviteResourceIntTest {
    private static final String DEFAULT_TA_LIBELLE = "AAAAA";
    private static final String UPDATED_TA_LIBELLE = "BBBBB";

    @Inject
    private ActiviteRepository activiteRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restActiviteMockMvc;

    private Activite activite;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ActiviteResource activiteResource = new ActiviteResource();
        ReflectionTestUtils.setField(activiteResource, "activiteRepository", activiteRepository);
        this.restActiviteMockMvc = MockMvcBuilders.standaloneSetup(activiteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Activite createEntity(EntityManager em) {
        Activite activite = new Activite()
                .taLibelle(DEFAULT_TA_LIBELLE);
        return activite;
    }

    @Before
    public void initTest() {
        activite = createEntity(em);
    }

    @Test
    @Transactional
    public void createActivite() throws Exception {
        int databaseSizeBeforeCreate = activiteRepository.findAll().size();

        // Create the Activite

        restActiviteMockMvc.perform(post("/api/activites")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(activite)))
                .andExpect(status().isCreated());

        // Validate the Activite in the database
        List<Activite> activites = activiteRepository.findAll();
        assertThat(activites).hasSize(databaseSizeBeforeCreate + 1);
        Activite testActivite = activites.get(activites.size() - 1);
        assertThat(testActivite.getTaLibelle()).isEqualTo(DEFAULT_TA_LIBELLE);
    }

    @Test
    @Transactional
    public void checkTaLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = activiteRepository.findAll().size();
        // set the field null
        activite.setTaLibelle(null);

        // Create the Activite, which fails.

        restActiviteMockMvc.perform(post("/api/activites")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(activite)))
                .andExpect(status().isBadRequest());

        List<Activite> activites = activiteRepository.findAll();
        assertThat(activites).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllActivites() throws Exception {
        // Initialize the database
        activiteRepository.saveAndFlush(activite);

        // Get all the activites
        restActiviteMockMvc.perform(get("/api/activites?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(activite.getId().intValue())))
                .andExpect(jsonPath("$.[*].taLibelle").value(hasItem(DEFAULT_TA_LIBELLE.toString())));
    }

    @Test
    @Transactional
    public void getActivite() throws Exception {
        // Initialize the database
        activiteRepository.saveAndFlush(activite);

        // Get the activite
        restActiviteMockMvc.perform(get("/api/activites/{id}", activite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(activite.getId().intValue()))
            .andExpect(jsonPath("$.taLibelle").value(DEFAULT_TA_LIBELLE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingActivite() throws Exception {
        // Get the activite
        restActiviteMockMvc.perform(get("/api/activites/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateActivite() throws Exception {
        // Initialize the database
        activiteRepository.saveAndFlush(activite);
        int databaseSizeBeforeUpdate = activiteRepository.findAll().size();

        // Update the activite
        Activite updatedActivite = activiteRepository.findOne(activite.getId());
        updatedActivite
                .taLibelle(UPDATED_TA_LIBELLE);

        restActiviteMockMvc.perform(put("/api/activites")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedActivite)))
                .andExpect(status().isOk());

        // Validate the Activite in the database
        List<Activite> activites = activiteRepository.findAll();
        assertThat(activites).hasSize(databaseSizeBeforeUpdate);
        Activite testActivite = activites.get(activites.size() - 1);
        assertThat(testActivite.getTaLibelle()).isEqualTo(UPDATED_TA_LIBELLE);
    }

    @Test
    @Transactional
    public void deleteActivite() throws Exception {
        // Initialize the database
        activiteRepository.saveAndFlush(activite);
        int databaseSizeBeforeDelete = activiteRepository.findAll().size();

        // Get the activite
        restActiviteMockMvc.perform(delete("/api/activites/{id}", activite.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Activite> activites = activiteRepository.findAll();
        assertThat(activites).hasSize(databaseSizeBeforeDelete - 1);
    }
}
