(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .factory('ExportCSV', ExportCSV);

    function ExportCSV () {
        var service = {
            convertStagesToCSVObject: convertStagesToCSVObject,
            convertStageToCSVObject: convertStageToCSVObject,
            convertStagesValidationEnseignantToCSVObject: convertStagesValidationEnseignantToCSVObject,
            convertStagesEtudiantToCSVObject: convertStagesEtudiantToCSVObject,
            convertStagesUniversiteToCSVObject: convertStagesUniversiteToCSVObject
        };
        return service;

        function convertStagesToCSVObject(source) {
            var dest = [];
            // Condensed CSV data
            for(var i=0; i<source.length; i++) {
                dest.push({
                    id: source[i].id,
                    nom_etudiant: source[i].aIdent.aNomUsuel,
                    prenom_etudiant: source[i].aIdent.aPrenoms,
                    date_debut: source[i].sDateDebut,
                    date_fin: source[i].sDateFin,
                    etat_convention: source[i].scIdent.libelle,
                    date_modification: source[i].sDateMaj,
                    auteur_modification: source[i].sMaj
                });
            }
            return dest;
        };

        function convertStagesValidationEnseignantToCSVObject(source) {
            var dest = [];
            // Condensed CSV data
            for(var i=0; i<source.length; i++) {
                dest.push({
                    id: source[i].id,
                    nom_etudiant: source[i].aIdent.aNomUsuel,
                    prenom_etudiant: source[i].aIdent.aPrenoms,
                    date_debut: source[i].sDateDebut,
                    date_fin: source[i].sDateFin,
                    etat_convention: source[i].scIdent.libelle,
                    date_modification: source[i].sDateMaj,
                    auteur_modification: source[i].sMaj
                });
            }
            return dest;
        };

        function convertStagesEtudiantToCSVObject(source) {
            var dest = [];
            // Condensed CSV data
            for(var i=0; i<source.length; i++) {
                dest.push({
                    id: source[i].id,
                    sujet: source[i].sSujet,
                    partenaire: source[i].pIdent.pNompart,
                    service: source[i].pIdent.pService,
                    date_debut: source[i].sDateDebut,
                    date_fin: source[i].sDateFin,
                    etat_convention: source[i].scIdent.libelle,
                    date_modification: source[i].sDateMaj,
                    auteur_modification: source[i].sMaj
                });
            }
            return dest;
        };

        function convertStagesUniversiteToCSVObject(source) {
            var dest = [];
            // Condensed CSV data
            for(var i=0; i<source.length; i++) {
                dest.push({
                    id: source[i].id,
                    numero_etudiant: source[i].aIdent.aNumSeq,
                    nom_etudiant: source[i].aIdent.aNomUsuel,
                    prenom_etudiant: source[i].aIdent.aPrenoms,
                    partenaire: source[i].pIdent.pNompart,
                    enseignant: source[i].enIdent.enSesame,
                    etat_convention: source[i].scIdent.libelle,
                    date_modification: source[i].sDateMaj,
                    auteur_modification: source[i].sMaj
                });
            }
            return dest;
        };

        function convertStageToCSVObject(source) {
            var dest = {
                id: source.id,
                nom_entreprise: source.pIdent.pNompart,
                adresse_entreprise: source.pIdent.pRue,
                complement_adresse_entreprise: source.pIdent.pCpltRue,
                ville_entreprise: source.pIdent.pVille,
                service_entreprise: source.pIdent.pService,
                sesame_etudiant: source.aIdent.aNumSeq,
                nom_etudiant: source.aIdent.aNomUsuel,
                prenom_etudiant: source.aIdent.aPrenoms,
                date_debut: source.sDateDebut,
                date_fin: source.sDateFin,
                sujet: source.sSujet,
                langages: source.sLang,
                materiel: source.sMateriel,
                mots_cles: source.mMotsCles,
                details: source.sDetails,
                jours_duree_travail: source.sJoursDureeTravail,
                indp: source.sIndp,
                versement: source.sVersement,
                avantages_nature: source.sAvantagesNature,
                mode_suivi: source.sModeSuiv,
                appreciation: source.sAppreciation,
                date_convention: source.sDateConv,
                etat_convention: source.sEtaConv,
                soutenance: source.sSoutenance,
                rapport: source.sRapport,
                operation: source.sOperat,
                sesame_enseignant: source.enIdent.enSesame,
                nom_enseignant: source.enIdent.enNom,
                prenom_enseignant: source.enIdent.enPrenom,
                nom_tuteur: source.numSeqEnc.cNom,
                prenom_tuteur: source.numSeqEnc.user.firstName,
                nom_responsable: source.numSeqResp.cNom,
                prenom_responsable: source.numSeqResp.user.firstName,
                date_modification: source.sDateMaj,
                auteur_modification: source.sMaj
            };
            return dest;
        }
    }
})();
