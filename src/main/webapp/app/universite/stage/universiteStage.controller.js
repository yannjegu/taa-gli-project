(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('UniversiteStageController', UniversiteStageController);

    UniversiteStageController.$inject = ['$scope', '$state', 'UniversiteStage', 'StatusConvention', 'ExportCSV'];

    function UniversiteStageController ($scope, $state, UniversiteStage, StatusConvention, ExportCSV) {
        var vm = this;

        vm.stages = [];
        vm.stagesCSV = [];

        $scope.sortType     = 'id';
        $scope.sortReverse  = false;
        vm.filter           = '';

        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;

        loadAll();

        function loadAll() {
            UniversiteStage.query(function(result) {
                vm.stages = result;
            }).$promise.then(function (res) {
                // Content loaded
                vm.stagesCSV = ExportCSV.convertStagesUniversiteToCSVObject(vm.stages);
            }, function (err) {
                // Error
                console.log(err);
            });
            StatusConvention.query(function(result) {
                vm.statusConvention = result;
            });
        }

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
