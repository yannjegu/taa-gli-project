(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('UniversiteStageSignController',UniversiteStageSignController);

    UniversiteStageSignController.$inject = ['$uibModalInstance', 'entity', 'ConventionSigned'];

    function UniversiteStageSignController($uibModalInstance, entity, ConventionSigned) {
        var vm = this;

        vm.stage = entity;
        vm.clear = clear;
        vm.confirmSign = confirmSign;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmSign (id) {
            ConventionSigned.update(id,
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
