(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('UniversiteStageDeleteController',UniversiteStageDeleteController);

    UniversiteStageDeleteController.$inject = ['$uibModalInstance', 'entity', 'UniversiteStage'];

    function UniversiteStageDeleteController($uibModalInstance, entity, UniversiteStage) {
        var vm = this;

        vm.stage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UniversiteStage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
