(function() {
    'use strict';
    angular
        .module('taagliProjectApp')
        .factory('UniversiteStage', UniversiteStage);

    UniversiteStage.$inject = ['$resource', 'DateUtils'];

    angular
        .module('taagliProjectApp')
        .factory('ConventionSigned', ConventionSigned);

    ConventionSigned.$inject = ['$resource', 'DateUtils'];

    function UniversiteStage ($resource, DateUtils) {
        var resourceUrl =  'api/stages/universite/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.sDateMaj = DateUtils.convertLocalDateFromServer(data.sDateMaj);
                        data.sDateDebut = DateUtils.convertLocalDateFromServer(data.sDateDebut);
                        data.sDateFin = DateUtils.convertLocalDateFromServer(data.sDateFin);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.sDateMaj = DateUtils.convertLocalDateToServer(data.sDateMaj);
                    data.sDateDebut = DateUtils.convertLocalDateToServer(data.sDateDebut);
                    data.sDateFin = DateUtils.convertLocalDateToServer(data.sDateFin);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.sDateMaj = DateUtils.convertLocalDateToServer(data.sDateMaj);
                    data.sDateDebut = DateUtils.convertLocalDateToServer(data.sDateDebut);
                    data.sDateFin = DateUtils.convertLocalDateToServer(data.sDateFin);
                    return angular.toJson(data);
                }
            }
        });
    }

    function ConventionSigned ($resource, DateUtils) {
        var resourceUrl =  'api/stages/universite/sign/:id';

        return $resource(resourceUrl, {}, {
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    return data;
                }
            }
        });
    }
})();
