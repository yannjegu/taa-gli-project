(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('universiteStage', {
            parent: 'app',
            url: '/universite/stage',
            data: {
                authorities: ['ROLE_UNIVERSITY'],
                pageTitle: 'Stages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/universite/stage/universiteStage.html',
                    controller: 'UniversiteStageController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('universiteStage-detail', {
            parent: 'app',
            url: '/universite/stage/{id}',
            data: {
                authorities: ['ROLE_UNIVERSITY'],
                pageTitle: 'Stage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/universite/stage/universiteStage-detail.html',
                    controller: 'UniversiteStageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'UniversiteStage', function($stateParams, UniversiteStage) {
                    return UniversiteStage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'universiteStage',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('universiteStage-detail.edit', {
            parent: 'universiteStage-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_UNIVERSITY']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/universite/stage/universiteStage-dialog.html',
                    controller: 'UniversiteStageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UniversiteStage', function(UniversiteStage) {
                            return UniversiteStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('universiteStage-detail.sign', {
            parent: 'universiteStage-detail',
            url: '/detail/sign',
            data: {
                authorities: ['ROLE_UNIVERSITY']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/universite/stage/universiteStage-sign-dialog.html',
                    controller: 'UniversiteStageSignController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UniversiteStage', function(UniversiteStage) {
                            return UniversiteStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('universiteStage.edit', {
            parent: 'universiteStage',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_UNIVERSITY']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/universite/stage/universiteStage-dialog.html',
                    controller: 'UniversiteStageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UniversiteStage', function(UniversiteStage) {
                            return UniversiteStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('universiteStage', null, { reload: 'universiteStage' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('universiteStage.delete', {
            parent: 'universiteStage',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_UNIVERSITY']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/universite/stage/universiteStage-delete-dialog.html',
                    controller: 'UniversiteStageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UniversiteStage', function(UniversiteStage) {
                            return UniversiteStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('universiteStage', null, { reload: 'universiteStage' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('universiteStage.sign', {
            parent: 'universiteStage',
            url: '/{id}/sign',
            data: {
                authorities: ['ROLE_UNIVERSITY']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/universite/stage/universiteStage-sign-dialog.html',
                    controller: 'UniversiteStageSignController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UniversiteStage', function(UniversiteStage) {
                            return UniversiteStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('universiteStage', null, { reload: 'universiteStage' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
