(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ExportCsvController', ExportCsvController);

    ExportCsvController.$inject = ['$scope'];

    function ExportCsvController ($scope) {
        $scope.exportData = function (fileName, items) {
            alasql('SELECT * INTO CSV(?,{headers:true}) FROM ?',[fileName + '.csv', items]);
        };
    };
})();
