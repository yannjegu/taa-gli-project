(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ModeObtentionDetailController', ModeObtentionDetailController);

    ModeObtentionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ModeObtention'];

    function ModeObtentionDetailController($scope, $rootScope, $stateParams, previousState, entity, ModeObtention) {
        var vm = this;

        vm.modeObtention = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:modeObtentionUpdate', function(event, result) {
            vm.modeObtention = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
