(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ModeObtentionDialogController', ModeObtentionDialogController);

    ModeObtentionDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ModeObtention'];

    function ModeObtentionDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ModeObtention) {
        var vm = this;

        vm.modeObtention = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.modeObtention.id !== null) {
                ModeObtention.update(vm.modeObtention, onSaveSuccess, onSaveError);
            } else {
                ModeObtention.save(vm.modeObtention, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:modeObtentionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
