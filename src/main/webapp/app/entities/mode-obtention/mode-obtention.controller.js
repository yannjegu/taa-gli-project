(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ModeObtentionController', ModeObtentionController);

    ModeObtentionController.$inject = ['$scope', '$state', 'ModeObtention'];

    function ModeObtentionController ($scope, $state, ModeObtention) {
        var vm = this;
        
        vm.modeObtentions = [];

        loadAll();

        function loadAll() {
            ModeObtention.query(function(result) {
                vm.modeObtentions = result;
            });
        }
    }
})();
