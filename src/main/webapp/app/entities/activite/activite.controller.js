(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ActiviteController', ActiviteController);

    ActiviteController.$inject = ['$scope', '$state', 'Activite'];

    function ActiviteController ($scope, $state, Activite) {
        var vm = this;
        
        vm.activites = [];

        loadAll();

        function loadAll() {
            Activite.query(function(result) {
                vm.activites = result;
            });
        }
    }
})();
