(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ActiviteDetailController', ActiviteDetailController);

    ActiviteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Activite'];

    function ActiviteDetailController($scope, $rootScope, $stateParams, previousState, entity, Activite) {
        var vm = this;

        vm.activite = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:activiteUpdate', function(event, result) {
            vm.activite = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
