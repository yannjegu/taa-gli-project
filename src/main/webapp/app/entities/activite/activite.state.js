(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('activite', {
            parent: 'entity',
            url: '/entity/activite',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'Activites'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/activite/activites.html',
                    controller: 'ActiviteController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('activite-detail', {
            parent: 'entity',
            url: '/entity/activite/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'Activite'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/activite/activite-detail.html',
                    controller: 'ActiviteDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Activite', function($stateParams, Activite) {
                    return Activite.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'activite',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('activite-detail.edit', {
            parent: 'activite-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/activite/activite-dialog.html',
                    controller: 'ActiviteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Activite', function(Activite) {
                            return Activite.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('activite.new', {
            parent: 'activite',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/activite/activite-dialog.html',
                    controller: 'ActiviteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                taLibelle: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('activite', null, { reload: 'activite' });
                }, function() {
                    $state.go('activite');
                });
            }]
        })
        .state('activite.edit', {
            parent: 'activite',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/activite/activite-dialog.html',
                    controller: 'ActiviteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Activite', function(Activite) {
                            return Activite.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('activite', null, { reload: 'activite' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('activite.delete', {
            parent: 'activite',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/activite/activite-delete-dialog.html',
                    controller: 'ActiviteDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Activite', function(Activite) {
                            return Activite.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('activite', null, { reload: 'activite' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
