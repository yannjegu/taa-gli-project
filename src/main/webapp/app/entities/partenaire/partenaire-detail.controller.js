(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('PartenaireDetailController', PartenaireDetailController);

    PartenaireDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Partenaire', 'Naf', 'Region', 'Activite'];

    function PartenaireDetailController($scope, $rootScope, $stateParams, previousState, entity, Partenaire, Naf, Region, Activite) {
        var vm = this;

        vm.partenaire = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:partenaireUpdate', function(event, result) {
            vm.partenaire = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
