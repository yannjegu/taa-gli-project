(function() {
    'use strict';
    angular
        .module('taagliProjectApp')
        .factory('Partenaire', Partenaire);

    Partenaire.$inject = ['$resource', 'DateUtils'];

    function Partenaire ($resource, DateUtils) {
        var resourceUrl =  'api/partenaires/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.pDateMaj = DateUtils.convertLocalDateFromServer(data.pDateMaj);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.pDateMaj = DateUtils.convertLocalDateToServer(data.pDateMaj);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.pDateMaj = DateUtils.convertLocalDateToServer(data.pDateMaj);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
