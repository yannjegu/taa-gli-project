(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('PartenaireDialogController', PartenaireDialogController);

    PartenaireDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Partenaire', 'Naf', 'Region', 'Activite', 'Principal'];

    function PartenaireDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Partenaire, Naf, Region, Activite, Principal) {
        var vm = this;

        vm.partenaire = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.nafcodes = Naf.query({filter: 'partenaire-is-null'});
        $q.all([vm.partenaire.$promise, vm.nafcodes.$promise]).then(function() {
            if (!vm.partenaire.nafCode || !vm.partenaire.nafCode.id) {
                return $q.reject();
            }
            return Naf.get({id : vm.partenaire.nafCode.id}).$promise;
        }).then(function(nafCode) {
            vm.nafcodes.push(nafCode);
        });
        vm.rcoderegs = Region.query({filter: 'partenaire-is-null'});
        $q.all([vm.partenaire.$promise, vm.rcoderegs.$promise]).then(function() {
            if (!vm.partenaire.rCodereg || !vm.partenaire.rCodereg.id) {
                return $q.reject();
            }
            return Region.get({id : vm.partenaire.rCodereg.id}).$promise;
        }).then(function(rCodereg) {
            vm.rcoderegs.push(rCodereg);
        });
        vm.activites = Activite.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            Principal.identity().then(function(account) {
            	vm.partenaire.pMaj = account.login;
            });
            vm.partenaire.pDateMaj = new Date().toISOString().slice(0,10);
            if (vm.partenaire.id !== null) {
                Partenaire.update(vm.partenaire, onSaveSuccess, onSaveError);
            } else {
                Partenaire.save(vm.partenaire, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:partenaireUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.pDateMaj = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
