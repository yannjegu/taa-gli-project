(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('dipl-istic', {
            parent: 'entity',
            url: '/entity/dipl-istic',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'DiplISTICS'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dipl-istic/dipl-istics.html',
                    controller: 'DiplISTICController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('dipl-istic-detail', {
            parent: 'entity',
            url: '/entity/dipl-istic/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'DiplISTIC'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dipl-istic/dipl-istic-detail.html',
                    controller: 'DiplISTICDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'DiplISTIC', function($stateParams, DiplISTIC) {
                    return DiplISTIC.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'dipl-istic',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('dipl-istic-detail.edit', {
            parent: 'dipl-istic-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dipl-istic/dipl-istic-dialog.html',
                    controller: 'DiplISTICDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DiplISTIC', function(DiplISTIC) {
                            return DiplISTIC.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dipl-istic.new', {
            parent: 'dipl-istic',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dipl-istic/dipl-istic-dialog.html',
                    controller: 'DiplISTICDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                anSortie: null,
                                ifDivers: null,
                                anEntree: null,
                                poursuiteEtudes: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('dipl-istic', null, { reload: 'dipl-istic' });
                }, function() {
                    $state.go('dipl-istic');
                });
            }]
        })
        .state('dipl-istic.edit', {
            parent: 'dipl-istic',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dipl-istic/dipl-istic-dialog.html',
                    controller: 'DiplISTICDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DiplISTIC', function(DiplISTIC) {
                            return DiplISTIC.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dipl-istic', null, { reload: 'dipl-istic' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dipl-istic.delete', {
            parent: 'dipl-istic',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dipl-istic/dipl-istic-delete-dialog.html',
                    controller: 'DiplISTICDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DiplISTIC', function(DiplISTIC) {
                            return DiplISTIC.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dipl-istic', null, { reload: 'dipl-istic' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
