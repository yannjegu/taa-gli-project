(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('DiplISTICDeleteController',DiplISTICDeleteController);

    DiplISTICDeleteController.$inject = ['$uibModalInstance', 'entity', 'DiplISTIC'];

    function DiplISTICDeleteController($uibModalInstance, entity, DiplISTIC) {
        var vm = this;

        vm.diplISTIC = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DiplISTIC.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
