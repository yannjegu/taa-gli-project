(function() {
    'use strict';
    angular
        .module('taagliProjectApp')
        .factory('DiplISTIC', DiplISTIC);

    DiplISTIC.$inject = ['$resource'];

    function DiplISTIC ($resource) {
        var resourceUrl =  'api/dipl-istics/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
