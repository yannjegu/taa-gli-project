(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('DiplISTICDetailController', DiplISTICDetailController);

    DiplISTICDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DiplISTIC'];

    function DiplISTICDetailController($scope, $rootScope, $stateParams, previousState, entity, DiplISTIC) {
        var vm = this;

        vm.diplISTIC = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:diplISTICUpdate', function(event, result) {
            vm.diplISTIC = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
