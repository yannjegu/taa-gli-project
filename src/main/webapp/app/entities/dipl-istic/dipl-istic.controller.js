(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('DiplISTICController', DiplISTICController);

    DiplISTICController.$inject = ['$scope', '$state', 'DiplISTIC'];

    function DiplISTICController ($scope, $state, DiplISTIC) {
        var vm = this;
        
        vm.diplISTICS = [];

        loadAll();

        function loadAll() {
            DiplISTIC.query(function(result) {
                vm.diplISTICS = result;
            });
        }
    }
})();
