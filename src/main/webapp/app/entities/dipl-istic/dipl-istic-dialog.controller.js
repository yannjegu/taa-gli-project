(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('DiplISTICDialogController', DiplISTICDialogController);

    DiplISTICDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DiplISTIC'];

    function DiplISTICDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DiplISTIC) {
        var vm = this;

        vm.diplISTIC = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.diplISTIC.id !== null) {
                DiplISTIC.update(vm.diplISTIC, onSaveSuccess, onSaveError);
            } else {
                DiplISTIC.save(vm.diplISTIC, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:diplISTICUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
