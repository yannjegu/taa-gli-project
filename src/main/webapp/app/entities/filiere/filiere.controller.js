(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('FiliereController', FiliereController);

    FiliereController.$inject = ['$scope', '$state', 'Filiere'];

    function FiliereController ($scope, $state, Filiere) {
        var vm = this;
        
        vm.filieres = [];

        loadAll();

        function loadAll() {
            Filiere.query(function(result) {
                vm.filieres = result;
            });
        }
    }
})();
