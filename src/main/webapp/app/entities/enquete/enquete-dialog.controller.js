(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('EnqueteDialogController', EnqueteDialogController);

    EnqueteDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Enquete', 'Situation', 'ModeObtention', 'Partenaire', 'Etudiant'];

    function EnqueteDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Enquete, Situation, ModeObtention, Partenaire, Etudiant) {
        var vm = this;

        vm.enquete = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.enqsituations = Situation.query({filter: 'enquete-is-null'});
        $q.all([vm.enquete.$promise, vm.enqsituations.$promise]).then(function() {
            if (!vm.enquete.enqSituation || !vm.enquete.enqSituation.id) {
                return $q.reject();
            }
            return Situation.get({id : vm.enquete.enqSituation.id}).$promise;
        }).then(function(enqSituation) {
            vm.enqsituations.push(enqSituation);
        });
        vm.enqmodeobtentions = ModeObtention.query({filter: 'enquete-is-null'});
        $q.all([vm.enquete.$promise, vm.enqmodeobtentions.$promise]).then(function() {
            if (!vm.enquete.enqModeObtention || !vm.enquete.enqModeObtention.id) {
                return $q.reject();
            }
            return ModeObtention.get({id : vm.enquete.enqModeObtention.id}).$promise;
        }).then(function(enqModeObtention) {
            vm.enqmodeobtentions.push(enqModeObtention);
        });
        vm.partenaires = Partenaire.query();
        vm.etudiants = Etudiant.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.enquete.id !== null) {
                Enquete.update(vm.enquete, onSaveSuccess, onSaveError);
            } else {
                Enquete.save(vm.enquete, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:enqueteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.sDateMaj = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
