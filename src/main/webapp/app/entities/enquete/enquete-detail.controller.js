(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('EnqueteDetailController', EnqueteDetailController);

    EnqueteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Enquete', 'Situation', 'ModeObtention', 'Partenaire', 'Etudiant'];

    function EnqueteDetailController($scope, $rootScope, $stateParams, previousState, entity, Enquete, Situation, ModeObtention, Partenaire, Etudiant) {
        var vm = this;

        vm.enquete = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:enqueteUpdate', function(event, result) {
            vm.enquete = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
