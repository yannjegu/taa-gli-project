(function() {
    'use strict';
    angular
        .module('taagliProjectApp')
        .factory('StatusConvention', StatusConvention);

    StatusConvention.$inject = ['$resource'];

    function StatusConvention ($resource) {
        var resourceUrl =  'api/status-conventions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
