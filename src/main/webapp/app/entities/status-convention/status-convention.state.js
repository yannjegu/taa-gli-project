(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('status-convention', {
            parent: 'entity',
            url: '/status-convention?page&sort&search',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'StatusConventions'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/status-convention/status-conventions.html',
                    controller: 'StatusConventionController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
            }
        })
        .state('status-convention-detail', {
            parent: 'entity',
            url: '/status-convention/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'StatusConvention'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/status-convention/status-convention-detail.html',
                    controller: 'StatusConventionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'StatusConvention', function($stateParams, StatusConvention) {
                    return StatusConvention.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'status-convention',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        });
    }

})();
