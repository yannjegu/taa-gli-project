(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('StatusConventionDetailController', StatusConventionDetailController);

    StatusConventionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'StatusConvention'];

    function StatusConventionDetailController($scope, $rootScope, $stateParams, previousState, entity, StatusConvention) {
        var vm = this;

        vm.statusConvention = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:statusConventionUpdate', function(event, result) {
            vm.statusConvention = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
