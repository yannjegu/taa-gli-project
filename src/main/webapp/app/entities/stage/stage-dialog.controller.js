(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('StageDialogController', StageDialogController);

    StageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Stage', 'Partenaire', 'Enseignant', 'Contact', 'Etudiant', 'Principal'];

    function StageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Stage, Partenaire, Enseignant, Contact, Etudiant, Principal) {
        var vm = this;

        vm.stage = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.pidents = Partenaire.query({filter: 'stage-is-null'});
        $q.all([vm.stage.$promise, vm.pidents.$promise]).then(function() {
            if (!vm.stage.pIdent || !vm.stage.pIdent.id) {
                return $q.reject();
            }
            return Partenaire.get({id : vm.stage.pIdent.id}).$promise;
        }).then(function(pIdent) {
            vm.pidents.push(pIdent);
        });
        vm.enidents = Enseignant.query({filter: 'stage-is-null'});
        $q.all([vm.stage.$promise, vm.enidents.$promise]).then(function() {
            if (!vm.stage.enIdent || !vm.stage.enIdent.id) {
                return $q.reject();
            }
            return Enseignant.get({id : vm.stage.enIdent.id}).$promise;
        }).then(function(enIdent) {
            vm.enidents.push(enIdent);
        });
        vm.numseqencs = Contact.query({filter: 'stage-is-null'});
        $q.all([vm.stage.$promise, vm.numseqencs.$promise]).then(function() {
            if (!vm.stage.numSeqEnc || !vm.stage.numSeqEnc.id) {
                return $q.reject();
            }
            return Contact.get({id : vm.stage.numSeqEnc.id}).$promise;
        }).then(function(numSeqEnc) {
            vm.numseqencs.push(numSeqEnc);
        });
        vm.numseqresps = Contact.query({filter: 'stage-is-null'});
        $q.all([vm.stage.$promise, vm.numseqresps.$promise]).then(function() {
            if (!vm.stage.numSeqResp || !vm.stage.numSeqResp.id) {
                return $q.reject();
            }
            return Contact.get({id : vm.stage.numSeqResp.id}).$promise;
        }).then(function(numSeqResp) {
            vm.numseqresps.push(numSeqResp);
        });
        vm.etudiants = Etudiant.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            vm.stage.sDateMaj = new Date().toISOString().slice(0,10);
            Principal.identity().then(function(account) {
            	vm.stage.sMaj = account.login;
            });
            if (vm.stage.id !== null) {
                Stage.update(vm.stage, onSaveSuccess, onSaveError);
            } else {
                Stage.save(vm.stage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:stageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.sDateMaj = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
