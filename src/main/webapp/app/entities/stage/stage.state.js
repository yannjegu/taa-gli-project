(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('stage', {
            parent: 'entity',
            url: '/entity/stage',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'Stages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/stage/stages.html',
                    controller: 'StageController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('stage-detail', {
            parent: 'entity',
            url: '/entity/stage/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'Stage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/stage/stage-detail.html',
                    controller: 'StageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Stage', function($stateParams, Stage) {
                    return Stage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'stage',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('stage-detail.edit', {
            parent: 'stage-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/stage/stage-dialog.html',
                    controller: 'StageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Stage', function(Stage) {
                            return Stage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('stage.new', {
            parent: 'stage',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/stage/stage-dialog.html',
                    controller: 'StageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                sNumSeq: null,
                                sService: null,
                                sDateDebut: null,
                                sDateFin: null,
                                sSujet: null,
                                sLang: null,
                                sMateriel: null,
                                mMotsCles: null,
                                sDetails: null,
                                sJoursDureeTravail: null,
                                sIndp: null,
                                sVersement: null,
                                sAvantagesNature: null,
                                sModeSuiv: null,
                                sAppreciation: null,
                                sDateConv: null,
                                sEtaConv: null,
                                sSoutenance: null,
                                sRapport: null,
                                sOperat: null,
                                sDateMaj: null,
                                sMaj: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('stage', null, { reload: 'stage' });
                }, function() {
                    $state.go('stage');
                });
            }]
        })
        .state('stage.edit', {
            parent: 'stage',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/stage/stage-dialog.html',
                    controller: 'StageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Stage', function(Stage) {
                            return Stage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('stage', null, { reload: 'stage' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('stage.delete', {
            parent: 'stage',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/stage/stage-delete-dialog.html',
                    controller: 'StageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Stage', function(Stage) {
                            return Stage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('stage', null, { reload: 'stage' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
