(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ContactDialogController', ContactDialogController);

    ContactDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Contact', 'Partenaire', 'User'];

    function ContactDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Contact, Partenaire, User) {
        var vm = this;

        vm.contact = entity;
        vm.clear = clear;
        vm.save = save;
        vm.pidents = Partenaire.query({filter: 'contact-is-null'});
        $q.all([vm.contact.$promise, vm.pidents.$promise]).then(function() {
            if (!vm.contact.pIdent || !vm.contact.pIdent.id) {
                return $q.reject();
            }
            return Partenaire.get({id : vm.contact.pIdent.id}).$promise;
        }).then(function(pIdent) {
            vm.pidents.push(pIdent);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            vm.contact.user.lastName = vm.contact.cNom;
            if (vm.contact.id !== null) {
            	vm.contact.user.authorities = ["ROLE_ENTERPRISE"];
            	User.update(vm.contact.user, onUpdateUserSuccess, onSaveError);
            } else {
            	vm.contact.user.authorities = ["ROLE_ENTERPRISE"];
            	User.save(vm.contact.user, onSaveUserSuccess, onSaveError);
            }
        }

        function onSaveUserSuccess (result) {
            vm.contact.user = result;
            Contact.save(vm.contact, onSaveContactSuccess, onSaveError);
            $scope.$emit('taagliProjectApp:userInsert', result);
            $uibModalInstance.close(result);
        }
        
        function onUpdateUserSuccess (result) {
        	Contact.update(vm.contact, onSaveContactSuccess, onSaveError);
        	$scope.$emit('taagliProjectApp:userUpdate', result);
            $uibModalInstance.close(result);
        }
        
        function onSaveContactSuccess (result) {
        	$scope.$emit('taagliProjectApp:contactUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    }
})();
