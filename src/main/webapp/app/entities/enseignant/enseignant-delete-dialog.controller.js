(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('EnseignantDeleteController',EnseignantDeleteController);

    EnseignantDeleteController.$inject = ['$uibModalInstance', 'entity', 'Enseignant', 'User'];

    function EnseignantDeleteController($uibModalInstance, entity, Enseignant, User) {
        var vm = this;

        vm.enseignant = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id, user) {
            Enseignant.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
            User.delete({login: user},
                function () {

                });
        }
    }
})();
