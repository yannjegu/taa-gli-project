(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('EnseignantDialogController', EnseignantDialogController);

    EnseignantDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Enseignant', 'Principal', 'User'];

    function EnseignantDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Enseignant, Principal, User) {
        var vm = this;

        vm.enseignant = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            Principal.identity().then(function(account) {
            	vm.enseignant.enMaj = account.login;
            });

            vm.enseignant.enActif = vm.enseignant.user.activated;
            vm.enseignant.enSesame = vm.enseignant.user.login;

            vm.enseignant.user.lastName = vm.enseignant.enNom;
            vm.enseignant.user.firstName = vm.enseignant.enPrenom;
            vm.enseignant.user.email = vm.enseignant.enAdElec;
            vm.enseignant.user.authorities = ["ROLE_TEACHER"];

            if (vm.enseignant.id !== null) {

            	User.update(vm.enseignant.user, onUpdateUserSuccess, onSaveError);
            } else {
            	User.save(vm.enseignant.user, onSaveUserSuccess, onSaveError);
            }
        }

        function onSaveUserSuccess (result) {
            vm.enseignant.user = result;
            Enseignant.save(vm.enseignant, onSaveEnseignantSuccess, onSaveError);
            $scope.$emit('taagliProjectApp:userInsert', result);
            $uibModalInstance.close(result);
        }

        function onUpdateUserSuccess (result) {
        	Enseignant.update(vm.enseignant, onSaveEnseignantSuccess, onSaveError);
        	$scope.$emit('taagliProjectApp:userUpdate', result);
          $uibModalInstance.close(result);
        }

        function onSaveEnseignantSuccess (result) {
        	$scope.$emit('taagliProjectApp:enseignantUpdate', result);
          $uibModalInstance.close(result);
          vm.isSaving = false;
        }

        function onSaveError () {
          vm.isSaving = false;
        }
    }
})();
