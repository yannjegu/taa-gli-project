(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('EtudiantDeleteController',EtudiantDeleteController);

    EtudiantDeleteController.$inject = ['$uibModalInstance', 'entity', 'Etudiant', 'User'];

    function EtudiantDeleteController($uibModalInstance, entity, Etudiant, User) {
        var vm = this;

        vm.etudiant = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id, user) {
        	Etudiant.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        	User.delete({login: user},
            	function () {
                  	
                });
        }
    }
})();
