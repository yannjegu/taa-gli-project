(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('EtudiantDialogController', EtudiantDialogController);

    EtudiantDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Etudiant', 'Enquete', 'Stage', 'User', 'Principal', 'DipISTIC'];

    function EtudiantDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Etudiant, Enquete, Stage, User, Principal, DipISTIC) {
        var vm = this;

        vm.etudiant = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.anpremieremplois = Enquete.query({filter: 'etudiant-is-null'});
        $q.all([vm.etudiant.$promise, vm.anpremieremplois.$promise]).then(function() {
            if (!vm.etudiant.anPremierEmploi || !vm.etudiant.anPremierEmploi.id) {
                return $q.reject();
            }
            return Enquete.get({id : vm.etudiant.anPremierEmploi.id}).$promise;
        }).then(function(anPremierEmploi) {
            vm.anpremieremplois.push(anPremierEmploi);
        });

        vm.andernieresituations = Enquete.query({filter: 'etudiant-is-null'});
        $q.all([vm.etudiant.$promise, vm.andernieresituations.$promise]).then(function() {
            if (!vm.etudiant.anDerniereSituation || !vm.etudiant.anDerniereSituation.id) {
                return $q.reject();
            }
            return Enquete.get({id : vm.etudiant.anDerniereSituation.id}).$promise;
        }).then(function(anDerniereSituation) {
            vm.andernieresituations.push(anDerniereSituation);
        });

        vm.diplome = DipISTIC.query({filter: 'dipISTIC-is-null'});
        $q.all([vm.etudiant.$promise, vm.diplome.$promise]).then(function() {
            if (!vm.etudiant.etDiplome || !vm.etudiant.etDiplome.id) {
                return $q.reject();
            }
            return DipISTIC.get({id : vm.etudiant.etDiplome.id}).$promise;
        }).then(function(etDiplome) {
            vm.etDiplome.push(etDiplome);
        });

        vm.stages = Stage.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            Principal.identity().then(function(account) {
            	vm.etudiant.aMaj = account.login;
            });
            vm.etudiant.aNumSeq = vm.etudiant.user.login;
            vm.etudiant.user.lastName = vm.etudiant.aNomUsuel;
            vm.etudiant.user.email = vm.etudiant.aAdElec;
            vm.etudiant.user.firstName = vm.etudiant.aPrenoms;
            if (vm.etudiant.id !== null) {
            	vm.etudiant.user.authorities = ["ROLE_STUDENT"];
            	User.update(vm.etudiant.user, onUpdateUserSuccess, onSaveError);
            } else {
            	vm.etudiant.user.authorities = ["ROLE_STUDENT"];
            	User.save(vm.etudiant.user, onSaveUserSuccess, onSaveError);
            }
        }

        function onSaveUserSuccess (result) {
            vm.etudiant.user = result;
            vm.etudiant.aDateMaj = new Date().toISOString().slice(0,10);
            Etudiant.save(vm.etudiant, onSaveEtudiantSuccess, onSaveError);
            $scope.$emit('taagliProjectApp:userInsert', result);
            $uibModalInstance.close(result);
        }

        function onUpdateUserSuccess (result) {
        	vm.etudiant.aDateMaj = new Date().toISOString().slice(0,10);
        	Etudiant.update(vm.etudiant, onSaveEtudiantSuccess, onSaveError);
        	$scope.$emit('taagliProjectApp:userUpdate', result);
            $uibModalInstance.close(result);
        }

        function onSaveEtudiantSuccess (result) {
        	$scope.$emit('taagliProjectApp:etudiantUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.aDateMaj = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
