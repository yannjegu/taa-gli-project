(function() {
    'use strict';
    angular
        .module('taagliProjectApp')
        .factory('DipISTIC', DipISTIC);

    DipISTIC.$inject = ['$resource'];

    function DipISTIC ($resource) {
        var resourceUrl =  'api/dip-istics/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
