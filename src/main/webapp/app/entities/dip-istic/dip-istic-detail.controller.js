(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('DipISTICDetailController', DipISTICDetailController);

    DipISTICDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DipISTIC', 'Enseignant'];

    function DipISTICDetailController($scope, $rootScope, $stateParams, previousState, entity, DipISTIC, Enseignant) {
        var vm = this;

        vm.dipISTIC = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:dipISTICUpdate', function(event, result) {
            vm.dipISTIC = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
