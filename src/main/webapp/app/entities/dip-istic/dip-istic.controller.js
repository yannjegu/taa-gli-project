(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('DipISTICController', DipISTICController);

    DipISTICController.$inject = ['$scope', '$state', 'DipISTIC'];

    function DipISTICController ($scope, $state, DipISTIC) {
        var vm = this;
        
        vm.dipISTICS = [];

        loadAll();

        function loadAll() {
            DipISTIC.query(function(result) {
                vm.dipISTICS = result;
            });
        }
    }
})();
