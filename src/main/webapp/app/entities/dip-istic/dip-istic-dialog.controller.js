(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('DipISTICDialogController', DipISTICDialogController);

    DipISTICDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'DipISTIC', 'Enseignant'];

    function DipISTICDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, DipISTIC, Enseignant) {
        var vm = this;

        vm.dipISTIC = entity;
        vm.clear = clear;
        vm.save = save;
        vm.enidents = Enseignant.query({filter: 'dipistic-is-null'});
        $q.all([vm.dipISTIC.$promise, vm.enidents.$promise]).then(function() {
            if (!vm.dipISTIC.enIdent || !vm.dipISTIC.enIdent.id) {
                return $q.reject();
            }
            return Enseignant.get({id : vm.dipISTIC.enIdent.id}).$promise;
        }).then(function(enIdent) {
            vm.enidents.push(enIdent);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.dipISTIC.id !== null) {
                DipISTIC.update(vm.dipISTIC, onSaveSuccess, onSaveError);
            } else {
                DipISTIC.save(vm.dipISTIC, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:dipISTICUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
