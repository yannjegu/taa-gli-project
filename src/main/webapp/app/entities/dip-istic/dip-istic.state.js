(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('dip-istic', {
            parent: 'entity',
            url: '/entity/dip-istic',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'DipISTICS'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dip-istic/dip-istics.html',
                    controller: 'DipISTICController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('dip-istic-detail', {
            parent: 'entity',
            url: '/entity/dip-istic/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'DipISTIC'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dip-istic/dip-istic-detail.html',
                    controller: 'DipISTICDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'DipISTIC', function($stateParams, DipISTIC) {
                    return DipISTIC.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'dip-istic',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('dip-istic-detail.edit', {
            parent: 'dip-istic-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dip-istic/dip-istic-dialog.html',
                    controller: 'DipISTICDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DipISTIC', function(DipISTIC) {
                            return DipISTIC.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dip-istic.new', {
            parent: 'dip-istic',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dip-istic/dip-istic-dialog.html',
                    controller: 'DipISTICDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                diLibelle: null,
                                diDuree: null,
                                diLibc: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('dip-istic', null, { reload: 'dip-istic' });
                }, function() {
                    $state.go('dip-istic');
                });
            }]
        })
        .state('dip-istic.edit', {
            parent: 'dip-istic',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dip-istic/dip-istic-dialog.html',
                    controller: 'DipISTICDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DipISTIC', function(DipISTIC) {
                            return DipISTIC.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dip-istic', null, { reload: 'dip-istic' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dip-istic.delete', {
            parent: 'dip-istic',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dip-istic/dip-istic-delete-dialog.html',
                    controller: 'DipISTICDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DipISTIC', function(DipISTIC) {
                            return DipISTIC.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dip-istic', null, { reload: 'dip-istic' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
