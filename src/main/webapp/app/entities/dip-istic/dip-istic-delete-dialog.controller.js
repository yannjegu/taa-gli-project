(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('DipISTICDeleteController',DipISTICDeleteController);

    DipISTICDeleteController.$inject = ['$uibModalInstance', 'entity', 'DipISTIC'];

    function DipISTICDeleteController($uibModalInstance, entity, DipISTIC) {
        var vm = this;

        vm.dipISTIC = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DipISTIC.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
