(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('texte-enquete', {
            parent: 'entity',
            url: '/entity/texte-enquete',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'TexteEnquetes'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/texte-enquete/texte-enquetes.html',
                    controller: 'TexteEnqueteController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('texte-enquete-detail', {
            parent: 'entity',
            url: '/entity/texte-enquete/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'TexteEnquete'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-detail.html',
                    controller: 'TexteEnqueteDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'TexteEnquete', function($stateParams, TexteEnquete) {
                    return TexteEnquete.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'texte-enquete',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('texte-enquete-detail.edit', {
            parent: 'texte-enquete-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-dialog.html',
                    controller: 'TexteEnqueteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TexteEnquete', function(TexteEnquete) {
                            return TexteEnquete.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('texte-enquete.new', {
            parent: 'texte-enquete',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-dialog.html',
                    controller: 'TexteEnqueteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                teReponse: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('texte-enquete', null, { reload: 'texte-enquete' });
                }, function() {
                    $state.go('texte-enquete');
                });
            }]
        })
        .state('texte-enquete.edit', {
            parent: 'texte-enquete',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-dialog.html',
                    controller: 'TexteEnqueteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TexteEnquete', function(TexteEnquete) {
                            return TexteEnquete.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('texte-enquete', null, { reload: 'texte-enquete' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('texte-enquete.delete', {
            parent: 'texte-enquete',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-delete-dialog.html',
                    controller: 'TexteEnqueteDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TexteEnquete', function(TexteEnquete) {
                            return TexteEnquete.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('texte-enquete', null, { reload: 'texte-enquete' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
