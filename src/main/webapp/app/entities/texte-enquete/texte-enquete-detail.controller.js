(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('TexteEnqueteDetailController', TexteEnqueteDetailController);

    TexteEnqueteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TexteEnquete', 'Enquete'];

    function TexteEnqueteDetailController($scope, $rootScope, $stateParams, previousState, entity, TexteEnquete, Enquete) {
        var vm = this;

        vm.texteEnquete = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:texteEnqueteUpdate', function(event, result) {
            vm.texteEnquete = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
