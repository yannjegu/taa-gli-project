(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('TexteEnqueteDialogController', TexteEnqueteDialogController);

    TexteEnqueteDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'TexteEnquete', 'Enquete'];

    function TexteEnqueteDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, TexteEnquete, Enquete) {
        var vm = this;

        vm.texteEnquete = entity;
        vm.clear = clear;
        vm.save = save;
        vm.aidents = Enquete.query({filter: 'texteenquete-is-null'});
        $q.all([vm.texteEnquete.$promise, vm.aidents.$promise]).then(function() {
            if (!vm.texteEnquete.aIdent || !vm.texteEnquete.aIdent.id) {
                return $q.reject();
            }
            return Enquete.get({id : vm.texteEnquete.aIdent.id}).$promise;
        }).then(function(aIdent) {
            vm.aidents.push(aIdent);
        });
        vm.enqnordres = Enquete.query({filter: 'texteenquete-is-null'});
        $q.all([vm.texteEnquete.$promise, vm.enqnordres.$promise]).then(function() {
            if (!vm.texteEnquete.enqNOrdre || !vm.texteEnquete.enqNOrdre.id) {
                return $q.reject();
            }
            return Enquete.get({id : vm.texteEnquete.enqNOrdre.id}).$promise;
        }).then(function(enqNOrdre) {
            vm.enqnordres.push(enqNOrdre);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.texteEnquete.id !== null) {
                TexteEnquete.update(vm.texteEnquete, onSaveSuccess, onSaveError);
            } else {
                TexteEnquete.save(vm.texteEnquete, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:texteEnqueteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
