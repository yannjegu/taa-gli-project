(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('TexteEnqueteController', TexteEnqueteController);

    TexteEnqueteController.$inject = ['$scope', '$state', 'TexteEnquete'];

    function TexteEnqueteController ($scope, $state, TexteEnquete) {
        var vm = this;
        
        vm.texteEnquetes = [];

        loadAll();

        function loadAll() {
            TexteEnquete.query(function(result) {
                vm.texteEnquetes = result;
            });
        }
    }
})();
