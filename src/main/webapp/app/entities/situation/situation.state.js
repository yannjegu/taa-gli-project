(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('situation', {
            parent: 'entity',
            url: '/entity/situation',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'Situations'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/situation/situations.html',
                    controller: 'SituationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('situation-detail', {
            parent: 'entity',
            url: '/entity/situation/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'Situation'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/situation/situation-detail.html',
                    controller: 'SituationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Situation', function($stateParams, Situation) {
                    return Situation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'situation',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('situation-detail.edit', {
            parent: 'situation-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/situation/situation-dialog.html',
                    controller: 'SituationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Situation', function(Situation) {
                            return Situation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('situation.new', {
            parent: 'situation',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/situation/situation-dialog.html',
                    controller: 'SituationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                sitOrdre: null,
                                sitLibC: null,
                                sitDescription: null,
                                sitEmploi: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('situation', null, { reload: 'situation' });
                }, function() {
                    $state.go('situation');
                });
            }]
        })
        .state('situation.edit', {
            parent: 'situation',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/situation/situation-dialog.html',
                    controller: 'SituationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Situation', function(Situation) {
                            return Situation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('situation', null, { reload: 'situation' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('situation.delete', {
            parent: 'situation',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/situation/situation-delete-dialog.html',
                    controller: 'SituationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Situation', function(Situation) {
                            return Situation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('situation', null, { reload: 'situation' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
