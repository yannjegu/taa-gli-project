(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('SituationDeleteController',SituationDeleteController);

    SituationDeleteController.$inject = ['$uibModalInstance', 'entity', 'Situation'];

    function SituationDeleteController($uibModalInstance, entity, Situation) {
        var vm = this;

        vm.situation = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Situation.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
