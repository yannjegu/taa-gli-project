(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('SituationController', SituationController);

    SituationController.$inject = ['$scope', '$state', 'Situation'];

    function SituationController ($scope, $state, Situation) {
        var vm = this;
        
        vm.situations = [];

        loadAll();

        function loadAll() {
            Situation.query(function(result) {
                vm.situations = result;
            });
        }
    }
})();
