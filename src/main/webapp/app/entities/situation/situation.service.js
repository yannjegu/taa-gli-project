(function() {
    'use strict';
    angular
        .module('taagliProjectApp')
        .factory('Situation', Situation);

    Situation.$inject = ['$resource'];

    function Situation ($resource) {
        var resourceUrl =  'api/situations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
