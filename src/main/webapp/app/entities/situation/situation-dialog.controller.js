(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('SituationDialogController', SituationDialogController);

    SituationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Situation'];

    function SituationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Situation) {
        var vm = this;

        vm.situation = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.situation.id !== null) {
                Situation.update(vm.situation, onSaveSuccess, onSaveError);
            } else {
                Situation.save(vm.situation, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:situationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
