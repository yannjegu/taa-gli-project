(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('SituationDetailController', SituationDetailController);

    SituationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Situation'];

    function SituationDetailController($scope, $rootScope, $stateParams, previousState, entity, Situation) {
        var vm = this;

        vm.situation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:situationUpdate', function(event, result) {
            vm.situation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
