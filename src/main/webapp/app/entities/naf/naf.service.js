(function() {
    'use strict';
    angular
        .module('taagliProjectApp')
        .factory('Naf', Naf);

    Naf.$inject = ['$resource'];

    function Naf ($resource) {
        var resourceUrl =  'api/nafs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
