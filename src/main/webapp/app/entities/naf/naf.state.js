(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('naf', {
            parent: 'entity',
            url: '/entity/naf',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'Nafs'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/naf/nafs.html',
                    controller: 'NafController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('naf-detail', {
            parent: 'entity',
            url: '/entity/naf/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'Naf'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/naf/naf-detail.html',
                    controller: 'NafDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Naf', function($stateParams, Naf) {
                    return Naf.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'naf',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('naf-detail.edit', {
            parent: 'naf-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/naf/naf-dialog.html',
                    controller: 'NafDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Naf', function(Naf) {
                            return Naf.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('naf.new', {
            parent: 'naf',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/naf/naf-dialog.html',
                    controller: 'NafDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nafLibelle: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('naf', null, { reload: 'naf' });
                }, function() {
                    $state.go('naf');
                });
            }]
        })
        .state('naf.edit', {
            parent: 'naf',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/naf/naf-dialog.html',
                    controller: 'NafDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Naf', function(Naf) {
                            return Naf.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('naf', null, { reload: 'naf' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('naf.delete', {
            parent: 'naf',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/naf/naf-delete-dialog.html',
                    controller: 'NafDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Naf', function(Naf) {
                            return Naf.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('naf', null, { reload: 'naf' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
