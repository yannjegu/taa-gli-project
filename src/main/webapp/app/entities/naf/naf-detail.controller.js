(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('NafDetailController', NafDetailController);

    NafDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Naf', 'Activite'];

    function NafDetailController($scope, $rootScope, $stateParams, previousState, entity, Naf, Activite) {
        var vm = this;

        vm.naf = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:nafUpdate', function(event, result) {
            vm.naf = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
