(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('NafController', NafController);

    NafController.$inject = ['$scope', '$state', 'Naf'];

    function NafController ($scope, $state, Naf) {
        var vm = this;
        
        vm.nafs = [];

        loadAll();

        function loadAll() {
            Naf.query(function(result) {
                vm.nafs = result;
            });
        }
    }
})();
