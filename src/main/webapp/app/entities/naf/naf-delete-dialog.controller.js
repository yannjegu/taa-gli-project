(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('NafDeleteController',NafDeleteController);

    NafDeleteController.$inject = ['$uibModalInstance', 'entity', 'Naf'];

    function NafDeleteController($uibModalInstance, entity, Naf) {
        var vm = this;

        vm.naf = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Naf.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
