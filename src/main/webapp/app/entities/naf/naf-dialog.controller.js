(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('NafDialogController', NafDialogController);

    NafDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Naf', 'Activite'];

    function NafDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Naf, Activite) {
        var vm = this;

        vm.naf = entity;
        vm.clear = clear;
        vm.save = save;
        vm.activites = Activite.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.naf.id !== null) {
                Naf.update(vm.naf, onSaveSuccess, onSaveError);
            } else {
                Naf.save(vm.naf, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:nafUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
