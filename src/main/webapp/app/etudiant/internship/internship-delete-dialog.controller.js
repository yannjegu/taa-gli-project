(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('InternshipDeleteController',InternshipDeleteController);

    InternshipDeleteController.$inject = ['$uibModalInstance', 'entity', 'Internship'];

    function InternshipDeleteController($uibModalInstance, entity, Internship) {
        var vm = this;

        vm.stage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Internship.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
