(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('internship', {
            parent: 'app',
            url: '/etudiant/stage',
            data: {
                authorities: ['ROLE_STUDENT'],
                pageTitle: 'Stages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/etudiant/internship/internship.html',
                    controller: 'InternshipController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('internship-detail', {
            parent: 'app',
            url: '/etudiant/internship/{id}',
            data: {
                authorities: ['ROLE_STUDENT'],
                pageTitle: 'Stage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/etudiant/internship/internship-detail.html',
                    controller: 'InternshipDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Internship', function($stateParams, Internship) {
                    return Internship.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'internship',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('internship-detail.edit', {
            parent: 'internship-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_STUDENT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/etudiant/internship/internship-dialog.html',
                    controller: 'InternshipDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Internship', function(Internship) {
                            return Internship.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('internship.new', {
            parent: 'internship',
            url: '/new',
            data: {
                authorities: ['ROLE_STUDENT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/etudiant/internship/internship-dialog.html',
                    controller: 'InternshipDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                sNumSeq: null,
                                sService: null,
                                sDateDebut: null,
                                sDateFin: null,
                                sSujet: null,
                                sLang: null,
                                sMateriel: null,
                                mMotsCles: null,
                                sDetails: null,
                                sJoursDureeTravail: null,
                                sIndp: null,
                                sVersement: null,
                                sAvantagesNature: null,
                                sModeSuiv: null,
                                sAppreciation: null,
                                sDateConv: null,
                                sEtaConv: null,
                                sSoutenance: null,
                                sRapport: null,
                                sOperat: null,
                                sDateMaj: null,
                                sMaj: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('internship', null, { reload: 'internship' });
                }, function() {
                    $state.go('internship');
                });
            }]
        })
        .state('internship.edit', {
            parent: 'internship',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_STUDENT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/etudiant/internship/internship-dialog.html',
                    controller: 'InternshipDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Internship', function(Internship) {
                            return Internship.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('internship', null, { reload: 'internship' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('internship.delete', {
            parent: 'internship',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_STUDENT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/etudiant/internship/internship-delete-dialog.html',
                    controller: 'InternshipDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Internship', function(Internship) {
                            return Internship.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('internship', null, { reload: 'internship' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
