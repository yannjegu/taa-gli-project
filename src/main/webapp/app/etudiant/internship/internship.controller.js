(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('InternshipController', InternshipController);

    InternshipController.$inject = ['$scope', '$state', 'Internship', 'ExportCSV'];

    function InternshipController ($scope, $state, Internship, ExportCSV) {
        var vm = this;

        vm.stages = [];
        vm.stagesCSV = [];

        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;

        $scope.sortType     = 'sDateDebut';
        $scope.sortReverse  = true;
        vm.filter           = '';

        loadAll();

        function loadAll() {
        	Internship.query(function(result) {
                vm.stages = result;
            }).$promise.then(function (res) {
                // Content loaded
                console.log(vm.stages);
                vm.stagesCSV = ExportCSV.convertStagesToCSVObject(vm.stages);
            }, function (err) {
                // Error
                console.log(err);
            });
        }

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
