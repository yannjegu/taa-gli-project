(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ValidationStageController', ValidationStageController);

    ValidationStageController.$inject = ['$scope', '$state', 'ValidationStage', 'ExportCSV'];

    function ValidationStageController ($scope, $state, ValidationStage, ExportCSV) {
        var vm = this;

        vm.stages = [];
        vm.stagesCSV = [];

        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;

        $scope.sortType     = 'sDateDebut';
        $scope.sortReverse  = true;
        vm.filter           = '';

        loadAll();

        function loadAll() {
        	ValidationStage.query(function(result) {
                vm.stages = result;
            }).$promise.then(function (res) {
                // Content loaded
                vm.stagesCSV = ExportCSV.convertStagesValidationEnseignantToCSVObject(vm.stages);
            }, function (err) {
                // Error
                console.log(err);
            });
        }

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
