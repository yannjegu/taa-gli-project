(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ValidationStageValidateController',ValidationStageValidateController);

    ValidationStageValidateController.$inject = ['$uibModalInstance', 'entity', 'ValidationStage', '$scope', 'Principal'];

    function ValidationStageValidateController($uibModalInstance, entity, ValidationStage, $scope, Principal) {
        var vm = this;

        vm.stage = entity;
        vm.clear = clear;
        vm.confirmValidate = confirmValidate;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmValidate () {
            vm.isSaving = true;
            vm.stage.sDateMaj = new Date().toISOString().slice(0,10);
            Principal.identity().then(function(account) {
              vm.stage.sMaj = account.login;
            });

            if (vm.stage.id !== null) {
                ValidationStage.update(vm.stage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taagliProjectApp:stageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    }
})();
