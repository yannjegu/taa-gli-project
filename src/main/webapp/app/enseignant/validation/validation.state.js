(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('enseignantValidationStage', {
            parent: 'app',
            url: '/enseignant/validation',
            data: {
                authorities: ['ROLE_TEACHER'],
                pageTitle: 'Validations des stages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/enseignant/validation/validation.html',
                    controller: 'ValidationStageController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('enseignantValidationStage-detail', {
            parent: 'app',
            url: '/enseignant/validation/{id}',
            data: {
                authorities: ['ROLE_TEACHER'],
                pageTitle: 'Validation des stage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/enseignant/validation/validation-detail.html',
                    controller: 'ValidationStageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'ValidationStage', function($stateParams, ValidationStage) {
                    return ValidationStage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'enseignantValidationStage',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('enseignantValidationStage.valider', {
            parent: 'enseignantValidationStage',
            url: '/{id}/stage',
            data: {
                authorities: ['ROLE_TEACHER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/enseignant/validation/validation-validate-dialog.html',
                    controller: 'ValidationStageValidateController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ValidationStage', function(ValidationStage) {
                            return ValidationStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('enseignantValidationStage', null, { reload: 'enseignantValidationStage' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('enseignantValidationStage-detail.valider', {
            parent: 'enseignantValidationStage-detail',
            url: '/{id}/stage',
            data: {
                authorities: ['ROLE_TEACHER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/enseignant/validation/validation-validate-dialog.html',
                    controller: 'ValidationStageValidateController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ValidationStage', function(ValidationStage) {
                            return ValidationStage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('enseignantValidationStage', null, { reload: 'enseignantValidationStage' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
