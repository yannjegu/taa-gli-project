(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ValidationStageDetailController', ValidationStageDetailController);

    ValidationStageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ValidationStage', 'Partenaire', 'Enseignant', 'Contact', 'Etudiant', 'ExportCSV'];

    function ValidationStageDetailController($scope, $rootScope, $stateParams, previousState, entity, ValidationStage, Partenaire, Enseignant, Contact, Etudiant, ExportCSV) {
        var vm = this;

        vm.stage = entity;
        vm.stageCSV = ExportCSV.convertStageToCSVObject(vm.stage);
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:stageUpdate', function(event, result) {
            vm.stage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
