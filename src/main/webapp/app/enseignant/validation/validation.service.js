(function() {
    'use strict';
    angular
        .module('taagliProjectApp')
        .factory('ValidationStage', ValidationStage);

    ValidationStage.$inject = ['$resource', 'DateUtils'];

    function ValidationStage ($resource, DateUtils) {
        var resourceUrl =  'api/stages/responsable/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.sDateMaj = DateUtils.convertLocalDateFromServer(data.sDateMaj);
                        data.sDateDebut = DateUtils.convertLocalDateFromServer(data.sDateDebut);
                        data.sDateFin = DateUtils.convertLocalDateFromServer(data.sDateFin);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.sDateMaj = DateUtils.convertLocalDateToServer(data.sDateMaj);
                    data.sDateDebut = DateUtils.convertLocalDateToServer(data.sDateDebut);
                    data.sDateFin = DateUtils.convertLocalDateToServer(data.sDateFin);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
