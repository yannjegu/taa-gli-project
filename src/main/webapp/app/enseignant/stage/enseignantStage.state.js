(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('enseignantStage', {
            parent: 'app',
            url: '/enseignant/stage',
            data: {
                authorities: ['ROLE_TEACHER'],
                pageTitle: 'Stages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/enseignant/stage/enseignantStage.html',
                    controller: 'EnseignantStageController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('enseignantStage-detail', {
            parent: 'app',
            url: '/enseignant/stage/{id}',
            data: {
                authorities: ['ROLE_TEACHER'],
                pageTitle: 'Stage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/enseignant/stage/enseignantStage-detail.html',
                    controller: 'EnseignantStageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'EnseignantStage', function($stateParams, EnseignantStage) {
                    return EnseignantStage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'enseignantStage',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        });
    }

})();
