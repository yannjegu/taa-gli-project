(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('EnseignantStageController', EnseignantStageController);

    EnseignantStageController.$inject = ['$scope', '$state', '$filter', 'EnseignantStage', 'ExportCSV'];

    function EnseignantStageController ($scope, $state, $filter, EnseignantStage, ExportCSV) {
        var vm = this;

        vm.stages = [];
        vm.stagesCSV = [];



        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;

        $scope.sortType     = 'sDateDebut';
        $scope.sortReverse  = true;
        vm.filter           = '';

        loadAll();

        function loadAll() {
        	EnseignantStage.query(function(result) {
                vm.stages = result;
            }).$promise.then(function (res) {
                // Content loaded
                vm.stagesCSV = ExportCSV.convertStagesToCSVObject(vm.stages);
            }, function (err) {
                // Error
                console.log(err);
            });
        }

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
