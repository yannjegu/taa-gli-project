(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('EnseignantStageDetailController', EnseignantStageDetailController);

    EnseignantStageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'EnseignantStage', 'Partenaire', 'Enseignant', 'Contact', 'Etudiant', 'ExportCSV'];

    function EnseignantStageDetailController($scope, $rootScope, $stateParams, previousState, entity, EnseignantStage, Partenaire, Enseignant, Contact, Etudiant, ExportCSV) {
        var vm = this;

        vm.stage = entity;
        vm.stageCSV = ExportCSV.convertStageToCSVObject(vm.stage);
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('taagliProjectApp:stageUpdate', function(event, result) {
            vm.stage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
