(function() {
    'use strict';
    angular
        .module('taagliProjectApp')
        .factory('ContactStage', ContactStage);

    ContactStage.$inject = ['$resource', 'DateUtils'];

    function ContactStage ($resource, DateUtils) {
        var resourceUrl =  'api/stages/contact/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.sDateMaj = DateUtils.convertLocalDateFromServer(data.sDateMaj);
                        data.sDateDebut = DateUtils.convertLocalDateFromServer(data.sDateDebut);
                        data.sDateFin = DateUtils.convertLocalDateFromServer(data.sDateFin);
                    }
                    return data;
                }
            }
        });
    }
})();
