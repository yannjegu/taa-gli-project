(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('contactStage', {
            parent: 'app',
            url: '/contact/stage',
            data: {
                authorities: ['ROLE_ENTERPRISE'],
                pageTitle: 'Stages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/contact/stage/contactStage.html',
                    controller: 'ContactStageController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('contactStage-detail', {
            parent: 'app',
            url: '/contact/stage/{id}',
            data: {
                authorities: ['ROLE_ENTERPRISE'],
                pageTitle: 'Stage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/contact/stage/contactStage-detail.html',
                    controller: 'ContactStageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'ContactStage', function($stateParams, ContactStage) {
                    return ContactStage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'contactStage',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        });
    }

})();
