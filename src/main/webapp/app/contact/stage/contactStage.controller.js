(function() {
    'use strict';

    angular
        .module('taagliProjectApp')
        .controller('ContactStageController', ContactStageController);

    ContactStageController.$inject = ['$scope', '$state', '$filter', 'ContactStage', 'ExportCSV'];

    function ContactStageController ($scope, $state, $filter, ContactStage, ExportCSV) {
        var vm = this;

        vm.stages = [];
        vm.stagesCSV = [];



        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;

        $scope.sortType     = 'sDateDebut';
        $scope.sortReverse  = true;
        vm.filter           = '';

        loadAll();

        function loadAll() {
        	ContactStage.query(function(result) {
                vm.stages = result;
            }).$promise.then(function (res) {
                // Content loaded
                vm.stagesCSV = ExportCSV.convertStagesEtudiantToCSVObject(vm.stages);
            }, function (err) {
                // Error
                console.log(err);
            });
        }

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
