angular
    .module('taagliProjectApp')
    .filter('csvUniversiteStage', function() {
        function csvStageFilter(input) {
            var out = [];
            if(input.length == 0) return;
            for(var i = 0; i < input.length; i++) {
                out.push({
                    id: input[i].id,
                    nom_etudiant: input[i].aIdent.aNomUsuel,
                    prenom_etudiant: input[i].aIdent.aPrenoms,
                    sesame_etudiant: input[i].aIdent.aNumSeq,
                    partenaire: input[i].pIdent.pNompart,
                    enseignant: input[i].enIdent.enSesame,
                    etat_convention: input[i].scIdent.libelle,
                    date_modification: input[i].sDateMaj,
                    auteur_modification: input[i].sMaj
                });
            }
            return out;
        }

        return csvStageFilter;
});
