angular
    .module('taagliProjectApp')
    .filter('csvEtudiantStage', function() {
        function csvStageFilter(input) {
            var out = [];
            if(input.length == 0) return;
            for(var i = 0; i < input.length; i++) {
                out.push({
                    id: input[i].id,
                    sujet: input[i].sSujet,
                    partenaire: input[i].pIdent.pNompart,
                    service: input[i].pIdent.pService,
                    date_debut: input[i].sDateDebut,
                    date_fin: input[i].sDateFin,
                    etat_convention: input[i].scIdent.libelle,
                    date_modification: input[i].sDateMaj,
                    auteur_modification: input[i].sMaj
                });
            }
            return out;
        }

        return csvStageFilter;
});
