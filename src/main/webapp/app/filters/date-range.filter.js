angular
    .module('taagliProjectApp')
    .filter('dateRange', function() {
        function dateRangeFilter(input, fromDate, toDate) {
            var filtered = [];
            var from_date = Date.parse(fromDate) || 0;
            var to_date = Date.parse(toDate) || 32472144000000;
            angular.forEach(input, function(item) {
                if(Date.parse(item.sDateDebut) >= from_date && Date.parse(item.sDateFin) <= to_date) {
                    filtered.push(item);
                }
            });
            return filtered;
        }

        return dateRangeFilter;
});
