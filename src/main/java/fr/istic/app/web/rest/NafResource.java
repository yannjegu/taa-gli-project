package fr.istic.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.istic.app.domain.Naf;
import fr.istic.app.repository.NafRepository;
import fr.istic.app.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Naf.
 */
@RestController
@RequestMapping("/api")
public class NafResource {

    private final Logger log = LoggerFactory.getLogger(NafResource.class);
        
    @Inject
    private NafRepository nafRepository;

    /**
     * POST  /nafs : Create a new naf.
     *
     * @param naf the naf to create
     * @return the ResponseEntity with status 201 (Created) and with body the new naf, or with status 400 (Bad Request) if the naf has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/nafs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Naf> createNaf(@Valid @RequestBody Naf naf) throws URISyntaxException {
        log.debug("REST request to save Naf : {}", naf);
        if (naf.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("naf", "idexists", "A new naf cannot already have an ID")).body(null);
        }
        Naf result = nafRepository.save(naf);
        return ResponseEntity.created(new URI("/api/nafs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("naf", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /nafs : Updates an existing naf.
     *
     * @param naf the naf to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated naf,
     * or with status 400 (Bad Request) if the naf is not valid,
     * or with status 500 (Internal Server Error) if the naf couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/nafs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Naf> updateNaf(@Valid @RequestBody Naf naf) throws URISyntaxException {
        log.debug("REST request to update Naf : {}", naf);
        if (naf.getId() == null) {
            return createNaf(naf);
        }
        Naf result = nafRepository.save(naf);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("naf", naf.getId().toString()))
            .body(result);
    }

    /**
     * GET  /nafs : get all the nafs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of nafs in body
     */
    @RequestMapping(value = "/nafs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Naf> getAllNafs() {
        log.debug("REST request to get all Nafs");
        List<Naf> nafs = nafRepository.findAll();
        return nafs;
    }

    /**
     * GET  /nafs/:id : get the "id" naf.
     *
     * @param id the id of the naf to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the naf, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/nafs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Naf> getNaf(@PathVariable Long id) {
        log.debug("REST request to get Naf : {}", id);
        Naf naf = nafRepository.findOne(id);
        return Optional.ofNullable(naf)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /nafs/:id : delete the "id" naf.
     *
     * @param id the id of the naf to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/nafs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteNaf(@PathVariable Long id) {
        log.debug("REST request to delete Naf : {}", id);
        nafRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("naf", id.toString())).build();
    }

}
