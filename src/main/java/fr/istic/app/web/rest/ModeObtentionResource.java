package fr.istic.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.istic.app.domain.ModeObtention;
import fr.istic.app.repository.ModeObtentionRepository;
import fr.istic.app.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ModeObtention.
 */
@RestController
@RequestMapping("/api")
public class ModeObtentionResource {

    private final Logger log = LoggerFactory.getLogger(ModeObtentionResource.class);
        
    @Inject
    private ModeObtentionRepository modeObtentionRepository;

    /**
     * POST  /mode-obtentions : Create a new modeObtention.
     *
     * @param modeObtention the modeObtention to create
     * @return the ResponseEntity with status 201 (Created) and with body the new modeObtention, or with status 400 (Bad Request) if the modeObtention has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/mode-obtentions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ModeObtention> createModeObtention(@Valid @RequestBody ModeObtention modeObtention) throws URISyntaxException {
        log.debug("REST request to save ModeObtention : {}", modeObtention);
        if (modeObtention.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("modeObtention", "idexists", "A new modeObtention cannot already have an ID")).body(null);
        }
        ModeObtention result = modeObtentionRepository.save(modeObtention);
        return ResponseEntity.created(new URI("/api/mode-obtentions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("modeObtention", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /mode-obtentions : Updates an existing modeObtention.
     *
     * @param modeObtention the modeObtention to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated modeObtention,
     * or with status 400 (Bad Request) if the modeObtention is not valid,
     * or with status 500 (Internal Server Error) if the modeObtention couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/mode-obtentions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ModeObtention> updateModeObtention(@Valid @RequestBody ModeObtention modeObtention) throws URISyntaxException {
        log.debug("REST request to update ModeObtention : {}", modeObtention);
        if (modeObtention.getId() == null) {
            return createModeObtention(modeObtention);
        }
        ModeObtention result = modeObtentionRepository.save(modeObtention);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("modeObtention", modeObtention.getId().toString()))
            .body(result);
    }

    /**
     * GET  /mode-obtentions : get all the modeObtentions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of modeObtentions in body
     */
    @RequestMapping(value = "/mode-obtentions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ModeObtention> getAllModeObtentions() {
        log.debug("REST request to get all ModeObtentions");
        List<ModeObtention> modeObtentions = modeObtentionRepository.findAll();
        return modeObtentions;
    }

    /**
     * GET  /mode-obtentions/:id : get the "id" modeObtention.
     *
     * @param id the id of the modeObtention to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the modeObtention, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/mode-obtentions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ModeObtention> getModeObtention(@PathVariable Long id) {
        log.debug("REST request to get ModeObtention : {}", id);
        ModeObtention modeObtention = modeObtentionRepository.findOne(id);
        return Optional.ofNullable(modeObtention)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /mode-obtentions/:id : delete the "id" modeObtention.
     *
     * @param id the id of the modeObtention to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/mode-obtentions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteModeObtention(@PathVariable Long id) {
        log.debug("REST request to delete ModeObtention : {}", id);
        modeObtentionRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("modeObtention", id.toString())).build();
    }

}
