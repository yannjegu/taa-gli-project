package fr.istic.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.istic.app.domain.DipISTIC;
import fr.istic.app.repository.DipISTICRepository;
import fr.istic.app.web.rest.util.HeaderUtil;

/**
 * REST controller for managing DipISTIC.
 */
@RestController
@RequestMapping("/api")
public class DipISTICResource {

    private final Logger log = LoggerFactory.getLogger(DipISTICResource.class);
        
    @Inject
    private DipISTICRepository dipISTICRepository;

    /**
     * POST  /dip-istics : Create a new dipISTIC.
     *
     * @param dipISTIC the dipISTIC to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dipISTIC, or with status 400 (Bad Request) if the dipISTIC has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dip-istics",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DipISTIC> createDipISTIC(@Valid @RequestBody DipISTIC dipISTIC) throws URISyntaxException {
        log.debug("REST request to save DipISTIC : {}", dipISTIC);
        if (dipISTIC.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("dipISTIC", "idexists", "A new dipISTIC cannot already have an ID")).body(null);
        }
        DipISTIC result = dipISTICRepository.save(dipISTIC);
        return ResponseEntity.created(new URI("/api/dip-istics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("dipISTIC", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dip-istics : Updates an existing dipISTIC.
     *
     * @param dipISTIC the dipISTIC to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dipISTIC,
     * or with status 400 (Bad Request) if the dipISTIC is not valid,
     * or with status 500 (Internal Server Error) if the dipISTIC couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dip-istics",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DipISTIC> updateDipISTIC(@Valid @RequestBody DipISTIC dipISTIC) throws URISyntaxException {
        log.debug("REST request to update DipISTIC : {}", dipISTIC);
        if (dipISTIC.getId() == null) {
            return createDipISTIC(dipISTIC);
        }
        DipISTIC result = dipISTICRepository.save(dipISTIC);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("dipISTIC", dipISTIC.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dip-istics : get all the dipISTICS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dipISTICS in body
     */
    @RequestMapping(value = "/dip-istics",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DipISTIC> getAllDipISTICS() {
        log.debug("REST request to get all DipISTICS");
        List<DipISTIC> dipISTICS = dipISTICRepository.findAll();
        return dipISTICS;
    }

    /**
     * GET  /dip-istics/:id : get the "id" dipISTIC.
     *
     * @param id the id of the dipISTIC to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dipISTIC, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/dip-istics/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DipISTIC> getDipISTIC(@PathVariable Long id) {
        log.debug("REST request to get DipISTIC : {}", id);
        DipISTIC dipISTIC = dipISTICRepository.findOne(id);
        return Optional.ofNullable(dipISTIC)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /dip-istics/:id : delete the "id" dipISTIC.
     *
     * @param id the id of the dipISTIC to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/dip-istics/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDipISTIC(@PathVariable Long id) {
        log.debug("REST request to delete DipISTIC : {}", id);
        dipISTICRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("dipISTIC", id.toString())).build();
    }

}
