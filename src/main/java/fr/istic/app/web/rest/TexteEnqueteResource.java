package fr.istic.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.istic.app.domain.TexteEnquete;
import fr.istic.app.repository.TexteEnqueteRepository;
import fr.istic.app.web.rest.util.HeaderUtil;

/**
 * REST controller for managing TexteEnquete.
 */
@RestController
@RequestMapping("/api")
public class TexteEnqueteResource {

    private final Logger log = LoggerFactory.getLogger(TexteEnqueteResource.class);
        
    @Inject
    private TexteEnqueteRepository texteEnqueteRepository;

    /**
     * POST  /texte-enquetes : Create a new texteEnquete.
     *
     * @param texteEnquete the texteEnquete to create
     * @return the ResponseEntity with status 201 (Created) and with body the new texteEnquete, or with status 400 (Bad Request) if the texteEnquete has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/texte-enquetes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TexteEnquete> createTexteEnquete(@Valid @RequestBody TexteEnquete texteEnquete) throws URISyntaxException {
        log.debug("REST request to save TexteEnquete : {}", texteEnquete);
        if (texteEnquete.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("texteEnquete", "idexists", "A new texteEnquete cannot already have an ID")).body(null);
        }
        TexteEnquete result = texteEnqueteRepository.save(texteEnquete);
        return ResponseEntity.created(new URI("/api/texte-enquetes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("texteEnquete", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /texte-enquetes : Updates an existing texteEnquete.
     *
     * @param texteEnquete the texteEnquete to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated texteEnquete,
     * or with status 400 (Bad Request) if the texteEnquete is not valid,
     * or with status 500 (Internal Server Error) if the texteEnquete couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/texte-enquetes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TexteEnquete> updateTexteEnquete(@Valid @RequestBody TexteEnquete texteEnquete) throws URISyntaxException {
        log.debug("REST request to update TexteEnquete : {}", texteEnquete);
        if (texteEnquete.getId() == null) {
            return createTexteEnquete(texteEnquete);
        }
        TexteEnquete result = texteEnqueteRepository.save(texteEnquete);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("texteEnquete", texteEnquete.getId().toString()))
            .body(result);
    }

    /**
     * GET  /texte-enquetes : get all the texteEnquetes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of texteEnquetes in body
     */
    @RequestMapping(value = "/texte-enquetes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TexteEnquete> getAllTexteEnquetes() {
        log.debug("REST request to get all TexteEnquetes");
        List<TexteEnquete> texteEnquetes = texteEnqueteRepository.findAll();
        return texteEnquetes;
    }

    /**
     * GET  /texte-enquetes/:id : get the "id" texteEnquete.
     *
     * @param id the id of the texteEnquete to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the texteEnquete, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/texte-enquetes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TexteEnquete> getTexteEnquete(@PathVariable Long id) {
        log.debug("REST request to get TexteEnquete : {}", id);
        TexteEnquete texteEnquete = texteEnqueteRepository.findOne(id);
        return Optional.ofNullable(texteEnquete)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /texte-enquetes/:id : delete the "id" texteEnquete.
     *
     * @param id the id of the texteEnquete to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/texte-enquetes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTexteEnquete(@PathVariable Long id) {
        log.debug("REST request to delete TexteEnquete : {}", id);
        texteEnqueteRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("texteEnquete", id.toString())).build();
    }

}
