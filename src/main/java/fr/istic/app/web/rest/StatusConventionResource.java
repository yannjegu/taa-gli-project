package fr.istic.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.istic.app.domain.StatusConvention;

import fr.istic.app.repository.StatusConventionRepository;
import fr.istic.app.web.rest.util.HeaderUtil;
import fr.istic.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing StatusConvention.
 */
@RestController
@RequestMapping("/api")
public class StatusConventionResource {

    private final Logger log = LoggerFactory.getLogger(StatusConventionResource.class);
        
    @Inject
    private StatusConventionRepository statusConventionRepository;

    /**
     * POST  /status-conventions : Create a new statusConvention.
     *
     * @param statusConvention the statusConvention to create
     * @return the ResponseEntity with status 201 (Created) and with body the new statusConvention, or with status 400 (Bad Request) if the statusConvention has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/status-conventions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StatusConvention> createStatusConvention(@RequestBody StatusConvention statusConvention) throws URISyntaxException {
        log.debug("REST request to save StatusConvention : {}", statusConvention);
        if (statusConvention.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("statusConvention", "idexists", "A new statusConvention cannot already have an ID")).body(null);
        }
        StatusConvention result = statusConventionRepository.save(statusConvention);
        return ResponseEntity.created(new URI("/api/status-conventions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("statusConvention", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /status-conventions : Updates an existing statusConvention.
     *
     * @param statusConvention the statusConvention to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated statusConvention,
     * or with status 400 (Bad Request) if the statusConvention is not valid,
     * or with status 500 (Internal Server Error) if the statusConvention couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/status-conventions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StatusConvention> updateStatusConvention(@RequestBody StatusConvention statusConvention) throws URISyntaxException {
        log.debug("REST request to update StatusConvention : {}", statusConvention);
        if (statusConvention.getId() == null) {
            return createStatusConvention(statusConvention);
        }
        StatusConvention result = statusConventionRepository.save(statusConvention);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("statusConvention", statusConvention.getId().toString()))
            .body(result);
    }

    /**
     * GET  /status-conventions : get all the statusConventions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of statusConventions in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/status-conventions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<StatusConvention>> getAllStatusConventions(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of StatusConventions");
        Page<StatusConvention> page = statusConventionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/status-conventions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /status-conventions/:id : get the "id" statusConvention.
     *
     * @param id the id of the statusConvention to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the statusConvention, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/status-conventions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StatusConvention> getStatusConvention(@PathVariable Long id) {
        log.debug("REST request to get StatusConvention : {}", id);
        StatusConvention statusConvention = statusConventionRepository.findOne(id);
        return Optional.ofNullable(statusConvention)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /status-conventions/:id : delete the "id" statusConvention.
     *
     * @param id the id of the statusConvention to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/status-conventions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteStatusConvention(@PathVariable Long id) {
        log.debug("REST request to delete StatusConvention : {}", id);
        statusConventionRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("statusConvention", id.toString())).build();
    }

}
