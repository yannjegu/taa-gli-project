package fr.istic.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.istic.app.domain.Situation;
import fr.istic.app.repository.SituationRepository;
import fr.istic.app.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Situation.
 */
@RestController
@RequestMapping("/api")
public class SituationResource {

    private final Logger log = LoggerFactory.getLogger(SituationResource.class);
        
    @Inject
    private SituationRepository situationRepository;

    /**
     * POST  /situations : Create a new situation.
     *
     * @param situation the situation to create
     * @return the ResponseEntity with status 201 (Created) and with body the new situation, or with status 400 (Bad Request) if the situation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/situations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Situation> createSituation(@Valid @RequestBody Situation situation) throws URISyntaxException {
        log.debug("REST request to save Situation : {}", situation);
        if (situation.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("situation", "idexists", "A new situation cannot already have an ID")).body(null);
        }
        Situation result = situationRepository.save(situation);
        return ResponseEntity.created(new URI("/api/situations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("situation", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /situations : Updates an existing situation.
     *
     * @param situation the situation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated situation,
     * or with status 400 (Bad Request) if the situation is not valid,
     * or with status 500 (Internal Server Error) if the situation couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/situations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Situation> updateSituation(@Valid @RequestBody Situation situation) throws URISyntaxException {
        log.debug("REST request to update Situation : {}", situation);
        if (situation.getId() == null) {
            return createSituation(situation);
        }
        Situation result = situationRepository.save(situation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("situation", situation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /situations : get all the situations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of situations in body
     */
    @RequestMapping(value = "/situations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Situation> getAllSituations() {
        log.debug("REST request to get all Situations");
        List<Situation> situations = situationRepository.findAll();
        return situations;
    }

    /**
     * GET  /situations/:id : get the "id" situation.
     *
     * @param id the id of the situation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the situation, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/situations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Situation> getSituation(@PathVariable Long id) {
        log.debug("REST request to get Situation : {}", id);
        Situation situation = situationRepository.findOne(id);
        return Optional.ofNullable(situation)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /situations/:id : delete the "id" situation.
     *
     * @param id the id of the situation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/situations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSituation(@PathVariable Long id) {
        log.debug("REST request to delete Situation : {}", id);
        situationRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("situation", id.toString())).build();
    }

}
