package fr.istic.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.istic.app.domain.Contact;
import fr.istic.app.domain.Enseignant;
import fr.istic.app.domain.Etudiant;
import fr.istic.app.domain.Stage;
import fr.istic.app.domain.StatusConvention;
import fr.istic.app.repository.StageRepository;
import fr.istic.app.repository.StatusConventionRepository;
import fr.istic.app.service.UserService;
import fr.istic.app.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Stage.
 */
@RestController
@RequestMapping("/api")
public class StageResource {
	
	private static final long STATUT_ENREGISTRE = 1;
	private static final long STATUT_VALIDE = 2;
	private static final long STATUT_SIGNEE = 4;

	private final Logger log = LoggerFactory.getLogger(StageResource.class);

	@Inject
	private StageRepository stageRepository;
	
	@Inject
	private StatusConventionRepository statusConventionRepository;

	@Inject
	private UserService userService;

	// -----------------------ADMIN ONLY SECTION-----------------------

	/**
	 * POST /stages : Create a new stage.
	 *
	 * @param stage
	 *            the stage to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new stage, or with status 400 (Bad Request) if the stage has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/stages", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> createStage(@Valid @RequestBody Stage stage) throws URISyntaxException {
		log.debug("REST request to save Stage : {}", stage);
		if (stage.getId() != null) {
			return ResponseEntity.badRequest()
					.headers(
							HeaderUtil.createFailureAlert("stage", "idexists", "A new stage cannot already have an ID"))
					.body(null);
		}
		Stage result = stageRepository.save(stage);
		return ResponseEntity.created(new URI("/api/stages/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("stage", result.getId().toString())).body(result);
	}

	/**
	 * PUT /stages : Updates an existing stage.
	 *
	 * @param stage
	 *            the stage to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         stage, or with status 400 (Bad Request) if the stage is not
	 *         valid, or with status 500 (Internal Server Error) if the stage
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/stages", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> updateStage(@Valid @RequestBody Stage stage) throws URISyntaxException {
		log.debug("REST request to update Stage : {}", stage);
		if (stage.getId() == null) {
			return createStage(stage);
		}
		Stage result = stageRepository.save(stage);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("stage", stage.getId().toString()))
				.body(result);
	}

	/**
	 * GET /stages : get all the stages.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of stages in
	 *         body
	 */
	@RequestMapping(value = "/stages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Stage> getAllStages() {
		log.debug("REST request to get all Stages");
		List<Stage> stages = stageRepository.findAll();
		return stages;
	}

	/**
	 * GET /stages/:id : get the "id" stage.
	 *
	 * @param id
	 *            the id of the stage to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the stage,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/stages/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> getStage(@PathVariable Long id) {
		log.debug("REST request to get Stage : {}", id);
		Stage stage = stageRepository.findOne(id);
		return Optional.ofNullable(stage).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /stages/:id : delete the "id" stage.
	 *
	 * @param id
	 *            the id of the stage to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/stages/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteStage(@PathVariable Long id) {
		log.debug("REST request to delete Stage : {}", id);
		stageRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("stage", id.toString())).build();
	}

	// -----------------------STUDENT ONLY SECTION-----------------------

	/**
	 * POST /stages/etudiant : Create a new stage for current student.
	 *
	 * @param stage
	 *            the stage to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new stage, or with status 400 (Bad Request) if the stage has
	 *         already an ID or the user is not a student
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/stages/etudiant", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> createStageForStudent(@RequestBody Stage stage) throws URISyntaxException {
		log.debug("REST request to save Stage for current user : {}", stage);
		log.error("ERROR---------------------------------------nn\n\n\n\n");
		if (stage.getId() != null) {
			return ResponseEntity.badRequest()
					.headers(
							HeaderUtil.createFailureAlert("stage", "idexists", "A new stage cannot already have an ID"))
					.body(null);
		}

		Etudiant etudiant = userService.getEtudiantForCurrentUser();
		if (etudiant == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "notstudent", "The current user is not a student"))
					.body(null);
		}
		
		StatusConvention statusConvention = statusConventionRepository.getOne(STATUT_ENREGISTRE);

		stage.setaIdent(etudiant);
		stage.setscIdent(statusConvention);
		
		stage.setsNomPart(stage.getpIdent().getpNompart());
		stage.setsAdresse(stage.getpIdent().getpRue());
		stage.setsCpltAdresse(stage.getpIdent().getpCpltRue());
		
		Stage result = stageRepository.save(stage);
		return ResponseEntity.created(new URI("/api/stages/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("stage", result.getId().toString())).body(result);
	}
	
	/**
	 * PUT /stages/etudiant : Updates an existing stage for current student.
	 *
	 * @param stage
	 *            the stage to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         stage, or with status 400 (Bad Request) if the stage is not
	 *         valid, or with status 500 (Internal Server Error) if the stage
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/stages/etudiant", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> updateStageForStudent(@Valid @RequestBody Stage stage) throws URISyntaxException {
		log.debug("REST request to update Stage for current user : {}", stage);
		
		Etudiant etudiant = userService.getEtudiantForCurrentUser();
		if (etudiant == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "notstudent", "The current user is not a student"))
					.body(null);
		}
		
		List<Stage> studentStages = stageRepository.findByAIdent(etudiant);
		
		if(!studentStages.contains(stage)){
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "badstage", "The current student does not possess this internship"))
					.body(null);
		}
		
		StatusConvention statusConvention = statusConventionRepository.getOne(STATUT_ENREGISTRE);
		if(!stage.getscIdent().equals(statusConvention)){
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "updateunallowed", "The current stage is already validated"))
					.body(null);
		}
		
		if (stage.getId() == null) {
			return createStageForStudent(stage);
		}
		
		stage.setsNomPart(stage.getpIdent().getpNompart());
		stage.setsAdresse(stage.getpIdent().getpRue());
		stage.setsCpltAdresse(stage.getpIdent().getpCpltRue());
		
		Stage result = stageRepository.save(stage);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("stage", stage.getId().toString()))
				.body(result);
	}
	
	/**
	 * GET /stages/etudiant : get all the stages for the current user.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of stages in
	 *         body
	 */
	@RequestMapping(value = "/stages/etudiant", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Stage> getStagesForStudent() {
		log.debug("REST request to get Stages for current user");
		
		Etudiant etudiant = userService.getEtudiantForCurrentUser();
		
		List<Stage> stages = stageRepository.findByAIdent(etudiant);
		return stages;
	}
	
	/**
	 * GET /stages/etudiant/:id : get the "id" stage for the current user.
	 *
	 * @param id
	 *            the id of the stage to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the stage,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/stages/etudiant/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> getStageForStudent(@PathVariable Long id) {
		log.debug("REST request to get Stage : {}", id);
		
		Etudiant etudiant = userService.getEtudiantForCurrentUser();
		if (etudiant == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "notstudent", "The current user is not a student"))
					.body(null);
		}
		
		Stage stage = stageRepository.findOne(id);
		
		List<Stage> studentStages = stageRepository.findByAIdent(etudiant);
		
		if(!studentStages.contains(stage)){
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "badstage", "The current student does not possess this internship"))
					.body(null);
		}
		
		return Optional.ofNullable(stage).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	/**
	 * DELETE /stages/student/:id : delete the "id" stage for the current user.
	 *
	 * @param id
	 *            the id of the stage to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/stages/student/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteStageForStudent(@PathVariable Long id) {
		log.debug("REST request to delete Stage : {}", id);
		
		Etudiant etudiant = userService.getEtudiantForCurrentUser();
		if (etudiant == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "notstudent", "The current user is not a student"))
					.body(null);
		}
		
		Stage stage = stageRepository.findOne(id);
		
		List<Stage> studentStages = stageRepository.findByAIdent(etudiant);
		
		if(!studentStages.contains(stage)){
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "badstage", "The current student does not possess this internship"))
					.body(null);
		}
		StatusConvention statusConvention = statusConventionRepository.getOne(STATUT_ENREGISTRE);
		if(!stage.getscIdent().equals(statusConvention)){
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "updateunallowed", "The current stage is already validated"))
					.body(null);
		}
		
		stageRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("stage", id.toString())).build();
	}
	
	// -----------------------CONTACT ONLY SECTION-----------------------

	/**
	 * GET /stages/contact : get all the stages for the current contact.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of stages in
	 *         body
	 */
	@RequestMapping(value = "/stages/contact", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Stage> getStagesForContact() {
		log.debug("REST request to get Stages for current contact");
		Contact contact = userService.getContactForCurrentUser();
		
		StatusConvention statusConvention = statusConventionRepository.getOne(STATUT_SIGNEE);
		
		List<Stage> stages = stageRepository.findByNumSeqRespAndScIdent(contact, statusConvention);
		return stages;
	}
	
	/**
	 * GET /stages/contact/:id : get the "id" stage for the current user.
	 *
	 * @param id
	 *            the id of the stage to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the stage,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/stages/contact/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> getStageForContact(@PathVariable Long id) {
		log.debug("REST request to get Stage : {}", id);
		
		Contact contact = userService.getContactForCurrentUser();
		if (contact == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "notcontact", "The current user is not a contact"))
					.body(null);
		}
		
		Stage stage = stageRepository.findOne(id);
		
		StatusConvention statusConvention = statusConventionRepository.getOne(STATUT_SIGNEE);

		List<Stage> contactStages = stageRepository.findByNumSeqRespAndScIdent(contact, statusConvention);
		
		if(!contactStages.contains(stage)){
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "badstage", "The current contact does not possess this internship"))
					.body(null);
		}
		
		return Optional.ofNullable(stage).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	

	// -----------------------UIVERSITY ONLY SECTION-----------------------

	
	/**
	 * PUT /stages/universite : Updates an existing stage.
	 *
	 * @param stage
	 *            the stage to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         stage, or with status 400 (Bad Request) if the stage is not
	 *         valid, or with status 500 (Internal Server Error) if the stage
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/stages/universite", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> updateStageForUniversity(@Valid @RequestBody Stage stage) throws URISyntaxException {
		log.debug("REST request to update Stage : {}", stage);
		if (stage.getId() == null) {
			return createStage(stage);//ERROR 403 if no admin rights
		}
		Stage result = stageRepository.save(stage);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("stage", stage.getId().toString()))
				.body(result);
	}
	
	

	/**
	 * GET /stages/universite : get all the stages.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of stages in
	 *         body
	 */
	@RequestMapping(value = "/stages/universite", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Stage> getAllStagesForUniversity() {
		log.debug("REST request to get all Stages");
		List<Stage> stages = stageRepository.findAll();
		return stages;
	}
	
	/**
	 * PUT /stages/universite/sign/:id : sign the "id" stage.
	 *
	 * @param id
	 *            the id of the stage to sign
	 * @return the ResponseEntity with status 200 (OK) and with body the stage,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/stages/universite/sign", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> signStageForUniversity(@RequestBody Long id) {
		log.debug("REST request to sign Stage : {}", id);
		Stage stage = stageRepository.findOne(id);
		
		StatusConvention statusConvention = statusConventionRepository.getOne(STATUT_SIGNEE);

		stage.setscIdent(statusConvention);
		Stage result = stageRepository.save(stage);
		
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("stage", stage.getId().toString()))
				.body(result);
	}

	/**
	 * GET /stages/universite/:id : get the "id" stage.
	 *
	 * @param id
	 *            the id of the stage to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the stage,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/stages/universite/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> getStageForUniversity(@PathVariable Long id) {
		log.debug("REST request to get Stage : {}", id);
		Stage stage = stageRepository.findOne(id);
		return Optional.ofNullable(stage).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /stages/universite/:id : delete the "id" stage.
	 *
	 * @param id
	 *            the id of the stage to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/stages/universite/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteStageForUniversity(@PathVariable Long id) {
		log.debug("REST request to delete Stage : {}", id);
		stageRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("stage", id.toString())).build();
	}
	
	// -----------------------RESPONSABLE TEACHER ONLY SECTION-----------------------

	
	/**
	 * PUT /stages/responsable : Updates an existing stage.
	 *
	 * @param stage
	 *            the stage to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         stage, or with status 400 (Bad Request) if the stage is not
	 *         valid, or with status 500 (Internal Server Error) if the stage
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/stages/responsable", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> updateStageForResponsable(@Valid @RequestBody Stage stage) throws URISyntaxException {
		log.debug("REST request to update Stage : {}", stage);
		
		if (stage.getId() == null) {
			return createStage(stage);//ERROR 403 if no admin rights
		}
		//TODO vérifier si le stage a bien un status enregistré avant la validation (le code ci-dessous renvoi une erreur)
		/*StatusConvention statusConvention = statusConventionRepository.getOne(STATUT_ENREGISTRE);
		System.out.println();
		System.out.println(stage.getscIdent().getLibelle());
		System.out.println(statusConvention.getLibelle());
		if(!stage.getscIdent().equals(statusConvention)){
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "updateunallowed", "The current stage is already validated"))
					.body(null);
		}*/
		StatusConvention statusConvention = statusConventionRepository.getOne(STATUT_VALIDE);
		stage.setscIdent(statusConvention);
		Stage result = stageRepository.save(stage);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("stage", stage.getId().toString()))
				.body(result);
	}
	
	/**
	 * GET /stages/universite : get all the stages.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of stages in
	 *         body
	 */
	@RequestMapping(value = "/stages/responsable", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Stage> getAllStagesForResponsable() {
		log.debug("REST request to get all Stages");
				
		StatusConvention statusConvention = statusConventionRepository.getOne(STATUT_ENREGISTRE);

		List<Stage> stages = stageRepository.findByScIdentAndResp(statusConvention, userService.getEnseignantForCurrentUser());
		
		return stages;
	}

	/**
	 * GET /stages/universite/:id : get the "id" stage.
	 *
	 * @param id
	 *            the id of the stage to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the stage,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/stages/responsable/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> getStageForResponsable(@PathVariable Long id) {
		log.debug("REST request to get Stage : {}", id);
		Stage stage = stageRepository.findOne(id);
		return Optional.ofNullable(stage).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	// -----------------------TEACHER ONLY SECTION-----------------------
	
	/**
	 * GET /stages/enseignant : get all the stages.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of stages in
	 *         body
	 */
	@RequestMapping(value = "/stages/enseignant", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Stage> getAllStagesForTeacher() {
		log.debug("REST request to get all Stages for teacher");
				
		List<Stage> stages = stageRepository.findByEnIdent(userService.getEnseignantForCurrentUser());
		
		return stages;
	}

	/**
	 * GET /stages/enseignant/:id : get the "id" stage.
	 *
	 * @param id
	 *            the id of the stage to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the stage,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/stages/enseignant/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Stage> getStageForTeacher(@PathVariable Long id) {
		log.debug("REST request to get Stage : {}", id);

		Enseignant enseignant = userService.getEnseignantForCurrentUser();
		if (enseignant == null) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "notenseignant", "The current user is not a teacher"))
					.body(null);
		}
		
		Stage stage = stageRepository.findOne(id);

		List<Stage> teacherStages = stageRepository.findByEnIdent(enseignant);
		
		if(!teacherStages.contains(stage)){
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("stage", "badstage", "The current teacher does not possess this internship"))
					.body(null);
		}
		return Optional.ofNullable(stage).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
}