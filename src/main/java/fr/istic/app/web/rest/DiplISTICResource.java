package fr.istic.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.istic.app.domain.DiplISTIC;
import fr.istic.app.repository.DiplISTICRepository;
import fr.istic.app.web.rest.util.HeaderUtil;

/**
 * REST controller for managing DiplISTIC.
 */
@RestController
@RequestMapping("/api")
public class DiplISTICResource {

    private final Logger log = LoggerFactory.getLogger(DiplISTICResource.class);
        
    @Inject
    private DiplISTICRepository diplISTICRepository;

    /**
     * POST  /dipl-istics : Create a new diplISTIC.
     *
     * @param diplISTIC the diplISTIC to create
     * @return the ResponseEntity with status 201 (Created) and with body the new diplISTIC, or with status 400 (Bad Request) if the diplISTIC has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dipl-istics",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DiplISTIC> createDiplISTIC(@Valid @RequestBody DiplISTIC diplISTIC) throws URISyntaxException {
        log.debug("REST request to save DiplISTIC : {}", diplISTIC);
        if (diplISTIC.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("diplISTIC", "idexists", "A new diplISTIC cannot already have an ID")).body(null);
        }
        DiplISTIC result = diplISTICRepository.save(diplISTIC);
        return ResponseEntity.created(new URI("/api/dipl-istics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("diplISTIC", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dipl-istics : Updates an existing diplISTIC.
     *
     * @param diplISTIC the diplISTIC to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated diplISTIC,
     * or with status 400 (Bad Request) if the diplISTIC is not valid,
     * or with status 500 (Internal Server Error) if the diplISTIC couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dipl-istics",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DiplISTIC> updateDiplISTIC(@Valid @RequestBody DiplISTIC diplISTIC) throws URISyntaxException {
        log.debug("REST request to update DiplISTIC : {}", diplISTIC);
        if (diplISTIC.getId() == null) {
            return createDiplISTIC(diplISTIC);
        }
        DiplISTIC result = diplISTICRepository.save(diplISTIC);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("diplISTIC", diplISTIC.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dipl-istics : get all the diplISTICS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of diplISTICS in body
     */
    @RequestMapping(value = "/dipl-istics",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DiplISTIC> getAllDiplISTICS() {
        log.debug("REST request to get all DiplISTICS");
        List<DiplISTIC> diplISTICS = diplISTICRepository.findAll();
        return diplISTICS;
    }

    /**
     * GET  /dipl-istics/:id : get the "id" diplISTIC.
     *
     * @param id the id of the diplISTIC to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the diplISTIC, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/dipl-istics/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DiplISTIC> getDiplISTIC(@PathVariable Long id) {
        log.debug("REST request to get DiplISTIC : {}", id);
        DiplISTIC diplISTIC = diplISTICRepository.findOne(id);
        return Optional.ofNullable(diplISTIC)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /dipl-istics/:id : delete the "id" diplISTIC.
     *
     * @param id the id of the diplISTIC to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/dipl-istics/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDiplISTIC(@PathVariable Long id) {
        log.debug("REST request to delete DiplISTIC : {}", id);
        diplISTICRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("diplISTIC", id.toString())).build();
    }

}
