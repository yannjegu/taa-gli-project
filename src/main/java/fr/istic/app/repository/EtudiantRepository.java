package fr.istic.app.repository;

import fr.istic.app.domain.Etudiant;
import fr.istic.app.domain.User;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Etudiant entity.
 */
@SuppressWarnings("unused")
public interface EtudiantRepository extends JpaRepository<Etudiant,Long> {
	
	public Etudiant findOneByUser(User user);

}
