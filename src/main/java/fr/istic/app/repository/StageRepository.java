package fr.istic.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.istic.app.domain.Contact;
import fr.istic.app.domain.Enseignant;
import fr.istic.app.domain.Etudiant;
import fr.istic.app.domain.Stage;
import fr.istic.app.domain.StatusConvention;

/**
 * Spring Data JPA repository for the Stage entity.
 */
public interface StageRepository extends JpaRepository<Stage,Long> {

	List<Stage> findByAIdent(Etudiant e);
	
	List<Stage> findByNumSeqRespAndScIdent(Contact c, StatusConvention s);
	
	List<Stage> findByEnIdent(Enseignant e);
	
	@Query("SELECT s FROM Stage s inner join s.aIdent std inner join std.etDiplome dip WHERE s.scIdent = :sC AND dip.enIdent = :resp")
	List<Stage> findByScIdentAndResp(@Param("sC")StatusConvention sC, @Param("resp") Enseignant resp);

}
