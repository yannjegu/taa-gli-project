package fr.istic.app.repository;

import fr.istic.app.domain.Naf;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Naf entity.
 */
@SuppressWarnings("unused")
public interface NafRepository extends JpaRepository<Naf,Long> {

}
