package fr.istic.app.repository;

import fr.istic.app.domain.ModeObtention;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ModeObtention entity.
 */
@SuppressWarnings("unused")
public interface ModeObtentionRepository extends JpaRepository<ModeObtention,Long> {

}
