package fr.istic.app.repository;

import fr.istic.app.domain.DiplISTIC;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the DiplISTIC entity.
 */
@SuppressWarnings("unused")
public interface DiplISTICRepository extends JpaRepository<DiplISTIC,Long> {

}
