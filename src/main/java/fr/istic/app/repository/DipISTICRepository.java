package fr.istic.app.repository;

import fr.istic.app.domain.DipISTIC;
import fr.istic.app.domain.Enseignant;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the DipISTIC entity.
 */
public interface DipISTICRepository extends JpaRepository<DipISTIC,Long> {

}
