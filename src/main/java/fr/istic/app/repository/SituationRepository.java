package fr.istic.app.repository;

import fr.istic.app.domain.Situation;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Situation entity.
 */
@SuppressWarnings("unused")
public interface SituationRepository extends JpaRepository<Situation,Long> {

}
