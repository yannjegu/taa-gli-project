package fr.istic.app.repository;

import fr.istic.app.domain.Partenaire;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Partenaire entity.
 */
@SuppressWarnings("unused")
public interface PartenaireRepository extends JpaRepository<Partenaire,Long> {

}
