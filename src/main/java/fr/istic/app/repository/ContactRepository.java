package fr.istic.app.repository;

import fr.istic.app.domain.Contact;
import fr.istic.app.domain.User;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Contact entity.
 */
@SuppressWarnings("unused")
public interface ContactRepository extends JpaRepository<Contact,Long> {
	
	public Contact findOneByUser(User user);

}
