package fr.istic.app.repository;

import fr.istic.app.domain.StatusConvention;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the StatusConvention entity.
 */
@SuppressWarnings("unused")
public interface StatusConventionRepository extends JpaRepository<StatusConvention,Long> {

}
