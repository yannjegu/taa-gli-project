package fr.istic.app.repository;

import fr.istic.app.domain.Enseignant;
import fr.istic.app.domain.User;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Enseignant entity.
 */
@SuppressWarnings("unused")
public interface EnseignantRepository extends JpaRepository<Enseignant,Long> {
	
	public Enseignant findOneByUser(User user);

}
