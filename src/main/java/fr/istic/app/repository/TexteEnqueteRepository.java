package fr.istic.app.repository;

import fr.istic.app.domain.TexteEnquete;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TexteEnquete entity.
 */
@SuppressWarnings("unused")
public interface TexteEnqueteRepository extends JpaRepository<TexteEnquete,Long> {

}
