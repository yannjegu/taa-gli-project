package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ModeObtention.
 */
@Entity
@Table(name = "mode_obtention")
@AttributeOverride(name = "id", column = @Column(name = "obt_code"))
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ModeObtention implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "obt_ordre")
    private String obtOrdre;

    @NotNull
    @Column(name = "obt_lib_c")
    private String obtLibC;

    @Column(name = "obt_description")
    private String obtDescription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObtOrdre() {
        return obtOrdre;
    }

    public ModeObtention obtOrdre(String obtOrdre) {
        this.obtOrdre = obtOrdre;
        return this;
    }

    public void setObtOrdre(String obtOrdre) {
        this.obtOrdre = obtOrdre;
    }

    public String getObtLibC() {
        return obtLibC;
    }

    public ModeObtention obtLibC(String obtLibC) {
        this.obtLibC = obtLibC;
        return this;
    }

    public void setObtLibC(String obtLibC) {
        this.obtLibC = obtLibC;
    }

    public String getObtDescription() {
        return obtDescription;
    }

    public ModeObtention obtDescription(String obtDescription) {
        this.obtDescription = obtDescription;
        return this;
    }

    public void setObtDescription(String obtDescription) {
        this.obtDescription = obtDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModeObtention modeObtention = (ModeObtention) o;
        if(modeObtention.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, modeObtention.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ModeObtention{" +
            "id=" + id +
            ", obtOrdre='" + obtOrdre + "'" +
            ", obtLibC='" + obtLibC + "'" +
            ", obtDescription='" + obtDescription + "'" +
            '}';
    }
}
