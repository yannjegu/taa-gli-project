package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DipISTIC.
 */
@Entity
@Table(name = "dip_istic")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DipISTIC implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "di_libelle")
    private String diLibelle;

    @NotNull
    @Column(name = "di_duree")
    private String diDuree;

    @Column(name = "di_libc")
    private String diLibc;

    @OneToOne
    @JoinColumn(unique = true)
    private Enseignant enIdent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiLibelle() {
        return diLibelle;
    }

    public DipISTIC diLibelle(String diLibelle) {
        this.diLibelle = diLibelle;
        return this;
    }

    public void setDiLibelle(String diLibelle) {
        this.diLibelle = diLibelle;
    }

    public String getDiDuree() {
        return diDuree;
    }

    public DipISTIC diDuree(String diDuree) {
        this.diDuree = diDuree;
        return this;
    }

    public void setDiDuree(String diDuree) {
        this.diDuree = diDuree;
    }

    public String getDiLibc() {
        return diLibc;
    }

    public DipISTIC diLibc(String diLibc) {
        this.diLibc = diLibc;
        return this;
    }

    public void setDiLibc(String diLibc) {
        this.diLibc = diLibc;
    }

    public Enseignant getEnIdent() {
        return enIdent;
    }

    public DipISTIC enIdent(Enseignant enseignant) {
        this.enIdent = enseignant;
        return this;
    }

    public void setEnIdent(Enseignant enseignant) {
        this.enIdent = enseignant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DipISTIC dipISTIC = (DipISTIC) o;
        if(dipISTIC.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, dipISTIC.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DipISTIC{" +
            "id=" + id +
            ", diLibelle='" + diLibelle + "'" +
            ", diDuree='" + diDuree + "'" +
            ", diLibc='" + diLibc + "'" +
            '}';
    }
}
