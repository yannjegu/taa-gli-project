package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Filiere.
 */
@Entity
@Table(name = "filiere")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Filiere implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "fi_libelle")
    private String fiLibelle;

    @OneToOne
    @JoinColumn(unique = true)
    private Enseignant enIdent;

    @OneToOne
    @JoinColumn(unique = true)
    private Enseignant sValideur;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFiLibelle() {
        return fiLibelle;
    }

    public Filiere fiLibelle(String fiLibelle) {
        this.fiLibelle = fiLibelle;
        return this;
    }

    public void setFiLibelle(String fiLibelle) {
        this.fiLibelle = fiLibelle;
    }

    public Enseignant getEnIdent() {
        return enIdent;
    }

    public Filiere enIdent(Enseignant enseignant) {
        this.enIdent = enseignant;
        return this;
    }

    public void setEnIdent(Enseignant enseignant) {
        this.enIdent = enseignant;
    }

    public Enseignant getSValideur() {
        return sValideur;
    }

    public Filiere sValideur(Enseignant enseignant) {
        this.sValideur = enseignant;
        return this;
    }

    public void setSValideur(Enseignant enseignant) {
        this.sValideur = enseignant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Filiere filiere = (Filiere) o;
        if(filiere.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, filiere.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Filiere{" +
            "id=" + id +
            ", fiLibelle='" + fiLibelle + "'" +
            '}';
    }
}
