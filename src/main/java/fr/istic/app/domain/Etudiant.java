package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Etudiant.
 */
@Entity
@Table(name = "etudiant")
@AttributeOverride(name = "id", column = @Column(name = "a_ident"))
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Etudiant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "a_num_seq")
    private String aNumSeq;

    @Column(name = "a_sexe")
    private String aSexe;

    @NotNull
    @Column(name = "a_nom_patronymique")
    private String aNomPatronymique;

    @NotNull
    @Column(name = "a_nom_usuel")
    private String aNomUsuel;

    @NotNull
    @Size(max = 50)
    @Column(name = "a_prenoms")
    private String aPrenoms;

    @Column(name = "a_prenom_2")
    private String aPrenom2;

    @Column(name = "a_rue")
    private String aRue;

    @Column(name = "a_cplt_rue")
    private String aCpltRue;

    @Column(name = "a_ville")
    private String aVille;

    @Column(name = "a_codep")
    private String aCodep;

    @Column(name = "a_tel_perso")
    private String aTelPerso;

    @Column(name = "a_tel_mob")
    private String aTelMob;

    @Column(name = "an_tel_dir")
    private String anTelDir;
    
    @NotNull
    @Size(max = 100)
    @Column(name = "a_ad_elec")
    private String aAdElec;
    
    @NotNull
    @Size(max = 100)
    @Column(name = "et_ad_elec")
    private String etAdElec;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "et_diplome_id", referencedColumnName = "id")
    private DipISTIC etDiplome;

    @Column(name = "a_premiere_inscription")
    private String aPremiereInscription;

    @Column(name = "et_td")
    private String etTd;

    @Column(name = "et_tp")
    private String etTp;

    @Column(name = "et_autre")
    private String etAutre;

    @Column(name = "et_dispense_stage")
    private String etDispenseStage;

    @Column(name = "an_etat")
    private String anEtat;

    @Column(name = "an_rech_emp")
    private String anRechEmp;

    @Column(name = "a_operat")
    private String aOperat;

    @Column(name = "a_date_maj")
    private LocalDate aDateMaj;

    @Column(name = "a_maj")
    private String aMaj;

    @OneToOne
    @JoinColumn(unique = true, name = "an_premier_emploi_id", referencedColumnName = "enq_ident")
    private Enquete anPremierEmploi;

    @OneToOne
    @JoinColumn(unique = true, name = "an_derniere_situation_id", referencedColumnName = "enq_ident")
    private Enquete anDerniereSituation;

    @OneToOne
    @NotNull
    @JoinColumn(unique = true, name = "user_id", referencedColumnName = "id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getaNumSeq() {
        return aNumSeq;
    }

    public Etudiant aNumSeq(String aNumSeq) {
        this.aNumSeq = aNumSeq;
        return this;
    }

    public void setaNumSeq(String aNumSeq) {
        this.aNumSeq = aNumSeq;
    }

    public String getaSexe() {
        return aSexe;
    }

    public Etudiant aSexe(String aSexe) {
        this.aSexe = aSexe;
        return this;
    }

    public void setaSexe(String aSexe) {
        this.aSexe = aSexe;
    }

    public String getaNomPatronymique() {
        return aNomPatronymique;
    }

    public Etudiant aNomPatronymique(String aNomPatronymique) {
        this.aNomPatronymique = aNomPatronymique;
        return this;
    }

    public void setaNomPatronymique(String aNomPatronymique) {
        this.aNomPatronymique = aNomPatronymique;
    }

    public String getaNomUsuel() {
        return aNomUsuel;
    }

    public Etudiant aNomUsuel(String aNomUsuel) {
        this.aNomUsuel = aNomUsuel;
        return this;
    }

    public void setaNomUsuel(String aNomUsuel) {
        this.aNomUsuel = aNomUsuel;
    }

    public String getaPrenoms() {
        return aPrenoms;
    }

    public Etudiant aPrenoms(String aPrenoms) {
        this.aPrenoms = aPrenoms;
        return this;
    }

    public void setaPrenoms(String aPrenoms) {
        this.aPrenoms = aPrenoms;
    }

    public String getaPrenom2() {
        return aPrenom2;
    }

    public Etudiant aPrenom2(String aPrenom2) {
        this.aPrenom2 = aPrenom2;
        return this;
    }

    public void setaPrenom2(String aPrenom2) {
        this.aPrenom2 = aPrenom2;
    }

    public String getaRue() {
        return aRue;
    }

    public Etudiant aRue(String aRue) {
        this.aRue = aRue;
        return this;
    }

    public void setaRue(String aRue) {
        this.aRue = aRue;
    }

    public String getaCpltRue() {
        return aCpltRue;
    }

    public Etudiant aCpltRue(String aCpltRue) {
        this.aCpltRue = aCpltRue;
        return this;
    }

    public void setaCpltRue(String aCpltRue) {
        this.aCpltRue = aCpltRue;
    }

    public String getaVille() {
        return aVille;
    }

    public Etudiant aVille(String aVille) {
        this.aVille = aVille;
        return this;
    }

    public void setaVille(String aVille) {
        this.aVille = aVille;
    }

    public String getaCodep() {
        return aCodep;
    }

    public Etudiant aCodep(String aCodep) {
        this.aCodep = aCodep;
        return this;
    }

    public void setaCodep(String aCodep) {
        this.aCodep = aCodep;
    }

    public String getaTelPerso() {
        return aTelPerso;
    }

    public Etudiant aTelPerso(String aTelPerso) {
        this.aTelPerso = aTelPerso;
        return this;
    }

    public void setaTelPerso(String aTelPerso) {
        this.aTelPerso = aTelPerso;
    }

    public String getaTelMob() {
        return aTelMob;
    }

    public Etudiant aTelMob(String aTelMob) {
        this.aTelMob = aTelMob;
        return this;
    }

    public void setaTelMob(String aTelMob) {
        this.aTelMob = aTelMob;
    }

    public String getAnTelDir() {
        return anTelDir;
    }

    public Etudiant anTelDir(String anTelDir) {
        this.anTelDir = anTelDir;
        return this;
    }

    public void setAnTelDir(String anTelDir) {
        this.anTelDir = anTelDir;
    }

    public String getaAdElec() {
        return aAdElec;
    }

    public Etudiant aAdElec(String aAdElec) {
        this.aAdElec = aAdElec;
        return this;
    }

    public void setaAdElec(String aAdElec) {
        this.aAdElec = aAdElec;
    }

    public String getEtAdElec() {
        return etAdElec;
    }

    public Etudiant etAdElec(String etAdElec) {
        this.etAdElec = etAdElec;
        return this;
    }

    public void setEtAdElec(String etAdElec) {
        this.etAdElec = etAdElec;
    }

    public DipISTIC getEtDiplome() {
        return etDiplome;
    }

    public Etudiant etDiplome(DipISTIC etDiplome) {
        this.etDiplome = etDiplome;
        return this;
    }

    public void setEtDiplome(DipISTIC etDiplome) {
        this.etDiplome = etDiplome;
    }

    public String getaPremiereInscription() {
        return aPremiereInscription;
    }

    public Etudiant aPremiereInscription(String aPremiereInscription) {
        this.aPremiereInscription = aPremiereInscription;
        return this;
    }

    public void setaPremiereInscription(String aPremiereInscription) {
        this.aPremiereInscription = aPremiereInscription;
    }

    public String getEtTd() {
        return etTd;
    }

    public Etudiant etTd(String etTd) {
        this.etTd = etTd;
        return this;
    }

    public void setEtTd(String etTd) {
        this.etTd = etTd;
    }

    public String getEtTp() {
        return etTp;
    }

    public Etudiant etTp(String etTp) {
        this.etTp = etTp;
        return this;
    }

    public void setEtTp(String etTp) {
        this.etTp = etTp;
    }

    public String getEtAutre() {
        return etAutre;
    }

    public Etudiant etAutre(String etAutre) {
        this.etAutre = etAutre;
        return this;
    }

    public void setEtAutre(String etAutre) {
        this.etAutre = etAutre;
    }

    public String getEtDispenseStage() {
        return etDispenseStage;
    }

    public Etudiant etDispenseStage(String etDispenseStage) {
        this.etDispenseStage = etDispenseStage;
        return this;
    }

    public void setEtDispenseStage(String etDispenseStage) {
        this.etDispenseStage = etDispenseStage;
    }

    public String getAnEtat() {
        return anEtat;
    }

    public Etudiant anEtat(String anEtat) {
        this.anEtat = anEtat;
        return this;
    }

    public void setAnEtat(String anEtat) {
        this.anEtat = anEtat;
    }

    public String getAnRechEmp() {
        return anRechEmp;
    }

    public Etudiant anRechEmp(String anRechEmp) {
        this.anRechEmp = anRechEmp;
        return this;
    }

    public void setAnRechEmp(String anRechEmp) {
        this.anRechEmp = anRechEmp;
    }

    public String getaOperat() {
        return aOperat;
    }

    public Etudiant aOperat(String aOperat) {
        this.aOperat = aOperat;
        return this;
    }

    public void setaOperat(String aOperat) {
        this.aOperat = aOperat;
    }

    public LocalDate getaDateMaj() {
        return aDateMaj;
    }

    public Etudiant aDateMaj(LocalDate aDateMaj) {
        this.aDateMaj = aDateMaj;
        return this;
    }

    public void setaDateMaj(LocalDate aDateMaj) {
        this.aDateMaj = aDateMaj;
    }

    public String getaMaj() {
        return aMaj;
    }

    public Etudiant aMaj(String aMaj) {
        this.aMaj = aMaj;
        return this;
    }

    public void setaMaj(String aMaj) {
        this.aMaj = aMaj;
    }

    public Enquete getAnPremierEmploi() {
        return anPremierEmploi;
    }

    public Etudiant anPremierEmploi(Enquete enquete) {
        this.anPremierEmploi = enquete;
        return this;
    }

    public void setAnPremierEmploi(Enquete enquete) {
        this.anPremierEmploi = enquete;
    }

    public Enquete getAnDerniereSituation() {
        return anDerniereSituation;
    }

    public Etudiant anDerniereSituation(Enquete enquete) {
        this.anDerniereSituation = enquete;
        return this;
    }

    public void setAnDerniereSituation(Enquete enquete) {
        this.anDerniereSituation = enquete;
    }
    
    public User getUser() {
    	return user;
    }
    
    public Etudiant user(User user){
    	this.user = user;
    	return this;
    }
    
    public void setUser(User user){
    	this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Etudiant etudiant = (Etudiant) o;
        if(etudiant.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, etudiant.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Etudiant{" +
            "id=" + id +
            ", aNumSeq='" + aNumSeq + "'" +
            ", aSexe='" + aSexe + "'" +
            ", aNomPatronymique='" + aNomPatronymique + "'" +
            ", aNomUsuel='" + aNomUsuel + "'" +
            ", aPrenoms='" + aPrenoms + "'" +
            ", aPrenom2='" + aPrenom2 + "'" +
            ", aRue='" + aRue + "'" +
            ", aCpltRue='" + aCpltRue + "'" +
            ", aVille='" + aVille + "'" +
            ", aCodep='" + aCodep + "'" +
            ", aTelPerso='" + aTelPerso + "'" +
            ", aTelMob='" + aTelMob + "'" +
            ", anTelDir='" + anTelDir + "'" +
            ", aAdElec='" + aAdElec + "'" +
            ", etAdElec='" + etAdElec + "'" +
            ", aPremiereInscription='" + aPremiereInscription + "'" +
            ", etTd='" + etTd + "'" +
            ", etTp='" + etTp + "'" +
            ", etAutre='" + etAutre + "'" +
            ", etDispenseStage='" + etDispenseStage + "'" +
            ", anEtat='" + anEtat + "'" +
            ", anRechEmp='" + anRechEmp + "'" +
            ", aOperat='" + aOperat + "'" +
            ", aDateMaj='" + aDateMaj + "'" +
            ", aMaj='" + aMaj + "'" +
            '}';
    }
}
