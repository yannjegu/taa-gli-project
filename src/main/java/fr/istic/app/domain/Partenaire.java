package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Partenaire.
 */
@Entity
@AttributeOverride(name = "id", column = @Column(name = "p_ident"))
@Table(name = "partenaire")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Partenaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "p_num_seq")
    private Long pNumSeq;

    @Size(min = 14, max = 14)
    @Column(name = "p_siret", length = 14)
    private String pSiret;

    @Column(name = "p_service")
    private String pService;

    @NotNull
    @Column(name = "p_nompart")
    private String pNompart;

    @Column(name = "p_rue")
    private String pRue;

    @Column(name = "p_cplt_rue")
    private String pCpltRue;

    @NotNull
    @Column(name = "p_codep")
    private String pCodep;

    @NotNull
    @Column(name = "p_ville")
    private String pVille;

    @Column(name = "p_tel_std")
    private String pTelStd;

    @Column(name = "p_fax")
    private String pFax;

    @Column(name = "p_url")
    private String pUrl;

    @Column(name = "p_commentaire")
    private String pCommentaire;

    @NotNull
    @Column(name = "e_relation")
    private String eRelation;

    @NotNull
    @Column(name = "e_envoi")
    private String eEnvoi;

    @NotNull
    @Column(name = "e_public_asso")
    private String ePublicAsso;

    @NotNull
    @Column(name = "e_taxe_pro")
    private String eTaxePro;

    @Column(name = "e_der_prop")
    private String eDerProp;

    @Column(name = "e_der_stage")
    private String eDerStage;

    @Column(name = "e_nom_signataire")
    private String eNomSignataire;

    @NotNull
    @Column(name = "e_nbranc")
    private String eNbranc;

    @Column(name = "e_effectif")
    private String eEffectif;

    @NotNull
    @Column(name = "p_operat")
    private String pOperat;

    @NotNull
    @Column(name = "p_date_maj")
    private LocalDate pDateMaj;

    @Column(name = "p_sauvegarde")
    private String pSauvegarde;

    @Column(name = "p_maj")
    private String pMaj;

    @OneToOne
    @JoinColumn(unique = true, name="naf_code_id", referencedColumnName="naf_code")
    private Naf nafCode;

    @ManyToOne
    @NotNull
    @JoinColumn(name="r_codereg_id", referencedColumnName="r_codereg")
    private Region rCodereg;

    @ManyToOne
    @NotNull
    @JoinColumn(name="ta_code_id", referencedColumnName="ta_code")
    private Activite taCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getpNumSeq() {
        return pNumSeq;
    }

    public Partenaire pNumSeq(Long pNumSeq) {
        this.pNumSeq = pNumSeq;
        return this;
    }

    public void setpNumSeq(Long pNumSeq) {
        this.pNumSeq = pNumSeq;
    }

    public String getpSiret() {
        return pSiret;
    }

    public Partenaire pSiret(String pSiret) {
        this.pSiret = pSiret;
        return this;
    }

    public void setpSiret(String pSiret) {
        this.pSiret = pSiret;
    }

    public String getpService() {
        return pService;
    }

    public Partenaire pService(String pService) {
        this.pService = pService;
        return this;
    }

    public void setpService(String pService) {
        this.pService = pService;
    }

    public String getpNompart() {
        return pNompart;
    }

    public Partenaire pNompart(String pNompart) {
        this.pNompart = pNompart;
        return this;
    }

    public void setpNompart(String pNompart) {
        this.pNompart = pNompart;
    }

    public String getpRue() {
        return pRue;
    }

    public Partenaire pRue(String pRue) {
        this.pRue = pRue;
        return this;
    }

    public void setpRue(String pRue) {
        this.pRue = pRue;
    }

    public String getpCpltRue() {
        return pCpltRue;
    }

    public Partenaire pCpltRue(String pCpltRue) {
        this.pCpltRue = pCpltRue;
        return this;
    }

    public void setpCpltRue(String pCpltRue) {
        this.pCpltRue = pCpltRue;
    }

    public String getpCodep() {
        return pCodep;
    }

    public Partenaire pCodep(String pCodep) {
        this.pCodep = pCodep;
        return this;
    }

    public void setpCodep(String pCodep) {
        this.pCodep = pCodep;
    }

    public String getpVille() {
        return pVille;
    }

    public Partenaire pVille(String pVille) {
        this.pVille = pVille;
        return this;
    }

    public void setpVille(String pVille) {
        this.pVille = pVille;
    }

    public String getpTelStd() {
        return pTelStd;
    }

    public Partenaire pTelStd(String pTelStd) {
        this.pTelStd = pTelStd;
        return this;
    }

    public void setpTelStd(String pTelStd) {
        this.pTelStd = pTelStd;
    }

    public String getpFax() {
        return pFax;
    }

    public Partenaire pFax(String pFax) {
        this.pFax = pFax;
        return this;
    }

    public void setpFax(String pFax) {
        this.pFax = pFax;
    }

    public String getpUrl() {
        return pUrl;
    }

    public Partenaire pUrl(String pUrl) {
        this.pUrl = pUrl;
        return this;
    }

    public void setpUrl(String pUrl) {
        this.pUrl = pUrl;
    }

    public String getpCommentaire() {
        return pCommentaire;
    }

    public Partenaire pCommentaire(String pCommentaire) {
        this.pCommentaire = pCommentaire;
        return this;
    }

    public void setpCommentaire(String pCommentaire) {
        this.pCommentaire = pCommentaire;
    }

    public String geteRelation() {
        return eRelation;
    }

    public Partenaire eRelation(String eRelation) {
        this.eRelation = eRelation;
        return this;
    }

    public void seteRelation(String eRelation) {
        this.eRelation = eRelation;
    }

    public String geteEnvoi() {
        return eEnvoi;
    }

    public Partenaire eEnvoi(String eEnvoi) {
        this.eEnvoi = eEnvoi;
        return this;
    }

    public void seteEnvoi(String eEnvoi) {
        this.eEnvoi = eEnvoi;
    }

    public String getePublicAsso() {
        return ePublicAsso;
    }

    public Partenaire ePublicAsso(String ePublicAsso) {
        this.ePublicAsso = ePublicAsso;
        return this;
    }

    public void setePublicAsso(String ePublicAsso) {
        this.ePublicAsso = ePublicAsso;
    }

    public String geteTaxePro() {
        return eTaxePro;
    }

    public Partenaire eTaxePro(String eTaxePro) {
        this.eTaxePro = eTaxePro;
        return this;
    }

    public void seteTaxePro(String eTaxePro) {
        this.eTaxePro = eTaxePro;
    }

    public String geteDerProp() {
        return eDerProp;
    }

    public Partenaire eDerProp(String eDerProp) {
        this.eDerProp = eDerProp;
        return this;
    }

    public void seteDerProp(String eDerProp) {
        this.eDerProp = eDerProp;
    }

    public String geteDerStage() {
        return eDerStage;
    }

    public Partenaire eDerStage(String eDerStage) {
        this.eDerStage = eDerStage;
        return this;
    }

    public void seteDerStage(String eDerStage) {
        this.eDerStage = eDerStage;
    }

    public String geteNomSignataire() {
        return eNomSignataire;
    }

    public Partenaire eNomSignataire(String eNomSignataire) {
        this.eNomSignataire = eNomSignataire;
        return this;
    }

    public void seteNomSignataire(String eNomSignataire) {
        this.eNomSignataire = eNomSignataire;
    }

    public String geteNbranc() {
        return eNbranc;
    }

    public Partenaire eNbranc(String eNbranc) {
        this.eNbranc = eNbranc;
        return this;
    }

    public void seteNbranc(String eNbranc) {
        this.eNbranc = eNbranc;
    }

    public String geteEffectif() {
        return eEffectif;
    }

    public Partenaire eEffectif(String eEffectif) {
        this.eEffectif = eEffectif;
        return this;
    }

    public void seteEffectif(String eEffectif) {
        this.eEffectif = eEffectif;
    }

    public String getpOperat() {
        return pOperat;
    }

    public Partenaire pOperat(String pOperat) {
        this.pOperat = pOperat;
        return this;
    }

    public void setpOperat(String pOperat) {
        this.pOperat = pOperat;
    }

    public LocalDate getpDateMaj() {
        return pDateMaj;
    }

    public Partenaire pDateMaj(LocalDate pDateMaj) {
        this.pDateMaj = pDateMaj;
        return this;
    }

    public void setpDateMaj(LocalDate pDateMaj) {
        this.pDateMaj = pDateMaj;
    }

    public String getpSauvegarde() {
        return pSauvegarde;
    }

    public Partenaire pSauvegarde(String pSauvegarde) {
        this.pSauvegarde = pSauvegarde;
        return this;
    }

    public void setpSauvegarde(String pSauvegarde) {
        this.pSauvegarde = pSauvegarde;
    }

    public String getpMaj() {
        return pMaj;
    }

    public Partenaire pMaj(String pMaj) {
        this.pMaj = pMaj;
        return this;
    }

    public void setpMaj(String pMaj) {
        this.pMaj = pMaj;
    }

    public Naf getNafCode() {
        return nafCode;
    }

    public Partenaire nafCode(Naf naf) {
        this.nafCode = naf;
        return this;
    }

    public void setNafCode(Naf naf) {
        this.nafCode = naf;
    }

    public Region getrCodereg() {
        return rCodereg;
    }

    public Partenaire rCodereg(Region region) {
        this.rCodereg = region;
        return this;
    }

    public void setrCodereg(Region region) {
        this.rCodereg = region;
    }

    public Activite getTaCode() {
        return taCode;
    }

    public Partenaire taCode(Activite activite) {
        this.taCode = activite;
        return this;
    }

    public void setTaCode(Activite activite) {
        this.taCode = activite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Partenaire partenaire = (Partenaire) o;
        if(partenaire.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, partenaire.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Partenaire{" +
            "id=" + id +
            ", pNumSeq='" + pNumSeq + "'" +
            ", pSiret='" + pSiret + "'" +
            ", pService='" + pService + "'" +
            ", pNompart='" + pNompart + "'" +
            ", pRue='" + pRue + "'" +
            ", pCpltRue='" + pCpltRue + "'" +
            ", pCodep='" + pCodep + "'" +
            ", pVille='" + pVille + "'" +
            ", pTelStd='" + pTelStd + "'" +
            ", pFax='" + pFax + "'" +
            ", pUrl='" + pUrl + "'" +
            ", pCommentaire='" + pCommentaire + "'" +
            ", eRelation='" + eRelation + "'" +
            ", eEnvoi='" + eEnvoi + "'" +
            ", ePublicAsso='" + ePublicAsso + "'" +
            ", eTaxePro='" + eTaxePro + "'" +
            ", eDerProp='" + eDerProp + "'" +
            ", eDerStage='" + eDerStage + "'" +
            ", eNomSignataire='" + eNomSignataire + "'" +
            ", eNbranc='" + eNbranc + "'" +
            ", eEffectif='" + eEffectif + "'" +
            ", pOperat='" + pOperat + "'" +
            ", pDateMaj='" + pDateMaj + "'" +
            ", pSauvegarde='" + pSauvegarde + "'" +
            ", pMaj='" + pMaj + "'" +
            '}';
    }
}
