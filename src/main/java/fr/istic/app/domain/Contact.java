package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Contact.
 */
@Entity
@AttributeOverride(name = "id", column = @Column(name = "c_numseq"))
@Table(name = "contact")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "c_nom")
    private String cNom;

    @NotNull
    @Column(name = "c_role")
    private String cRole;

    @Column(name = "c_sexe")
    private String cSexe;

    @Column(name = "c_tel")
    private String cTel;

    @ManyToOne
    @NotNull
    @JoinColumn(name="p_ident_id", referencedColumnName="p_ident")
    private Partenaire pIdent;
    
    @OneToOne
    @NotNull
    @JoinColumn(unique = true, name = "user_id", referencedColumnName = "id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getcNom() {
        return cNom;
    }

    public Contact cNom(String cNom) {
        this.cNom = cNom;
        return this;
    }

    public void setcNom(String cNom) {
        this.cNom = cNom;
    }

    public String getcRole() {
        return cRole;
    }

    public Contact cRole(String cRole) {
        this.cRole = cRole;
        return this;
    }

    public void setcRole(String cRole) {
        this.cRole = cRole;
    }

    public String getcSexe() {
        return cSexe;
    }

    public Contact cSexe(String cSexe) {
        this.cSexe = cSexe;
        return this;
    }

    public void setcSexe(String cSexe) {
        this.cSexe = cSexe;
    }

    public String getcTel() {
        return cTel;
    }

    public Contact cTel(String cTel) {
        this.cTel = cTel;
        return this;
    }

    public void setcTel(String cTel) {
        this.cTel = cTel;
    }

    public Partenaire getpIdent() {
        return pIdent;
    }

    public Contact pIdent(Partenaire partenaire) {
        this.pIdent = partenaire;
        return this;
    }

    public void setpIdent(Partenaire partenaire) {
        this.pIdent = partenaire;
    }
    
    public User getUser() {
    	return user;
    }
    
    public Contact user(User user){
    	this.user = user;
    	return this;
    }
    
    public void setUser(User user){
    	this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contact contact = (Contact) o;
        if(contact.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, contact.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Contact{" +
            "id=" + id +
            ", cNom='" + cNom + "'" +
            ", cRole='" + cRole + "'" +
            ", cSexe='" + cSexe + "'" +
            ", cTel='" + cTel + "'" +
            '}';
    }
}
