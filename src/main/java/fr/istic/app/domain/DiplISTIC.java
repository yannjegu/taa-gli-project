package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DiplISTIC.
 */
@Entity
@Table(name = "dipl_istic")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DiplISTIC implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "an_sortie")
    private Integer anSortie;

    @Column(name = "if_divers")
    private String ifDivers;

    @Column(name = "an_entree")
    private Integer anEntree;

    @Column(name = "poursuite_etudes")
    private String poursuiteEtudes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAnSortie() {
        return anSortie;
    }

    public DiplISTIC anSortie(Integer anSortie) {
        this.anSortie = anSortie;
        return this;
    }

    public void setAnSortie(Integer anSortie) {
        this.anSortie = anSortie;
    }

    public String getIfDivers() {
        return ifDivers;
    }

    public DiplISTIC ifDivers(String ifDivers) {
        this.ifDivers = ifDivers;
        return this;
    }

    public void setIfDivers(String ifDivers) {
        this.ifDivers = ifDivers;
    }

    public Integer getAnEntree() {
        return anEntree;
    }

    public DiplISTIC anEntree(Integer anEntree) {
        this.anEntree = anEntree;
        return this;
    }

    public void setAnEntree(Integer anEntree) {
        this.anEntree = anEntree;
    }

    public String getPoursuiteEtudes() {
        return poursuiteEtudes;
    }

    public DiplISTIC poursuiteEtudes(String poursuiteEtudes) {
        this.poursuiteEtudes = poursuiteEtudes;
        return this;
    }

    public void setPoursuiteEtudes(String poursuiteEtudes) {
        this.poursuiteEtudes = poursuiteEtudes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DiplISTIC diplISTIC = (DiplISTIC) o;
        if(diplISTIC.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, diplISTIC.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DiplISTIC{" +
            "id=" + id +
            ", anSortie='" + anSortie + "'" +
            ", ifDivers='" + ifDivers + "'" +
            ", anEntree='" + anEntree + "'" +
            ", poursuiteEtudes='" + poursuiteEtudes + "'" +
            '}';
    }
}
