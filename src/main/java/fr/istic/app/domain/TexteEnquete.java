package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TexteEnquete.
 */
@Entity
@Table(name = "texte_enquete")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TexteEnquete implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "te_reponse")
    private String teReponse;

    @OneToOne
    @JoinColumn(unique = true, name = "a_ident_id")
    private Enquete aIdent;

    @OneToOne
    @JoinColumn(unique = true, name = "enq_n_ordre_id")
    private Enquete enqNOrdre;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeReponse() {
        return teReponse;
    }

    public TexteEnquete teReponse(String teReponse) {
        this.teReponse = teReponse;
        return this;
    }

    public void setTeReponse(String teReponse) {
        this.teReponse = teReponse;
    }

    public Enquete getaIdent() {
        return aIdent;
    }

    public TexteEnquete aIdent(Enquete enquete) {
        this.aIdent = enquete;
        return this;
    }

    public void setaIdent(Enquete enquete) {
        this.aIdent = enquete;
    }

    public Enquete getEnqNOrdre() {
        return enqNOrdre;
    }

    public TexteEnquete enqNOrdre(Enquete enquete) {
        this.enqNOrdre = enquete;
        return this;
    }

    public void setEnqNOrdre(Enquete enquete) {
        this.enqNOrdre = enquete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TexteEnquete texteEnquete = (TexteEnquete) o;
        if(texteEnquete.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, texteEnquete.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TexteEnquete{" +
            "id=" + id +
            ", teReponse='" + teReponse + "'" +
            '}';
    }
}
