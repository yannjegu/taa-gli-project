package fr.istic.app.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Enquete.
 */
@Entity
@Table(name = "enquete")
@AttributeOverride(name = "id", column = @Column(name = "enq_ident"))
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Enquete implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "mode_enquete")
    private String modeEnquete;
    
    @NotNull
    @Column(name = "date_enquete")
    private LocalDate dateEnquete;

    @NotNull
    @Column(name = "a_nom_usuel")
    private String aNomUsuel;

    @Column(name = "a_rue")
    private String aRue;

    @Column(name = "a_cplt_rue")
    private String aCpltRue;

    @Column(name = "a_ville")
    private String aVille;

    @Column(name = "a_code_dep")
    private String aCodeDep;

    @Column(name = "enq_domact")
    private String enqDomact;

    @Column(name = "enq_date_debut")
    private LocalDate enqDateDebut;

    @Column(name = "enq_duree_recherche")
    private String enqDureeRecherche;

    @Column(name = "enq_1_er_avant_dispo")
    private String enq1ErAvantDispo;

    @Column(name = "enq_quotite")
    private String enqQuotite;

    @Column(name = "enq_rech_autre")
    private String enqRechAutre;

    @Column(name = "enq_salaire")
    private Float enqSalaire;

    @Column(name = "enq_salaire_fixe")
    private Float enqSalaireFixe;

    @Column(name = "enq_salaire_variable")
    private Float enqSalaireVariable;

    @Column(name = "enq_salaire_pourcent")
    private Float enqSalairePourcent;

    @Column(name = "enq_salaire_avantage")
    private String enqSalaireAvantage;

    @Column(name = "enq_salaire_devise")
    private String enqSalaireDevise;

    @Column(name = "a_operat")
    private String aOperat;

    @OneToOne
    @JoinColumn(unique = true, name = "enq_situation_id", referencedColumnName = "sit_code")
    private Situation enqSituation;

    @OneToOne
    @JoinColumn(unique = true, name = "enq_mode_obtention_id", referencedColumnName = "obt_code")
    private ModeObtention enqModeObtention;

    @ManyToOne
    @JoinColumn(name = "p_ident_id", referencedColumnName = "p_ident")
    private Partenaire pIdent;

    @ManyToOne
    @JoinColumn(name = "a_ident_id", referencedColumnName = "a_ident")
    private Etudiant aIdent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModeEnquete() {
        return modeEnquete;
    }

    public Enquete modeEnquete(String modeEnquete) {
        this.modeEnquete = modeEnquete;
        return this;
    }

    public void setModeEnquete(String modeEnquete) {
        this.modeEnquete = modeEnquete;
    }

    public LocalDate getDateEnquete() {
        return dateEnquete;
    }

    public Enquete dateEnquete(LocalDate dateEnquete) {
        this.dateEnquete = dateEnquete;
        return this;
    }

    public void setDateEnquete(LocalDate dateEnquete) {
        this.dateEnquete = dateEnquete;
    }

    public String getaNomUsuel() {
        return aNomUsuel;
    }

    public Enquete aNomUsuel(String aNomUsuel) {
        this.aNomUsuel = aNomUsuel;
        return this;
    }

    public void setaNomUsuel(String aNomUsuel) {
        this.aNomUsuel = aNomUsuel;
    }

    public String getaRue() {
        return aRue;
    }

    public Enquete aRue(String aRue) {
        this.aRue = aRue;
        return this;
    }

    public void setaRue(String aRue) {
        this.aRue = aRue;
    }

    public String getaCpltRue() {
        return aCpltRue;
    }

    public Enquete aCpltRue(String aCpltRue) {
        this.aCpltRue = aCpltRue;
        return this;
    }

    public void setaCpltRue(String aCpltRue) {
        this.aCpltRue = aCpltRue;
    }

    public String getaVille() {
        return aVille;
    }

    public Enquete aVille(String aVille) {
        this.aVille = aVille;
        return this;
    }

    public void setaVille(String aVille) {
        this.aVille = aVille;
    }

    public String getaCodeDep() {
        return aCodeDep;
    }

    public Enquete aCodeDep(String aCodeDep) {
        this.aCodeDep = aCodeDep;
        return this;
    }

    public void setaCodeDep(String aCodeDep) {
        this.aCodeDep = aCodeDep;
    }

    public String getEnqDomact() {
        return enqDomact;
    }

    public Enquete enqDomact(String enqDomact) {
        this.enqDomact = enqDomact;
        return this;
    }

    public void setEnqDomact(String enqDomact) {
        this.enqDomact = enqDomact;
    }

    public LocalDate getEnqDateDebut() {
        return enqDateDebut;
    }

    public Enquete enqDateDebut(LocalDate enqDateDebut) {
        this.enqDateDebut = enqDateDebut;
        return this;
    }

    public void setEnqDateDebut(LocalDate enqDateDebut) {
        this.enqDateDebut = enqDateDebut;
    }

    public String getEnqDureeRecherche() {
        return enqDureeRecherche;
    }

    public Enquete enqDureeRecherche(String enqDureeRecherche) {
        this.enqDureeRecherche = enqDureeRecherche;
        return this;
    }

    public void setEnqDureeRecherche(String enqDureeRecherche) {
        this.enqDureeRecherche = enqDureeRecherche;
    }

    public String getEnq1ErAvantDispo() {
        return enq1ErAvantDispo;
    }

    public Enquete enq1ErAvantDispo(String enq1ErAvantDispo) {
        this.enq1ErAvantDispo = enq1ErAvantDispo;
        return this;
    }

    public void setEnq1ErAvantDispo(String enq1ErAvantDispo) {
        this.enq1ErAvantDispo = enq1ErAvantDispo;
    }

    public String getEnqQuotite() {
        return enqQuotite;
    }

    public Enquete enqQuotite(String enqQuotite) {
        this.enqQuotite = enqQuotite;
        return this;
    }

    public void setEnqQuotite(String enqQuotite) {
        this.enqQuotite = enqQuotite;
    }

    public String getEnqRechAutre() {
        return enqRechAutre;
    }

    public Enquete enqRechAutre(String enqRechAutre) {
        this.enqRechAutre = enqRechAutre;
        return this;
    }

    public void setEnqRechAutre(String enqRechAutre) {
        this.enqRechAutre = enqRechAutre;
    }

    public Float getEnqSalaire() {
        return enqSalaire;
    }

    public Enquete enqSalaire(Float enqSalaire) {
        this.enqSalaire = enqSalaire;
        return this;
    }

    public void setEnqSalaire(Float enqSalaire) {
        this.enqSalaire = enqSalaire;
    }

    public Float getEnqSalaireFixe() {
        return enqSalaireFixe;
    }

    public Enquete enqSalaireFixe(Float enqSalaireFixe) {
        this.enqSalaireFixe = enqSalaireFixe;
        return this;
    }

    public void setEnqSalaireFixe(Float enqSalaireFixe) {
        this.enqSalaireFixe = enqSalaireFixe;
    }

    public Float getEnqSalaireVariable() {
        return enqSalaireVariable;
    }

    public Enquete enqSalaireVariable(Float enqSalaireVariable) {
        this.enqSalaireVariable = enqSalaireVariable;
        return this;
    }

    public void setEnqSalaireVariable(Float enqSalaireVariable) {
        this.enqSalaireVariable = enqSalaireVariable;
    }

    public Float getEnqSalairePourcent() {
        return enqSalairePourcent;
    }

    public Enquete enqSalairePourcent(Float enqSalairePourcent) {
        this.enqSalairePourcent = enqSalairePourcent;
        return this;
    }

    public void setEnqSalairePourcent(Float enqSalairePourcent) {
        this.enqSalairePourcent = enqSalairePourcent;
    }

    public String getEnqSalaireAvantage() {
        return enqSalaireAvantage;
    }

    public Enquete enqSalaireAvantage(String enqSalaireAvantage) {
        this.enqSalaireAvantage = enqSalaireAvantage;
        return this;
    }

    public void setEnqSalaireAvantage(String enqSalaireAvantage) {
        this.enqSalaireAvantage = enqSalaireAvantage;
    }

    public String getEnqSalaireDevise() {
        return enqSalaireDevise;
    }

    public Enquete enqSalaireDevise(String enqSalaireDevise) {
        this.enqSalaireDevise = enqSalaireDevise;
        return this;
    }

    public void setEnqSalaireDevise(String enqSalaireDevise) {
        this.enqSalaireDevise = enqSalaireDevise;
    }

    public String getaOperat() {
        return aOperat;
    }

    public Enquete aOperat(String aOperat) {
        this.aOperat = aOperat;
        return this;
    }

    public void setaOperat(String aOperat) {
        this.aOperat = aOperat;
    }

    public Situation getEnqSituation() {
        return enqSituation;
    }

    public Enquete enqSituation(Situation situation) {
        this.enqSituation = situation;
        return this;
    }

    public void setEnqSituation(Situation situation) {
        this.enqSituation = situation;
    }

    public ModeObtention getEnqModeObtention() {
        return enqModeObtention;
    }

    public Enquete enqModeObtention(ModeObtention modeObtention) {
        this.enqModeObtention = modeObtention;
        return this;
    }

    public void setEnqModeObtention(ModeObtention modeObtention) {
        this.enqModeObtention = modeObtention;
    }

    public Partenaire getpIdent() {
        return pIdent;
    }

    public Enquete pIdent(Partenaire partenaire) {
        this.pIdent = partenaire;
        return this;
    }

    public void setpIdent(Partenaire partenaire) {
        this.pIdent = partenaire;
    }

    public Etudiant getaIdent() {
        return aIdent;
    }

    public Enquete aIdent(Etudiant etudiant) {
        this.aIdent = etudiant;
        return this;
    }

    public void setaIdent(Etudiant etudiant) {
        this.aIdent = etudiant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Enquete enquete = (Enquete) o;
        if(enquete.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, enquete.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Enquete{" +
            "id=" + id +
            ", modeEnquete='" + modeEnquete + "'" +
            ", dateEnquete='" + dateEnquete + "'" +
            ", aNomUsuel='" + aNomUsuel + "'" +
            ", aRue='" + aRue + "'" +
            ", aCpltRue='" + aCpltRue + "'" +
            ", aVille='" + aVille + "'" +
            ", aCodeDep='" + aCodeDep + "'" +
            ", enqDomact='" + enqDomact + "'" +
            ", enqDateDebut='" + enqDateDebut + "'" +
            ", enqDureeRecherche='" + enqDureeRecherche + "'" +
            ", enq1ErAvantDispo='" + enq1ErAvantDispo + "'" +
            ", enqQuotite='" + enqQuotite + "'" +
            ", enqRechAutre='" + enqRechAutre + "'" +
            ", enqSalaire='" + enqSalaire + "'" +
            ", enqSalaireFixe='" + enqSalaireFixe + "'" +
            ", enqSalaireVariable='" + enqSalaireVariable + "'" +
            ", enqSalairePourcent='" + enqSalairePourcent + "'" +
            ", enqSalaireAvantage='" + enqSalaireAvantage + "'" +
            ", enqSalaireDevise='" + enqSalaireDevise + "'" +
            ", aOperat='" + aOperat + "'" +
            '}';
    }
}
