package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Enseignant.
 */
@Entity
@Table(name = "enseignant")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Enseignant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "en_sesame")
    private String enSesame;
    
    @NotNull
    @Size(max = 100)
    @Column(name = "en_ad_elec")
    private String enAdElec;

    @Column(name = "en_sexe")
    private String enSexe;

    @NotNull
    @Column(name = "en_nom")
    private String enNom;

    @NotNull
    @Column(name = "en_prenom")
    private String enPrenom;

    @Column(name = "en_adr")
    private String enAdr;

    @Column(name = "en_tel_pro")
    private String enTelPro;

    @NotNull
    @Column(name = "en_actif")
    private boolean enActif = false;

    @Column(name = "en_maj")
    private String enMaj;
    
    @OneToOne
    @NotNull
    @JoinColumn(unique = true, name = "user_id", referencedColumnName = "id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEnSesame() {
        return enSesame;
    }

    public Enseignant enSesame(String enSesame) {
        this.enSesame = enSesame;
        return this;
    }

    public void setEnSesame(String enSesame) {
        this.enSesame = enSesame;
    }

    public String getEnAdElec() {
        return enAdElec;
    }

    public Enseignant enAdElec(String enAdElec) {
        this.enAdElec = enAdElec;
        return this;
    }

    public void setEnAdElec(String enAdElec) {
        this.enAdElec = enAdElec;
    }

    public String getEnSexe() {
        return enSexe;
    }

    public Enseignant enSexe(String enSexe) {
        this.enSexe = enSexe;
        return this;
    }

    public void setEnSexe(String enSexe) {
        this.enSexe = enSexe;
    }

    public String getEnNom() {
        return enNom;
    }

    public Enseignant enNom(String enNom) {
        this.enNom = enNom;
        return this;
    }

    public void setEnNom(String enNom) {
        this.enNom = enNom;
    }

    public String getEnPrenom() {
        return enPrenom;
    }

    public Enseignant enPrenom(String enPrenom) {
        this.enPrenom = enPrenom;
        return this;
    }

    public void setEnPrenom(String enPrenom) {
        this.enPrenom = enPrenom;
    }

    public String getEnAdr() {
        return enAdr;
    }

    public Enseignant enAdr(String enAdr) {
        this.enAdr = enAdr;
        return this;
    }

    public void setEnAdr(String enAdr) {
        this.enAdr = enAdr;
    }

    public String getEnTelPro() {
        return enTelPro;
    }

    public Enseignant enTelPro(String enTelPro) {
        this.enTelPro = enTelPro;
        return this;
    }

    public void setEnTelPro(String enTelPro) {
        this.enTelPro = enTelPro;
    }

    public boolean getEnActif() {
        return enActif;
    }

    public void setEnActif(boolean enActif) {
        this.enActif = enActif;
    }
    
    public Enseignant enActif(boolean enActif) {
		this.enActif = enActif;
    	return this;
	}

    public String getEnMaj() {
        return enMaj;
    }

    public Enseignant enMaj(String enMaj) {
        this.enMaj = enMaj;
        return this;
    }

    public void setEnMaj(String enMaj) {
        this.enMaj = enMaj;
    }
    
    public User getUser() {
    	return user;
    }
    
    public Enseignant user(User user){
    	this.user = user;
    	return this;
    }
    
    public void setUser(User user){
    	this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Enseignant enseignant = (Enseignant) o;
        if(enseignant.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, enseignant.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Enseignant{" +
            "id=" + id +
            ", enSesame='" + enSesame + "'" +
            ", enAdElec='" + enAdElec + "'" +
            ", enSexe='" + enSexe + "'" +
            ", enNom='" + enNom + "'" +
            ", enPrenom='" + enPrenom + "'" +
            ", enAdr='" + enAdr + "'" +
            ", enTelPro='" + enTelPro + "'" +
            ", enActif='" + enActif + "'" +
            ", enMaj='" + enMaj + "'" +
            '}';
    }
}
