package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Situation.
 */
@Entity
@Table(name = "situation")
@AttributeOverride(name = "id", column = @Column(name = "sit_code"))
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Situation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "sit_ordre")
    private String sitOrdre;

    @NotNull
    @Column(name = "sit_lib_c")
    private String sitLibC;

    @Column(name = "sit_description")
    private String sitDescription;

    @NotNull
    @Column(name = "sit_emploi")
    private String sitEmploi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSitOrdre() {
        return sitOrdre;
    }

    public Situation sitOrdre(String sitOrdre) {
        this.sitOrdre = sitOrdre;
        return this;
    }

    public void setSitOrdre(String sitOrdre) {
        this.sitOrdre = sitOrdre;
    }

    public String getSitLibC() {
        return sitLibC;
    }

    public Situation sitLibC(String sitLibC) {
        this.sitLibC = sitLibC;
        return this;
    }

    public void setSitLibC(String sitLibC) {
        this.sitLibC = sitLibC;
    }

    public String getSitDescription() {
        return sitDescription;
    }

    public Situation sitDescription(String sitDescription) {
        this.sitDescription = sitDescription;
        return this;
    }

    public void setSitDescription(String sitDescription) {
        this.sitDescription = sitDescription;
    }

    public String getSitEmploi() {
        return sitEmploi;
    }

    public Situation sitEmploi(String sitEmploi) {
        this.sitEmploi = sitEmploi;
        return this;
    }

    public void setSitEmploi(String sitEmploi) {
        this.sitEmploi = sitEmploi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Situation situation = (Situation) o;
        if(situation.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, situation.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Situation{" +
            "id=" + id +
            ", sitOrdre='" + sitOrdre + "'" +
            ", sitLibC='" + sitLibC + "'" +
            ", sitDescription='" + sitDescription + "'" +
            ", sitEmploi='" + sitEmploi + "'" +
            '}';
    }
}
