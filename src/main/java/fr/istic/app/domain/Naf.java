package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Naf.
 */
@Entity
@AttributeOverride(name = "id", column = @Column(name = "naf_code"))
@Table(name = "naf")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Naf implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "naf_libelle", unique = true)
    private String nafLibelle;

    @ManyToOne
    @JoinColumn(name="ta_code_id",referencedColumnName="ta_code")
    private Activite taCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNafLibelle() {
        return nafLibelle;
    }

    public Naf nafLibelle(String nafLibelle) {
        this.nafLibelle = nafLibelle;
        return this;
    }

    public void setNafLibelle(String nafLibelle) {
        this.nafLibelle = nafLibelle;
    }

    public Activite getTaCode() {
        return taCode;
    }

    public Naf taCode(Activite activite) {
        this.taCode = activite;
        return this;
    }

    public void setTaCode(Activite activite) {
        this.taCode = activite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Naf naf = (Naf) o;
        if(naf.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, naf.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Naf{" +
            "id=" + id +
            ", nafLibelle='" + nafLibelle + "'" +
            '}';
    }
}
