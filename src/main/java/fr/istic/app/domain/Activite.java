package fr.istic.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Activite.
 */
@Entity
@AttributeOverride(name = "id", column = @Column(name = "ta_code"))
@Table(name = "activite")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Activite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "ta_libelle")
    private String taLibelle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaLibelle() {
        return taLibelle;
    }

    public Activite taLibelle(String taLibelle) {
        this.taLibelle = taLibelle;
        return this;
    }

    public void setTaLibelle(String taLibelle) {
        this.taLibelle = taLibelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Activite activite = (Activite) o;
        if(activite.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, activite.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Activite{" +
            "id=" + id +
            ", taLibelle='" + taLibelle + "'" +
            '}';
    }
}
