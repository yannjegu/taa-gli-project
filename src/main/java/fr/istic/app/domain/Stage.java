package fr.istic.app.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Stage.
 */
@Entity
@Table(name = "stage")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Stage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "s_service")
    private String sService;

    @NotNull
    @Column(name = "s_date_debut")
    private LocalDate sDateDebut;

    @Column(name = "s_date_fin")
    private LocalDate sDateFin;
    
    @Column(name = "s_sujet")
    private String sSujet;

    @Column(name = "s_lang")
    private String sLang;

    @Column(name = "s_materiel")
    private String sMateriel;

    @Column(name = "m_mots_cles")
    private String mMotsCles;

    @Column(name = "s_details")
    private String sDetails;

    @Column(name = "s_jours_duree_travail")
    private String sJoursDureeTravail;

    @Column(name = "s_indp")
    private String sIndp;

    @Column(name = "s_versement")
    private String sVersement;

    @Column(name = "s_avantages_nature")
    private String sAvantagesNature;

    @Column(name = "s_mode_suiv")
    private String sModeSuiv;

    @Column(name = "s_appreciation")
    private String sAppreciation;

    @Column(name = "s_date_conv")
    private LocalDate sDateConv;

    @Column(name = "s_eta_conv")
    private String sEtaConv;

    @Column(name = "s_soutenance")
    private String sSoutenance;

    @Column(name = "s_rapport")
    private String sRapport;

    @Column(name = "s_operat")
    private String sOperat;

    @Column(name = "s_date_maj")
    private LocalDate sDateMaj;

    @Column(name = "s_maj")
    private String sMaj;
    
    @Column(name = "s_nom_part")
    private String sNomPart;
    
    @Column(name = "s_adresse")
    private String sAdresse;
    
    @Column(name = "s_cplt_adresse")
    private String sCpltAdresse;

    @OneToOne
    @JoinColumn(name = "p_ident_id", referencedColumnName = "p_ident")
    private Partenaire pIdent;

    @OneToOne
    private Enseignant enIdent;

    @OneToOne
    @JoinColumn(name = "num_seq_enc_id")
    private Contact numSeqEnc;

    @OneToOne
    @JoinColumn(name = "num_seq_resp_id")
    private Contact numSeqResp;
    
    @ManyToOne
    @NotNull
    @JoinColumn(name = "a_ident_id", referencedColumnName = "a_ident")
    private Etudiant aIdent;
    
    @ManyToOne
    @JoinColumn(name = "sc_ident_id", referencedColumnName = "id")
    private StatusConvention scIdent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getsService() {
        return sService;
    }

    public Stage sService(String sService) {
        this.sService = sService;
        return this;
    }

    public void setsService(String sService) {
        this.sService = sService;
    }

    public LocalDate getsDateDebut() {
        return sDateDebut;
    }

    public Stage sDateDebut(LocalDate sDateDebut) {
        this.sDateDebut = sDateDebut;
        return this;
    }

    public void setsDateDebut(LocalDate sDateDebut) {
        this.sDateDebut = sDateDebut;
    }

    public LocalDate getsDateFin() {
        return sDateFin;
    }

    public Stage sDateFin(LocalDate sDateFin) {
        this.sDateFin = sDateFin;
        return this;
    }

    public void setsDateFin(LocalDate sDateFin) {
        this.sDateFin = sDateFin;
    }

    public String getsSujet() {
        return sSujet;
    }

    public Stage sSujet(String sSujet) {
        this.sSujet = sSujet;
        return this;
    }

    public void setsSujet(String sSujet) {
        this.sSujet = sSujet;
    }

    public String getsLang() {
        return sLang;
    }

    public Stage sLang(String sLang) {
        this.sLang = sLang;
        return this;
    }

    public void setsLang(String sLang) {
        this.sLang = sLang;
    }

    public String getsMateriel() {
        return sMateriel;
    }

    public Stage sMateriel(String sMateriel) {
        this.sMateriel = sMateriel;
        return this;
    }

    public void setsMateriel(String sMateriel) {
        this.sMateriel = sMateriel;
    }

    public String getmMotsCles() {
        return mMotsCles;
    }

    public Stage mMotsCles(String mMotsCles) {
        this.mMotsCles = mMotsCles;
        return this;
    }

    public void setmMotsCles(String mMotsCles) {
        this.mMotsCles = mMotsCles;
    }

    public String getsDetails() {
        return sDetails;
    }

    public Stage sDetails(String sDetails) {
        this.sDetails = sDetails;
        return this;
    }

    public void setsDetails(String sDetails) {
        this.sDetails = sDetails;
    }

    public String getsJoursDureeTravail() {
        return sJoursDureeTravail;
    }

    public Stage sJoursDureeTravail(String sJoursDureeTravail) {
        this.sJoursDureeTravail = sJoursDureeTravail;
        return this;
    }

    public void setsJoursDureeTravail(String sJoursDureeTravail) {
        this.sJoursDureeTravail = sJoursDureeTravail;
    }

    public String getsIndp() {
        return sIndp;
    }

    public Stage sIndp(String sIndp) {
        this.sIndp = sIndp;
        return this;
    }

    public void setsIndp(String sIndp) {
        this.sIndp = sIndp;
    }

    public String getsVersement() {
        return sVersement;
    }

    public Stage sVersement(String sVersement) {
        this.sVersement = sVersement;
        return this;
    }

    public void setsVersement(String sVersement) {
        this.sVersement = sVersement;
    }

    public String getsAvantagesNature() {
        return sAvantagesNature;
    }

    public Stage sAvantagesNature(String sAvantagesNature) {
        this.sAvantagesNature = sAvantagesNature;
        return this;
    }

    public void setsAvantagesNature(String sAvantagesNature) {
        this.sAvantagesNature = sAvantagesNature;
    }

    public String getsModeSuiv() {
        return sModeSuiv;
    }

    public Stage sModeSuiv(String sModeSuiv) {
        this.sModeSuiv = sModeSuiv;
        return this;
    }

    public void setsModeSuiv(String sModeSuiv) {
        this.sModeSuiv = sModeSuiv;
    }

    public String getsAppreciation() {
        return sAppreciation;
    }

    public Stage sAppreciation(String sAppreciation) {
        this.sAppreciation = sAppreciation;
        return this;
    }

    public void setsAppreciation(String sAppreciation) {
        this.sAppreciation = sAppreciation;
    }
    
    public String getsNomPart() {
        return sNomPart;
    }

    public Stage sNomPart(String sNomPart) {
        this.sNomPart = sNomPart;
        return this;
    }

    public void setsNomPart(String sNomPart) {
        this.sNomPart = sNomPart;
    }
    
    public String getsAdresse() {
        return sAdresse;
    }

    public Stage sAdresse(String sAdresse) {
        this.sAdresse = sAdresse;
        return this;
    }

    public void setsAdresse(String sAdresse) {
        this.sAdresse = sAdresse;
    }
    
    public String getsCpltAdresse() {
        return sCpltAdresse;
    }

    public Stage sCpltAdresse(String sCpltAdresse) {
        this.sCpltAdresse = sCpltAdresse;
        return this;
    }

    public void setsCpltAdresse(String sCpltAdresse) {
        this.sCpltAdresse = sCpltAdresse;
    }

    public LocalDate getsDateConv() {
        return sDateConv;
    }

    public Stage sDateConv(LocalDate sDateConv) {
        this.sDateConv = sDateConv;
        return this;
    }

    public void setsDateConv(LocalDate sDateConv) {
        this.sDateConv = sDateConv;
    }

    public String getsEtaConv() {
        return sEtaConv;
    }

    public Stage sEtaConv(String sEtaConv) {
        this.sEtaConv = sEtaConv;
        return this;
    }

    public void setsEtaConv(String sEtaConv) {
        this.sEtaConv = sEtaConv;
    }

    public String getsSoutenance() {
        return sSoutenance;
    }

    public Stage sSoutenance(String sSoutenance) {
        this.sSoutenance = sSoutenance;
        return this;
    }

    public void setsSoutenance(String sSoutenance) {
        this.sSoutenance = sSoutenance;
    }

    public String getsRapport() {
        return sRapport;
    }

    public Stage sRapport(String sRapport) {
        this.sRapport = sRapport;
        return this;
    }

    public void setsRapport(String sRapport) {
        this.sRapport = sRapport;
    }

    public String getsOperat() {
        return sOperat;
    }

    public Stage sOperat(String sOperat) {
        this.sOperat = sOperat;
        return this;
    }

    public void setsOperat(String sOperat) {
        this.sOperat = sOperat;
    }

    public LocalDate getsDateMaj() {
        return sDateMaj;
    }

    public Stage sDateMaj(LocalDate sDateMaj) {
        this.sDateMaj = sDateMaj;
        return this;
    }

    public void setsDateMaj(LocalDate sDateMaj) {
        this.sDateMaj = sDateMaj;
    }

    public String getsMaj() {
        return sMaj;
    }

    public Stage sMaj(String sMaj) {
        this.sMaj = sMaj;
        return this;
    }

    public void setsMaj(String sMaj) {
        this.sMaj = sMaj;
    }

    public Partenaire getPIdent() {
        return pIdent;
    }

    public Stage pIdent(Partenaire partenaire) {
        this.pIdent = partenaire;
        return this;
    }

    public void setPIdent(Partenaire partenaire) {
        this.pIdent = partenaire;
    }

    public Enseignant getEnIdent() {
        return enIdent;
    }

    public Stage enIdent(Enseignant enseignant) {
        this.enIdent = enseignant;
        return this;
    }

    public void setEnIdent(Enseignant enseignant) {
        this.enIdent = enseignant;
    }

    public Contact getNumSeqEnc() {
        return numSeqEnc;
    }

    public Stage numSeqEnc(Contact contact) {
        this.numSeqEnc = contact;
        return this;
    }

    public void setNumSeqEnc(Contact contact) {
        this.numSeqEnc = contact;
    }

    public Contact getNumSeqResp() {
        return numSeqResp;
    }

    public Stage numSeqResp(Contact contact) {
        this.numSeqResp = contact;
        return this;
    }

    public void setNumSeqResp(Contact contact) {
        this.numSeqResp = contact;
    }

    public Partenaire getpIdent() {
		return pIdent;
	}

	public void setpIdent(Partenaire pIdent) {
		this.pIdent = pIdent;
	}

	public Etudiant getaIdent() {
		return aIdent;
	}

	public void setaIdent(Etudiant aIdent) {
		this.aIdent = aIdent;
	}
	
	public StatusConvention getscIdent() {
		return scIdent;
	}

	public void setscIdent(StatusConvention scIdent) {
		this.scIdent = scIdent;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Stage stage = (Stage) o;
        if(stage.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, stage.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Stage{" +
            "id=" + id +
            ", sService='" + sService + "'" +
            ", sDateDebut='" + sDateDebut + "'" +
            ", sDateFin='" + sDateFin + "'" +
            ", sSujet='" + sSujet + "'" +
            ", sLang='" + sLang + "'" +
            ", sMateriel='" + sMateriel + "'" +
            ", mMotsCles='" + mMotsCles + "'" +
            ", sDetails='" + sDetails + "'" +
            ", sJoursDureeTravail='" + sJoursDureeTravail + "'" +
            ", sIndp='" + sIndp + "'" +
            ", sVersement='" + sVersement + "'" +
            ", sAvantagesNature='" + sAvantagesNature + "'" +
            ", sModeSuiv='" + sModeSuiv + "'" +
            ", sAppreciation='" + sAppreciation + "'" +
            ", sDateConv='" + sDateConv + "'" +
            ", sEtaConv='" + sEtaConv + "'" +
            ", sSoutenance='" + sSoutenance + "'" +
            ", sRapport='" + sRapport + "'" +
            ", sOperat='" + sOperat + "'" +
            ", sDateMaj='" + sDateMaj + "'" +
            ", sMaj='" + sMaj + "'" +
            '}';
    }
}
